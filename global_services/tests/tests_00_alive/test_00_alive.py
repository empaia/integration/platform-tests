import requests

from ....global_services.data_generators.singletons import general_settings, login_manager


def test_alive_mps():
    headers = login_manager.wbs_client()
    url = f"{general_settings.mps_host}alive"
    r = requests.get(url, headers=headers)
    print(f"MPS {url}: ", r.status_code, r.content)
    assert r.status_code == 200


def test_alive_aaa():
    if general_settings.enable_aaa_mock:
        url = f"{general_settings.aaa_host}alive"
        r = requests.get(url)
        print(f"AAA MOCK {url}: ", r.status_code, r.content)
        assert r.status_code == 200
    else:
        url = f"{general_settings.aaa_host}api/v2/public/organizations"
        r = requests.get(url)
        print(f"AAA {url}: ", r.status_code, r.content)
        assert r.status_code == 200
