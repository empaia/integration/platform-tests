import datetime
from uuid import uuid4

import requests

from ....global_services.data_generators.singletons import general_settings


def generate_app_json(app: dict, organization: dict):
    active_app_views = {}
    for api_version in app:
        app_raw = app[api_version]
        ead = app_raw["ead"]
        docker_registry = app_raw["registry"]
        has_frontend = app_raw["has_frontend"]
        app_ui_url = None
        if "app_ui_url" in app_raw:
            app_ui_url = app_raw["app_ui_url"]
        portal_app_id = str(uuid4())
        creator_id = str(uuid4())
        now = int(datetime.datetime.now().timestamp())

        version = api_version
        if api_version in ["v3"]:
            version += ".0"
        active_app_view = {
            "version": version,
            "api_version": api_version,
            "details": {
                "name": ead["namespace"],
                "description": [
                    {"lang": "EN", "text": ead["description"]},
                    {"lang": "DE", "text": ead["description"]},
                ],
                "marketplace_url": "http://url.to/store",
            },
            "media": {
                "peek": [
                    {
                        "index": 0,
                        "caption": [
                            {"lang": "EN", "text": "Peek caption"},
                            {"lang": "DE", "text": "Titel Übersichtsbild"},
                        ],
                        "alt_text": [
                            {"lang": "EN", "text": "Peek alternative text"},
                            {"lang": "DE", "text": "Alternativtext Übersichtsbild"},
                        ],
                        "internal_path": "/internal/path/to",
                        "content_type": "image/jpeg",
                        "presigned_media_url": "https://url.to/image",
                        "id": str(uuid4()),
                    }
                ]
            },
            "tags": {
                "tissues": [
                    {
                        "name": "PROSTATE",
                        "tag_translations": [{"lang": "EN", "text": "Prostate"}, {"lang": "DE", "text": "Prostata"}],
                    }
                ],
                "stains": [
                    {
                        "name": "HE",
                        "tag_translations": [{"lang": "EN", "text": "HE"}, {"lang": "DE", "text": "HE"}],
                    }
                ],
                "indications": [
                    {
                        "name": "ICCR_PROSTATE_CANCERS",
                        "tag_translations": [
                            {"lang": "EN", "text": "Prostate Cancers [ICCR]"},
                            {"lang": "DE", "text": "Prostatakarzinome [ICCR]"},
                        ],
                    }
                ],
                "analysis": [
                    {
                        "name": "CLASSIFICATION",
                        "tag_translations": [
                            {"lang": "EN", "text": "Classification"},
                            {"lang": "DE", "text": "Klassifizierung"},
                        ],
                    }
                ],
            },
            "non_functional": False,
            "id": str(uuid4()),
            "portal_app_id": portal_app_id,
            "organization_id": organization["organization_id"],
            "status": "APPROVED",
            "app": {
                "ead": ead,
                "registry_image_url": docker_registry,
                "app_ui_url": app_ui_url,
                "id": str(uuid4()),
                "version": version,
                "has_frontend": has_frontend,
                "status": "APPROVED",
                "portal_app_id": portal_app_id,
                "creator_id": creator_id,
                "created_at": now,
                "updated_at": now,
            },
            "creator_id": creator_id,
            "review_comment": "Review comment",
            "reviewer_id": str(uuid4()),
            "created_at": now,
        }
        active_app_views[api_version] = active_app_view

    data = {
        "id": portal_app_id,
        "organization_id": organization["organization_id"],
        "status": "LISTED",
        "creator_id": creator_id,
        "active_app_views": active_app_views,
        "created_at": now,
        "updated_at": now,
    }
    return data


def mps_post_app(data):
    url = f"{general_settings.mps_host}v1/custom-mock/portal-apps"
    return requests.post(url, json=data)


def put_global_configuration(app_id: str, conf):
    url = f"{general_settings.mps_host}v1/custom-mock/apps/{app_id}/config/global"
    r = requests.put(url, json=conf)
    return r


def put_ui_configuration(app_id: str, ui_conf):
    url = f"{general_settings.mps_host}v1/custom-mock/apps/{app_id}/app-ui-config"
    r = requests.put(url, json=ui_conf)
    return r


def init_configurations(apps_by_namespace, configurations_by_namespace):
    for namespace in configurations_by_namespace:
        conf = configurations_by_namespace[namespace]
        app_id = apps_by_namespace[namespace]["id"]
        r = put_global_configuration(app_id, conf)
        print("POST - MPS configurations:")
        print("status_code: ", r.status_code)
        print("content: ", r.content)
        assert r.status_code == 200 or r.status_code == 409  # might already exist


def init_ui_configurations(apps_by_namespace, ui_configurations_by_namespace):
    for namespace in ui_configurations_by_namespace:
        ui_conf = ui_configurations_by_namespace[namespace]
        app_id = apps_by_namespace[namespace]["id"]
        r = put_ui_configuration(app_id, ui_conf)
        print("POST - MPS App UI configurations:")
        print("status_code: ", r.status_code)
        print("content: ", r.content)
        assert r.status_code == 200 or r.status_code == 409  # might already exist
