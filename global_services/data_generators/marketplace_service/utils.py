import requests

from ....global_services.data_generators.singletons import general_settings


def get_mps_apps_list(headers):
    r = requests.put(f"{general_settings.mps_host}v1/customer/portal-apps/query", json={}, headers=headers)
    r.raise_for_status()
    return r


def get_app_id_by_ead(headers, ead):
    apps = get_apps_by_namespace(headers)
    app_id = apps[ead["namespace"]]["id"]
    return app_id


def get_apps_by_namespace(headers, status=["LISTED"]):
    portal_apps_tmp = get_mps_apps_list(headers).json()
    apps_by_namespace = {}
    for portal_app in portal_apps_tmp["items"]:
        if portal_app["status"] in status:
            active_apps = portal_app["active_app_views"]
            for active_app_api in active_apps:
                if active_app_api in active_apps and active_apps[active_app_api]:
                    active_app_api_namespace = active_apps[active_app_api]["app"]["ead"]["namespace"]
                    apps_by_namespace[active_app_api_namespace] = active_apps[active_app_api]["app"]
    return apps_by_namespace


def get_apps_by_id(headers, status=["LISTED"], app_versions=["v1", "v2", "v3"]):
    portal_apps_tmp = get_mps_apps_list(headers).json()
    apps_by_id = {}
    for portal_app in portal_apps_tmp["items"]:
        if portal_app["status"] in status:
            active_apps = portal_app["active_app_views"]
            for active_app_api in active_apps:
                if active_app_api in app_versions:
                    if active_app_api in active_apps and active_apps[active_app_api]:
                        app_id = active_apps[active_app_api]["app"]["id"]
                        apps_by_id[app_id] = active_apps[active_app_api]["app"]
    return apps_by_id
