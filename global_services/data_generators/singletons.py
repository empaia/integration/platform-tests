from .login_manager import LoginManager
from .settings import CaseSeparation, ComposeSettings, GeneralSettings, MultiUserSettings

compose_settings = ComposeSettings()
general_settings = GeneralSettings()
login_manager = LoginManager()
multi_user_settings = MultiUserSettings()
case_separation = CaseSeparation()
