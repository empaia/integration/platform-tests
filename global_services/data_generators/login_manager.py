import datetime
import re
from base64 import urlsafe_b64encode
from hashlib import sha256
from random import choices
from string import ascii_letters, digits
from urllib.parse import parse_qs, urlparse

import jwt
import requests
from bs4 import BeautifulSoup
from pydantic import AnyHttpUrl
from pydantic_settings import BaseSettings, SettingsConfigDict


class LoginBaseSettings(BaseSettings):
    wbs_organization_id: str = "dummy_org"
    model_config = SettingsConfigDict(extra="allow", env_file=".env", env_prefix="LOGIN_")


class ModeSettings(LoginBaseSettings):
    auth_mode: str


class OffAuthSettings(LoginBaseSettings):
    patho_user_sub: str
    mta_user_sub: str
    wbs_client_id: str


class EmpaiaAuthSettings(LoginBaseSettings):
    login_idp_url: AnyHttpUrl

    wbs_client_id: str
    wbs_client_secret: str
    jes_client_id: str = ""
    jes_client_secret: str = ""
    as_client_id: str
    as_client_secret: str

    wbc_client_id: str
    patho_user_name: str
    patho_user_password: str

    mdc_client_id: str
    mta_user_name: str
    mta_user_password: str


class KeycloakAuthSettings(LoginBaseSettings):
    login_idp_url: AnyHttpUrl

    wbs_client_id: str
    wbs_client_secret: str
    jes_client_id: str = ""
    jes_client_secret: str = ""
    as_client_id: str
    as_client_secret: str

    wbc_client_id: str
    patho_user_name: str
    patho_user_password: str

    mdc_client_id: str
    mta_user_name: str
    mta_user_password: str


class LoginManager:
    _settings_map = {
        "off": OffAuthSettings,
        "empaia": EmpaiaAuthSettings,
        "keycloak": KeycloakAuthSettings,
    }

    def __init__(self):
        mode_settings = ModeSettings()
        self.auth_mode = mode_settings.auth_mode
        self.settings = self._settings_map[self.auth_mode]()
        self._well_known = None

        self._wbs_client_tokens = None
        self._wbs_client_best_before = None

        self._jes_client_tokens = None
        self._jes_client_best_before = None

        self._as_client_tokens = None
        self._as_client_best_before = None

        self._patho_user_tokens = None
        self._patho_user_best_before = None

        self._mta_user_tokens = None
        self._mta_user_best_before = None

    @property
    def well_known(self):
        if self._well_known is not None:
            return self._well_known
        idp_url = self.settings.login_idp_url
        well_known_url = f"{idp_url}/.well-known/openid-configuration"
        r = requests.get(well_known_url)
        r.raise_for_status()
        self._well_known = r.json()
        return self._well_known

    @property
    def token_url(self):
        return self.well_known["token_endpoint"]

    @property
    def auth_url(self):
        return self.well_known["authorization_endpoint"]

    def _client_credentials_flow(self, client_id, client_secret):
        data = {
            "grant_type": "client_credentials",
            "client_id": client_id,
            "client_secret": client_secret,
        }
        r = requests.post(self.token_url, data=data)
        data = r.json()
        token = data["access_token"]
        return token, LoginManager._decode(token)

    def _access_code_flow(self, client_id, user_name, user_password):
        # retreive first access code
        state = "".join(choices(ascii_letters + digits, k=16))
        code_verifier = "".join(choices(ascii_letters + digits + "-._~", k=128))
        m = sha256()
        m.update(code_verifier.encode("ascii"))
        code_challenge = urlsafe_b64encode(m.digest()).decode("ascii")
        code_challenge = code_challenge.rstrip("=")

        params = {
            "client_id": client_id,
            "response_type": "code",
            "redirect_uri": "http://localhost",
            "state": state,
            "code_challenge": code_challenge,
            "code_challenge_method": "S256",
        }
        headers = {"Content-type": "application/json"}
        r = requests.get(self.auth_url, params=params, headers=headers, allow_redirects=False)
        print("Retrieve First Access Code:", r.status_code)

        if self.auth_mode == "keycloak":
            assert r.status_code == 200
            cookie = r.headers["Set-Cookie"]

            matches = re.findall('"loginAction": "(.*?)"', r.text)
            if matches and len(matches) > 0:
                form_action = matches[0]
            else:
                soup = BeautifulSoup(r.text, "html.parser")
                kc_form_wrapper = soup.find_all(id="kc-form-wrapper")
                assert len(kc_form_wrapper) == 1
                form_action = kc_form_wrapper[0].form.get("action")

            # retrieve second access code
            r = requests.post(
                url=form_action,
                data={
                    "username": user_name,
                    "password": user_password,
                },
                headers={"Cookie": cookie},
                allow_redirects=False,
            )

        else:
            assert r.status_code == 302
            location = urlparse(r.headers["Location"])
            params = parse_qs(location.query)

            # retrieve second access code
            data = {
                "client_id": client_id,
                "execution": params["execution"][0],
                "tab_id": params["tab_id"][0],
                "session_state": params["session_state"][0],
                "code": params["access_code"][0],
                "username": user_name,
                "password": user_password,
            }
            r = requests.post(self.auth_url, data=data, allow_redirects=False)

        print("Retrieve Second Access Code:", r.status_code)
        assert r.status_code == 302
        location = urlparse(r.headers["Location"])
        params = parse_qs(location.query)

        # retrieve access token
        data = {
            "grant_type": "authorization_code",
            "client_id": client_id,
            "code": params["code"][0],
            "redirect_uri": "http://localhost",
            "code_verifier": code_verifier,
        }
        r = requests.post(self.token_url, data=data)
        r.raise_for_status()
        data = r.json()
        token = data["access_token"]

        return token, LoginManager._decode(token)

    @staticmethod
    def _decode(token):
        return jwt.decode(token, options={"verify_signature": False})

    @staticmethod
    def _calc_best_before(tokens):
        _, decoded_token = tokens
        exp = datetime.datetime.fromtimestamp(decoded_token["exp"])
        iat = datetime.datetime.fromtimestamp(decoded_token["iat"])
        delta = exp - iat
        best_before = datetime.datetime.now() + delta / 2
        return best_before

    @property
    def wbs_client_tokens(self):
        if self._wbs_client_tokens is not None and datetime.datetime.now() < self._wbs_client_best_before:
            return self._wbs_client_tokens

        self._wbs_client_tokens = self._client_credentials_flow(
            self.settings.wbs_client_id, self.settings.wbs_client_secret
        )
        self._wbs_client_best_before = self._calc_best_before(self._wbs_client_tokens)
        return self._wbs_client_tokens

    @property
    def jes_client_tokens(self):
        if self._jes_client_tokens is not None and datetime.datetime.now() < self._jes_client_best_before:
            return self._jes_client_tokens

        self._jes_client_tokens = self._client_credentials_flow(
            self.settings.jes_client_id, self.settings.jes_client_secret
        )
        self._jes_client_best_before = self._calc_best_before(self._jes_client_tokens)
        return self._jes_client_tokens

    @property
    def as_client_tokens(self):
        if self._as_client_tokens is not None and datetime.datetime.now() < self._as_client_best_before:
            return self._as_client_tokens

        self._as_client_tokens = self._client_credentials_flow(
            self.settings.as_client_id, self.settings.as_client_secret
        )
        self._as_client_best_before = self._calc_best_before(self._as_client_tokens)
        return self._as_client_tokens

    @property
    def patho_user_tokens(self):
        if self._patho_user_tokens is not None and datetime.datetime.now() < self._patho_user_best_before:
            return self._patho_user_tokens

        self._patho_user_tokens = self._access_code_flow(
            self.settings.wbc_client_id, self.settings.patho_user_name, self.settings.patho_user_password
        )
        self._patho_user_best_before = self._calc_best_before(self._patho_user_tokens)
        return self._patho_user_tokens

    @property
    def mta_user_tokens(self):
        if self._mta_user_tokens is not None and datetime.datetime.now() < self._mta_user_best_before:
            return self._mta_user_tokens

        self._mta_user_tokens = self._access_code_flow(
            self.settings.mdc_client_id, self.settings.mta_user_name, self.settings.mta_user_password
        )
        self._mta_user_best_before = self._calc_best_before(self._mta_user_tokens)
        return self._mta_user_tokens

    def patho_user(self):
        if self.auth_mode == "off":
            return {"user-id": self.settings.patho_user_sub}
        if self.auth_mode == "keycloak":
            token, decoded_token = self.patho_user_tokens
            return {
                "user-id": decoded_token["sub"],
                "Authorization": f"Bearer {token}",
            }
        if self.auth_mode == "empaia":
            token, decoded_token = self.patho_user_tokens
            return {
                "user-id": decoded_token["sub"],
                "Authorization": f"Bearer {token}",
            }

    def mta_user(self):
        if self.auth_mode == "off":
            return {"user-id": self.settings.mta_user_sub}
        if self.auth_mode == "keycloak":
            token, decoded_token = self.mta_user_tokens
            return {
                "user-id": decoded_token["sub"],
                "Authorization": f"Bearer {token}",
            }
        if self.auth_mode == "empaia":
            token, decoded_token = self.mta_user_tokens
            return {
                "user-id": decoded_token["sub"],
                "Authorization": f"Bearer {token}",
            }

    def wbs_client(self):
        if self.auth_mode == "off":
            return {"organization-id": self.settings.wbs_organization_id}
        # Get ORG_ID from token, when auth service is ready
        if self.auth_mode == "keycloak":
            token, _decoded_token = self.wbs_client_tokens
            return {
                "organization-id": self.settings.wbs_organization_id,
                "Authorization": f"Bearer {token}",
            }
        if self.auth_mode == "empaia":
            token, _decoded_token = self.wbs_client_tokens
            return {
                "organization-id": self.settings.wbs_organization_id,
                "Authorization": f"Bearer {token}",
            }

    def jes_client(self):
        if self.auth_mode == "off":
            return {}
        if self.auth_mode == "keycloak":
            token, _ = self.jes_client_tokens
            return {"Authorization": f"Bearer {token}"}
        if self.auth_mode == "empaia":
            token, _ = self.jes_client_tokens
            return {"Authorization": f"Bearer {token}"}

    def as_client(self):
        if self.auth_mode == "off":
            return {}
        if self.auth_mode == "keycloak":
            token, _ = self.as_client_tokens
            return {"Authorization": f"Bearer {token}"}
        if self.auth_mode == "empaia":
            token, _ = self.as_client_tokens
            return {"Authorization": f"Bearer {token}"}
