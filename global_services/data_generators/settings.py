from pydantic import AnyHttpUrl
from pydantic_settings import BaseSettings, SettingsConfigDict


class ComposeSettings(BaseSettings):
    host_path_to_wsis: str
    model_config = SettingsConfigDict(extra="allow", env_file=".env", env_prefix="COMPOSE_")


class GeneralSettings(BaseSettings):
    enable_mps_mock: bool
    enable_aaa_mock: bool
    aaa_openapi_url_suffix: str = "/openapi.json"
    stage: str
    us_host: AnyHttpUrl
    idm_host: AnyHttpUrl
    wbs_host: AnyHttpUrl
    mds_host: AnyHttpUrl
    jes_host: AnyHttpUrl
    mps_host: AnyHttpUrl
    aaa_host: AnyHttpUrl
    as_host: AnyHttpUrl
    app_api: AnyHttpUrl
    script_path_to_wsis: str = None
    model_config = SettingsConfigDict(extra="allow", env_file=".env", env_prefix="PYTEST_")


class MultiUserSettings(BaseSettings):
    disable_multi_user: bool = False
    model_config = SettingsConfigDict(extra="allow", env_file=".env", env_prefix="WBS_")


class CaseSeparation(BaseSettings):
    enable_annot_case_data_partitioning: bool = False
    model_config = SettingsConfigDict(extra="allow", env_file=".env", env_prefix="WBS_")
