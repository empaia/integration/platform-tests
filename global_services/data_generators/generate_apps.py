import argparse
import json
from pathlib import Path

from .aaa_service.utils_mock import init_orgas_and_apps
from .marketplace_service.utils import get_apps_by_namespace
from .marketplace_service.utils_mock import init_configurations, init_ui_configurations
from .singletons import login_manager

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--apps-file", help="Path to apps json file.")
    args = parser.parse_args()
    apps_file = Path(f"{Path.cwd()}/{args.apps_file}").expanduser()

    with open(apps_file, encoding="UTF-8") as f:
        apps_tmp = json.load(f)["APPS"]

    apps = []
    configurations_by_namespace = {}
    ui_configurations_by_namespace = {}
    for app_tmp in apps_tmp:
        app = {}
        for api_version in app_tmp:
            app_raw = app_tmp[api_version]
            ead_file = Path(f"{Path(__file__).parent}/{app_raw['ead']}")
            with open(ead_file, encoding="UTF-8") as f:
                ead = json.load(f)
            api_version_app = {"api_version": api_version, "ead": ead, "registry": app_raw["registry"]}
            api_version_app["has_frontend"] = "has_frontend" in app_raw and app_raw["has_frontend"] is True
            if "app_ui_url" in app_raw:
                api_version_app["app_ui_url"] = app_raw["app_ui_url"]
            if app_raw["configuration"] is not None:
                conf_file = Path(f"{Path(__file__).parent}/{app_raw['configuration']}")
                with open(conf_file, encoding="UTF-8") as f:
                    conf = json.load(f)
                configurations_by_namespace[ead["namespace"]] = conf
            if "app_ui_config_file" in app_raw:
                app_ui_config_file = Path(f"{Path(__file__).parent}/{app_raw['app_ui_config_file']}")
                with open(app_ui_config_file, encoding="UTF-8") as f:
                    ui_conf = json.load(f)
                ui_configurations_by_namespace[ead["namespace"]] = ui_conf
            app[api_version] = api_version_app

        apps.append(app)

    init_orgas_and_apps(apps)
    headers = login_manager.wbs_client()
    apps_by_namespace = get_apps_by_namespace(headers)
    init_configurations(apps_by_namespace, configurations_by_namespace)
    init_ui_configurations(apps_by_namespace, ui_configurations_by_namespace)
