from uuid import uuid4

import requests

from ....global_services.data_generators.marketplace_service.utils_mock import generate_app_json, mps_post_app
from ....global_services.data_generators.singletons import general_settings


def generate_organization_json(name: str, keycloak_id: str):
    data = {
        "organization_id": keycloak_id,
        "organization_name": name,
    }
    return data


def aaa_post_organization(data):
    url = f"{general_settings.aaa_host}api/v2/custom-mock/organization"
    r = requests.post(url, json=data)
    return r


def init_orgas_and_apps(apps):
    for app in apps:
        orga_name = None
        if "v3" in app:
            orga_name = app["v3"]["ead"]["namespace"].split(".")[-4]
        elif "v2" in app:
            orga_name = app["v2"]["ead"]["namespace"].split(".")[-3]
        elif "v1" in app:
            orga_name = app["v1"]["ead"]["namespace"].split(".")[-3]
        aaa_orga = generate_organization_json(name=orga_name, keycloak_id=str(uuid4()))
        r = aaa_post_organization(aaa_orga)
        print("POST AAA - organization:")
        print("payload: ", aaa_orga)
        print("status_code: ", r.status_code)
        print("content: ", r.content)
        mps_app = generate_app_json(app, aaa_orga)
        r = mps_post_app(mps_app)
        print("POST MPS - app:")
        print("payload: ", mps_app)
        print("status_code: ", r.status_code)
        print("content: ", r.content)
