DEFAULT_CASE_DESCRIPTION = (
    "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut "
    "labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores "
    "et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem "
    "ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et "
    "dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. "
    "Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."
)
MDS_HOST_DOCKER_INTERNAL = "http://medical-data-service:5000"
MOUNTED_WSIS_ROOT_DIR = "/data/"  # of wsi-service


def ead_to_job(ead: dict, app_id):
    job = {}
    job["app_id"] = app_id
    return job
