from copy import deepcopy

import requests
from sample_apps.sample_apps.test.tutorial_legacy._TA01_simple_app import v2 as app

from ....global_services.data_generators.marketplace_service import utils as utils_mps
from ....global_services.data_generators.singletons import general_settings, login_manager
from ..case_data import get_test_wsis
from ..commons import ead_to_job
from . import utils, utils_dads

# Constants
WSI_X_MAX = 100000
WSI_Y_MAX = 100000
RECTS = [  # upper left of my_rectangles
    [100, 1000],
    [2000, 5000],
    [7000, 9000],
]
RECTSW = 1600
RECTSH = 900

BASE_RES = 250
RECTS_MPP_C = BASE_RES * 2**4
RECTS_MPP_V = [BASE_RES * 2**2, BASE_RES * 2**8]


def _gen_inputs(
    ead,
    user_id,
    slide_id,
):
    inputs_by_key = {}
    inputs_type_id = []
    ead = deepcopy(ead)

    # my_wsi
    inputs_by_key["my_wsi"] = ead["inputs"]["my_wsi"]
    inputs_by_key["my_wsi"]["id"] = slide_id
    inputs_type_id.append(("wsi", slide_id))

    # my_rectangle
    data = utils_dads.generate_rectangle_json(
        slide_id, user_id, "my_rectangle", "user", [3000, 3000], RECTSW, RECTSH, RECTS_MPP_C, RECTS_MPP_V
    )
    headers = login_manager.wbs_client()
    r = utils_dads.mds_post_annotation(headers, data)
    print("DAD_Service - POST Recangle, status_code: ", r.status_code)
    assert r.status_code == 201
    inputs_by_key["my_rectangle"] = r.json()
    inputs_type_id.append(("rectangle", r.json()["id"]))

    return inputs_by_key, inputs_type_id


def generate(
    user_id,
    slide_id,
    ex_id,
    set_job_ready=False,
):
    ead = app.ead
    headers = login_manager.wbs_client()
    in_job = ead_to_job(ead, utils_mps.get_app_id_by_ead(headers, ead))

    app_id = in_job["app_id"]

    inputs_by_key, inputs_type_id = _gen_inputs(ead, user_id, slide_id)
    in_job, job_id, _token = utils.create_job(ead, in_job, user_id, ex_id, app_id)
    utils.add_inputs_to_job(job_id, inputs_by_key)
    utils.lock_to_job(job_id, inputs_type_id)
    job_status = "ERROR"
    if set_job_ready:
        job_status = "READY"
    utils.set_js_job_statuses(job_id, [job_status])
    job = utils.get_job(job_id)
    return job, job_id


def check_containerized_app_output(job_id):
    """
    Checks DADS output :
    - (1) this [_gen_inputs] are used as inputs
    - (2) the containtized app was executed using (1)
    """
    url = f"{general_settings.mds_host}v1/primitives/query"
    query = {"jobs": [job_id]}
    headers = login_manager.wbs_client()
    r = requests.put(url, json=query, headers=headers)
    print("MDS - PUT primitives query: ", r.status_code)
    assert r.status_code == 200
    assert r.json()["item_count"] == 1
    assert r.json()["items"][0]["value"] == 42


def main():
    mta_user_id = login_manager.mta_user()["user-id"]
    patho_user_id = login_manager.patho_user()["user-id"]

    slide_storage_adress = get_test_wsis()[0]

    case_id, slide_ids, _npp_ranges = utils.new_case_with_slides(
        mta_user_id,
        [slide_storage_adress],
        f"{__file__.split('/')[-1]}",
    )
    ex_id = utils.add_examination_to_case(
        patho_user_id,
        case_id,
    )
    _in_job, _job_id = generate(
        patho_user_id,
        slide_ids[0],
        ex_id,
    )


if __name__ == "__main__":
    main()
