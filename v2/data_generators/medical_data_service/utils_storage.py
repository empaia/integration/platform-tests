import uuid

import requests

from ....global_services.data_generators.singletons import general_settings


def mds_register_wsi(headers, slide_id, main_path: str, secondary_paths: list = None):
    main_storage_adress = {"storage_address_id": str(uuid.uuid4()), "path": main_path}

    secondary_storage_addresses = []
    if secondary_storage_addresses:
        secondary_storage_addresses = [
            {"storage_address_id": str(uuid.uuid4()), "path": sec_path} for sec_path in secondary_paths
        ]

    data = {"main_storage_address": main_storage_adress, "secondary_storage_addresses": secondary_storage_addresses}
    url = f"{general_settings.mds_host}private/v1/slides/{slide_id}/storage"
    r = requests.put(url, json=data, headers=headers)
    return r


def mds_get_wsi_storage(headers, slide_id):
    url = f"{general_settings.mds_host}private/v1/slides/{slide_id}/storage"
    r = requests.get(url, headers=headers)
    return r


def mds_delete_slide_storage(headers, slide_id):
    url = f"{general_settings.mds_host}private/v1/slides/{slide_id}/storage"
    r = requests.delete(url, headers=headers)
    return r
