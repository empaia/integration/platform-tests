import requests

from ....global_services.data_generators.singletons import general_settings


def generate_rectangle_json(
    reference_id,
    creator_id,
    name="my_rectangle",
    creator_type="user",
    upper_left=[1000, 2000],
    width=300,
    height=500,
    npp_created=250,
    npp_viewing=[250, 250 * 2**3],
):
    data = {
        "name": name,
        "type": "rectangle",
        "upper_left": upper_left,
        "width": width,
        "height": height,
        "reference_id": reference_id,
        "reference_type": "wsi",
        "creator_id": creator_id,
        "creator_type": creator_type,
        "npp_created": npp_created,
        "npp_viewing": npp_viewing,
    }
    return data


def generate_hexagon_json(
    reference_id,
    creator_id,
    name="my_rectangle",
    creator_type="user",
    start=[1000, 2000],
    npp_created=499,
    npp_viewing=[499, 250 * 2**2],
):
    coords = [start]
    coords.append([start[0] + 50, start[1] + 50])
    coords.append([start[0] + 100, start[1] + 50])
    coords.append([start[0] + 150, start[1] + 0])
    coords.append([start[0] + 100, start[1] - 50])
    coords.append([start[0] + 50, start[1] - 50])
    data = {
        "name": name,
        "type": "polygon",
        "reference_id": reference_id,
        "reference_type": "wsi",
        "creator_id": creator_id,
        "creator_type": creator_type,
        "coordinates": coords,
        "npp_created": npp_created,
        "npp_viewing": npp_viewing,
    }
    return data


def generate_polygon_json(
    reference_id,
    creator_id,
    name="my_polygon",
    creator_type="user",
    coordinates=[[100, 100], [100, 1000], [1000, 100]],
    npp_created=499,
    npp_viewing=[499, 250 * 2**2],
):
    data = {
        "name": name,
        "type": "polygon",
        "reference_id": reference_id,
        "reference_type": "wsi",
        "creator_id": creator_id,
        "creator_type": creator_type,
        "coordinates": coordinates,
        "npp_created": npp_created,
        "npp_viewing": npp_viewing,
    }
    return data


def generate_circle_json(
    reference_id,
    creator_id,
    name="my_circle",
    creator_type="user",
    center=[1000, 1000],
    radius=[500],
    npp_created=499,
    npp_viewing=[499, 250 * 2**2],
):
    data = {
        "name": name,
        "type": "circle",
        "reference_id": reference_id,
        "reference_type": "wsi",
        "creator_id": creator_id,
        "creator_type": creator_type,
        "center": center,
        "radius": radius,
        "npp_created": npp_created,
        "npp_viewing": npp_viewing,
    }
    return data


def remove_keys(dict_ref, *key_names):
    for key in key_names:
        if key in dict_ref:
            del dict_ref[key]


def mds_post_annotation(headers, data):
    url = f"{general_settings.mds_host}v1/annotations"
    r = requests.post(url, json=data, headers=headers)
    return r


def mds_get_annotation(headers, annot_id):
    url = f"{general_settings.mds_host}v1/annotations/{annot_id}"
    r = requests.get(url, headers=headers)
    return r


def mds_lock_any(headers, type_to_lock, id_to_lock, job_id):
    url = f"{general_settings.mds_host}v1/jobs/{job_id}/lock/{type_to_lock}/{id_to_lock}"
    r = requests.put(url, json={}, headers=headers)
    return r


def generate_collection_json(
    reference_id, reference_type, creator_id, name, creator_type="user", item_type="rectangle", items=[]
):
    data = {
        "name": name,
        "creator_id": creator_id,
        "creator_type": creator_type,
        "item_type": item_type,
        "type": "collection",
    }
    if items is not None and len(items) > 0:
        data["items"] = items
    if reference_id is not None:
        data["reference_id"] = reference_id
        data["reference_type"] = reference_type
    return data


def mds_post_collection(headers, data):
    url = f"{general_settings.mds_host}v1/collections"
    r = requests.post(url, json=data, headers=headers)
    return r


def mds_post_class(headers, data):
    url = f"{general_settings.mds_host}v1/classes"
    r = requests.post(url, json=data, headers=headers)
    return r


def generate_point_json(
    reference_id,
    creator_id,
    name="my_point",
    creator_type="user",
    coordinates=[1000, 2000],
    npp_created=250,
    npp_viewing=[250, 250 * 2**3],
):
    data = {
        "name": name,
        "type": "point",
        "coordinates": coordinates,
        "reference_id": reference_id,
        "reference_type": "wsi",
        "creator_id": creator_id,
        "creator_type": "user",
        "npp_created": npp_created,
        "npp_viewing": npp_viewing,
    }
    return data


def generate_class_json(
    reference_id, creator_id, creator_type="user", value="org.empaia.my_vendor.my_app.v1.classes.tumor"
):
    data = {
        "value": value,
        "reference_id": reference_id,
        "reference_type": "annotation",
        "creator_id": creator_id,
        "creator_type": creator_type,
        "type": "class",
    }
    return data


def generate_primitive(
    reference_id,
    creator_id,
    name="my_primitive",
    creator_type="user",
    prim_type="integer",
    value=42,
    reference_type=None,
):
    data = {
        "name": name,
        "type": prim_type,
        "value": value,
        "creator_id": creator_id,
        "creator_type": creator_type,
    }
    if reference_id is not None:
        data["reference_id"] = reference_id
    if reference_type is not None:
        data["reference_type"] = reference_type
    return data


def mds_post_primitive(headers, data):
    url = f"{general_settings.mds_host}v1/primitives"
    r = requests.post(url, json=data, headers=headers)
    return r


def mds_post_additional_items_to_collection(headers, collection_id, items_batch):
    url = f"{general_settings.mds_host}v1/collections/{collection_id}/items"
    r = requests.post(url, json=items_batch, headers=headers)
    return r
