import requests

from ....global_services.data_generators.singletons import general_settings


def idm_post_case(headers, mds_url, empaia_id, local_id):
    url = f"{general_settings.idm_host}v1/cases"
    data = {"mds_url": mds_url, "empaia_id": empaia_id, "local_id": local_id}
    r = requests.post(url, json=data, headers=headers)
    return r


def idm_post_slide(headers, mds_url, empaia_id, local_id):
    url = f"{general_settings.idm_host}v1/slides"
    data = {"mds_url": mds_url, "empaia_id": empaia_id, "local_id": local_id}
    r = requests.post(url, json=data, headers=headers)
    return r


def idm_delete_slide(headers, empaia_id):
    url = f"{general_settings.idm_host}v1/slides/empaia/{empaia_id}"
    r = requests.delete(url, headers=headers)
    return r
