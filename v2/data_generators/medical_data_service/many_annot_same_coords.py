import math
from random import randint

from ....global_services.data_generators.singletons import login_manager
from ..case_data import get_test_wsis
from ..medical_data_service import utils, utils_dads


def generate(
    user_id,
    case_id,
    slide_id,
    npp_ranges,
    n_batches=10,
    n_polys=100,
    same_pos=False,
):
    # add collection of rectangles
    items = []
    items.append(
        utils_dads.generate_rectangle_json(
            reference_id=slide_id,
            creator_id=user_id,
            creator_type="user",
            name="rect_1",
            upper_left=[1000, 1000],
            width=20000,
            height=20000,
            npp_created=npp_ranges[-1],
            npp_viewing=None,
        )
    )
    items.append(
        utils_dads.generate_rectangle_json(
            reference_id=slide_id,
            creator_id=user_id,
            creator_type="user",
            name="rect_2",
            upper_left=[1000, 21000],
            width=20000,
            height=20000,
            npp_created=npp_ranges[-1],
            npp_viewing=None,
        )
    )
    items.append(
        utils_dads.generate_rectangle_json(
            reference_id=slide_id,
            creator_id=user_id,
            creator_type="user",
            name="rect_3",
            upper_left=[21000, 10000],
            width=20000,
            height=20000,
            npp_created=npp_ranges[-1],
            npp_viewing=None,
        )
    )
    data = utils_dads.generate_collection_json(None, None, user_id, "rectangles", "user", "rectangle", items)
    headers = login_manager.wbs_client()
    r = utils_dads.mds_post_collection(headers, data)
    print("DAD_Service - POST Collection, status_code: ", r.status_code)
    assert r.status_code == 201
    rectangles = r.json()

    # rois
    items = []
    reference_id = rectangles["items"][0]["id"]
    items.append(utils_dads.generate_class_json(reference_id, user_id, "user", "org.empaia.global.v1.classes.roi"))
    reference_id = rectangles["items"][1]["id"]
    items.append(utils_dads.generate_class_json(reference_id, user_id, "user", "org.empaia.global.v1.classes.roi"))
    reference_id = rectangles["items"][2]["id"]
    items.append(utils_dads.generate_class_json(reference_id, user_id, "user", "org.empaia.global.v1.classes.roi"))
    data = utils_dads.generate_collection_json(None, None, user_id, "rois", "job", "class", items)
    headers = login_manager.wbs_client()
    r = utils_dads.mds_post_collection(headers, data)
    print("DAD_Service - POST Collection, status_code: ", r.status_code)
    print(r.content)
    assert r.status_code == 201

    # colleciton of collections
    items = []
    for r in rectangles["items"]:
        items.append(
            utils_dads.generate_collection_json(
                name="inner_polygons",
                reference_id=r["id"],
                reference_type="annotation",
                creator_id=user_id,
                creator_type="user",
                item_type="polygon",
                items=None,
            )
        )
    data = utils_dads.generate_collection_json(
        name="polygons",
        reference_id=None,
        reference_type=None,
        creator_id=user_id,
        creator_type="user",
        item_type="collection",
        items=items,
    )
    headers = login_manager.wbs_client()
    r = utils_dads.mds_post_collection(headers, data)
    print("DAD_Service - POST Collection, status_code: ", r.status_code)
    assert r.status_code == 201
    col_of_cols = r.json()

    # add polygons to inner collections
    for i in range(len(col_of_cols["items"])):
        col = list(col_of_cols["items"])[i]
        rect = list(rectangles["items"])[i]
        for b in range(n_batches):
            items = {"items": []}
            for p in range(n_polys):
                if not same_pos:
                    x = randint(0, rect["width"]) + rect["upper_left"][0]
                    y = randint(0, rect["height"]) + rect["upper_left"][1]
                else:
                    x = (rect["width"] + rect["upper_left"][0]) // 2
                    y = (rect["height"] + rect["upper_left"][1]) // 2
                polygon = utils_dads.generate_hexagon_json(
                    reference_id=slide_id,
                    creator_id=user_id,
                    creator_type="user",
                    name=f"polygon_{b}_{p}",
                    start=[x, y],
                    npp_created=npp_ranges[0],
                    npp_viewing=[npp_ranges[0], npp_ranges[1]],
                )
                items["items"].append(polygon)
            headers = login_manager.wbs_client()
            r = utils_dads.mds_post_additional_items_to_collection(headers, col["id"], items)
            print("DAD_Service - Extend Collection, status_code: ", r.status_code)
            assert r.status_code == 201


def main(n_batches=10, n_polys=10):
    mta_user_id = login_manager.mta_user()["user-id"]
    patho_user_id = login_manager.patho_user()["user-id"]

    n_slides = 5
    slides = []
    wsis = get_test_wsis()
    for _i in range(math.ceil(n_slides / len(wsis))):
        slides += wsis

    case_id, slide_ids, npp_ranges = utils.new_case_with_slides(
        mta_user_id,
        slides[0:5],
        f"{__file__.split('/')[-1]}",
    )
    # 1st slide 3x10k annotation max npp_viewing
    generate(
        patho_user_id,
        case_id,
        slide_ids[0],
        npp_ranges[0],
        n_batches=n_batches,
        n_polys=n_polys,
    )
    # 2nd slide 3x10k annotation lower npp_viewing
    generate(
        patho_user_id,
        case_id,
        slide_ids[1],
        npp_ranges[1],
        n_batches=n_batches,
        n_polys=n_polys,
    )
    # 3rd slide 3x10k annotation even lower npp_viewing
    generate(
        patho_user_id,
        case_id,
        slide_ids[2],
        npp_ranges[2],
        n_batches=n_batches,
        n_polys=n_polys,
    )
    # 3rd slide 3x10k annotation even more lower npp_viewing
    generate(
        patho_user_id,
        case_id,
        slide_ids[3],
        npp_ranges[3],
        n_batches=n_batches,
        n_polys=n_polys,
    )
    # 4th slide 3x30k annotation same position
    generate(
        patho_user_id,
        case_id,
        slide_ids[4],
        npp_ranges[3],
        n_batches=n_batches,
        n_polys=n_polys,
        same_pos=True,
    )


if __name__ == "__main__":
    main()
