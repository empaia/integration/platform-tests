from copy import deepcopy

import requests
from sample_apps.sample_apps.test.tutorial_legacy._TA06_large_collections import v2 as app

from ....global_services.data_generators.marketplace_service import utils as utils_mps
from ....global_services.data_generators.singletons import general_settings, login_manager
from ..case_data import get_test_wsis
from ..commons import ead_to_job
from . import utils, utils_dads

# Constants
WSI_X_MAX = 100000
WSI_Y_MAX = 100000
RECTS = [  # upper left of my_rectangles
    [100, 1000],
    [2000, 5000],
    [7000, 9000],
]
RECTSW = 1600
RECTSH = 900


def _gen_inputs(ead, user_id, slide_id, npp_ranges):
    inputs_by_key = {}
    inputs_type_id = []
    ead = deepcopy(ead)

    # my_wsi
    inputs_by_key["my_wsi"] = ead["inputs"]["my_wsi"]
    inputs_by_key["my_wsi"]["id"] = slide_id
    inputs_type_id.append(("wsi", slide_id))

    # my_rectangles
    items = []
    items.append(
        utils_dads.generate_rectangle_json(
            slide_id,
            user_id,
            "my_rectangle",
            "user",
            [RECTS[0][0], RECTS[0][1]],
            RECTSW,
            RECTSH,
            npp_ranges[-1],
            [npp_ranges[0], npp_ranges[-1]],
        )
    )
    items.append(
        utils_dads.generate_rectangle_json(
            slide_id,
            user_id,
            "my_rectangle",
            "user",
            [RECTS[1][0], RECTS[1][1]],
            RECTSW,
            RECTSH,
            npp_ranges[-1],
            [npp_ranges[0], npp_ranges[-1]],
        )
    )
    items.append(
        utils_dads.generate_rectangle_json(
            slide_id,
            user_id,
            "my_rectangle",
            "user",
            [RECTS[2][0], RECTS[2][1]],
            RECTSW,
            RECTSH,
            npp_ranges[-1],
            [npp_ranges[0], npp_ranges[-1]],
        )
    )
    data = utils_dads.generate_collection_json(None, None, user_id, "my_rectangles", "user", "rectangle", items)
    headers = login_manager.wbs_client()
    r = utils_dads.mds_post_collection(headers, data)
    print("DAD_Service - POST Collection, status_code: ", r.status_code)
    assert r.status_code == 201
    inputs_by_key["my_rectangles"] = r.json()
    inputs_type_id.append(("collection", r.json()["id"]))

    return inputs_by_key, inputs_type_id


def generate(
    user_id,
    slide_id,
    ex_id,
    npp_ranges,
    set_job_ready=False,
):
    ead = app.ead
    headers = login_manager.wbs_client()
    in_job = ead_to_job(ead, utils_mps.get_app_id_by_ead(headers, ead))

    app_id = in_job["app_id"]

    inputs_by_key, inputs_type_id = _gen_inputs(ead, user_id, slide_id, npp_ranges)
    in_job, job_id, _token = utils.create_job(ead, in_job, user_id, ex_id, app_id)
    utils.add_inputs_to_job(job_id, inputs_by_key)
    utils.lock_to_job(job_id, inputs_type_id)
    job_status = "ERROR"
    if set_job_ready:
        job_status = "READY"
    utils.set_js_job_statuses(job_id, [job_status])
    job = utils.get_job(job_id)
    return job, job_id


def check_containerized_app_output(job_id):
    """
    Checks DADS output :
    - (1) this [_gen_inputs] are used as inputs
    - (2) the containtized app was executed using (1)
    """
    url = f"{general_settings.mds_host}v1/annotations/query"
    query = {"jobs": [job_id], "creators": [job_id]}
    params = {"with_classes": True, "limit": 0}
    headers = login_manager.wbs_client()
    r = requests.put(url, json=query, headers=headers, params=params)
    print("MDS - PUT annotations query: ", r.status_code)
    assert r.status_code == 200
    assert r.json()["item_count"] == 12345 * 3


def main():
    mta_user_id = login_manager.mta_user()["user-id"]
    patho_user_id = login_manager.patho_user()["user-id"]

    slide_storage_adress = get_test_wsis()[0]

    case_id, slide_ids, _npp_ranges = utils.new_case_with_slides(
        mta_user_id,
        [slide_storage_adress],
        f"{__file__.split('/')[-1]}",
    )
    ex_id = utils.add_examination_to_case(
        patho_user_id,
        case_id,
    )
    _in_job, _job_id = generate(patho_user_id, slide_ids[0], ex_id, [499, 3000, 5000])


if __name__ == "__main__":
    main()
