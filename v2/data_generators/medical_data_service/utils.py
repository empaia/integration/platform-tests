import os
from pprint import pprint
from random import randint

from ....global_services.data_generators.singletons import login_manager
from ..commons import DEFAULT_CASE_DESCRIPTION, MDS_HOST_DOCKER_INTERNAL
from . import utils_case, utils_dads, utils_examination, utils_idm, utils_job, utils_storage, utils_wsi


def ead_type_to_api_path(ead_type: str):
    if ead_type in ["point", "arrow", "line", "rectangle", "polygon", "circle"]:
        return "annotations"
    elif ead_type in ["integer", "float", "bool", "string"]:
        return "primitives"
    elif ead_type == "collection":
        return "collections"
    elif ead_type == "class":
        return "classes"
    elif ead_type == "wsi":
        return "slides"
    else:
        raise TypeError(f"Invalid Type: {ead_type}")


def add_examination_to_case(
    user_id,
    case_id,
    close_open_examination=True,
):
    headers = login_manager.wbs_client()
    # Close any open examination
    if close_open_examination:
        utils_examination.mds_close_open_examinations_for_case(headers, case_id)
    # POST examination
    headers = login_manager.wbs_client()
    r = utils_examination.mds_post_examination(headers, case_id, user_id)
    print("Ex_Service - POST examination, status_code: ", r.status_code)
    pprint(r.json())
    assert r.status_code == 201
    ex_id = r.json()["id"]
    return ex_id


def new_case_with_slides(
    user_id: str,
    wsis: dict,
    local_case_id: str,
    description: str = DEFAULT_CASE_DESCRIPTION,
    local_slide_ids: list = None,
):
    ###########################
    # CDS #####################
    ###########################

    # POST case
    headers = login_manager.wbs_client()
    r = utils_case.mds_post_case(headers, user_id, description)
    print("CD_Service - POST case, status_code: ", r.status_code)
    pprint(r.json())
    assert r.status_code == 200
    case_id = r.json()["id"]

    # GET Case
    headers = login_manager.wbs_client()
    r = utils_case.mds_get_case(headers, case_id)
    print("CD_Service - GET case, status_code: ", r.status_code)
    assert r.status_code == 200

    # POST IDM Case
    headers = login_manager.wbs_client()
    r = utils_idm.idm_post_case(headers, MDS_HOST_DOCKER_INTERNAL, case_id, local_case_id)
    print("IDM_Service - POST case, status_code: ", r.status_code)
    pprint(r.json())
    assert r.status_code == 200

    slide_ids = []
    npp_ranges = []

    for idx, wsi in enumerate(wsis):
        address = wsi["path"]
        print(f"Processing slide at: {wsi['path']}")
        # POST Slide
        headers = login_manager.wbs_client()
        r = utils_case.mds_post_slide(
            headers, case_id, wsi.get("tissue"), wsi.get("stain"), wsi.get("block"), main_path=address
        )
        print("CD_Service - POST slide, status_code: ", r.status_code)
        assert r.status_code == 200
        slide_id = r.json()["id"]

        slide_ids.append(slide_id)

        # POST IDM Slide
        if isinstance(local_slide_ids, list):
            local_id = local_slide_ids[idx]
        else:
            local_id = address.split("/")[-2] + "_" + address.split("/")[-1] + "_" + slide_id.split("-")[0]
        local_id = local_id[-49:]
        headers = login_manager.wbs_client()
        r = utils_idm.idm_post_slide(headers, MDS_HOST_DOCKER_INTERNAL, slide_id, local_id)
        print("IDM_Service - POST slide, status_code: ", r.status_code)
        pprint(r.json())
        assert r.status_code == 200

        ###########################
        # Storage #################
        ###########################

        # validate get slide storage
        headers = login_manager.wbs_client()
        r = utils_storage.mds_get_wsi_storage(headers, slide_id)
        print("Storage_Service - GET WSI, status_code: ", r.status_code)
        assert r.status_code == 200

        ###########################
        # WSI #####################
        ###########################

        # just test get the slide
        headers = login_manager.wbs_client()
        r = utils_wsi.mds_get_wsi_info(headers, slide_id)
        print("WSI_Service - GET Info, status_code: ", r.status_code)
        print("WSI_Service - GET Info, content: ", r.content)
        assert r.status_code == 200
        pprint(r.json())
        pprint(slide_id)
        assert r.json()["id"] == slide_id
        slide_meta = r.json()
        pixel_size = slide_meta["pixel_size_nm"]
        npp_ranges_ = [pixel_size["x"] * levels["downsample_factor"] for levels in slide_meta["levels"]]
        npp_ranges = npp_ranges_

    return case_id, slide_ids, npp_ranges


def lock_to_job(
    job_id,
    params,
):
    for p in params:
        p_id = p[1]
        p_api_path = ead_type_to_api_path(p[0])
        headers = login_manager.wbs_client()
        r = utils_dads.mds_lock_any(headers, p_api_path, p_id, job_id)
        print("locking...type/id/job_id: ", p_api_path, p_id, job_id)
        print("DAD_Service - Lock, status_code: ", r.status_code)
        pprint(r.json())
        assert r.status_code == 200


def create_job(
    ead,
    in_job,
    creator_id,
    ex_id,
    app_id,
):
    in_job["creator_id"] = creator_id
    in_job["creator_type"] = "USER"

    # POST ead
    headers = login_manager.wbs_client()
    r = utils_job.put_ead(headers, ead, app_id)
    print("J_Service - POST ead, status_code: ", r.status_code)
    print("J_Service - POST ead, text: ", r.text)
    assert r.status_code == 200 or (r.status_code == 400 and "already exists" in r.json()["detail"]["cause"])

    # POST job
    headers = login_manager.wbs_client()
    r = utils_job.post_job(headers, in_job)
    print("J_Service - POST job, status_code: ", r.status_code)
    assert r.status_code == 200
    job_id = r.json()["id"]

    # GET job token
    headers = login_manager.wbs_client()
    r = utils_job.get_token(headers, job_id)
    print("J_Service - GET token, status_code: ", r.status_code)
    assert r.status_code == 200
    token = r.json()["access_token"]

    # Add to examination
    headers = login_manager.wbs_client()
    r = utils_examination.mds_put_app_to_examination(headers, ex_id, in_job["app_id"])
    print("Ex_Service - POST add app to examination, status_code: ", r.status_code)
    assert (
        r.status_code == 200
        or r.status_code == 201  # new already exists, replaces 405
        or (r.status_code == 405 and "already in" in r.json()["detail"])  # created  # deprecated
    )

    headers = login_manager.wbs_client()
    r = utils_examination.mds_put_job_to_exmaination(headers, ex_id, in_job["app_id"], job_id)
    print("Ex_Service - POST add job to examination, status_code: ", r.status_code)
    assert r.status_code == 201 or r.status_code == 200  # new created  # new already exists

    return in_job, job_id, token


def delete_job(job_id):
    headers = login_manager.wbs_client()
    r = utils_job.delete_job(headers, job_id)
    print("J_Service - DELETE job, status_code: ", r.status_code)
    assert r.status_code == 200
    data = r.json()
    assert job_id == data["id"]
    return job_id


def set_js_job_statuses(job_id, statuses):
    for status in statuses:
        headers = login_manager.wbs_client()
        r = utils_job.put_job_status(headers, job_id, status)
        print(f"J_Service - PUT job status [{status}], status_code: ", r.status_code)
        assert r.status_code == 200


def add_outputs_to_job(
    job_id,
    outputs,
):
    for out_k in outputs:
        out_v = outputs[out_k]
        headers = login_manager.wbs_client()
        r = utils_job.post_output_id_to_output(headers, job_id, out_k, out_v["id"])
        print("J_Service - PUT job add output-ID, status_code: ", r.content, r.status_code)
        assert r.status_code == 200

    headers = login_manager.wbs_client()
    r = utils_job.get_job(headers, job_id)
    print("J_Service - GET job, status_code: ", r.status_code)
    assert r.status_code == 200
    out_job = r.json()

    return out_job


def add_inputs_to_job(
    job_id,
    inputs,
):
    for in_k in inputs:
        in_v = inputs[in_k]
        headers = login_manager.wbs_client()
        r = utils_job.post_input_id_to_input(headers, job_id, in_k, in_v["id"])
        print("J_Service - PUT job add input-ID, status_code: ", r.status_code)
        pprint(r.json())
        assert r.status_code == 200

    r = utils_job.get_job(headers, job_id)
    print("J_Service - GET job, status_code: ", r.status_code)
    assert r.status_code == 200
    job = r.json()
    return job


def get_job(job_id):
    headers = login_manager.wbs_client()
    r = utils_job.get_job(headers, job_id)
    print("J_Service - GET job, status_code: ", r.status_code)
    assert r.status_code == 200
    job = r.json()
    return job


def rn_coords(x_min, y_min, x_max, y_max):
    x = randint(x_min, x_max)
    y = randint(y_min, y_max)
    return (x, y)


def delete_temp_file(filename):
    if os.path.exists(filename):
        os.remove(filename)


def simulate_dmc_delete_slide(slide_id):
    headers = login_manager.wbs_client()
    print(f"Deleting slide with id: {slide_id}")
    r = utils_storage.mds_delete_slide_storage(headers, slide_id)
    print("Storage - DELETE slide, status_code: ", r.status_code)
    assert r.status_code == 200
    utils_idm.idm_delete_slide(headers, slide_id)
    print("IDMS - DELETE slide, status_code: ", r.status_code)
    assert r.status_code == 200
    utils_case.mds_delete_slide(headers, slide_id)
    print("CDS - DELETE slide, status_code: ", r.status_code)
    assert r.status_code == 200


def simulate_dmc_delete_case(case_id):
    headers = login_manager.wbs_client()
    r = utils_case.mds_get_case(headers, case_id, with_slides=True)
    for s in r.json()["slides"]:
        simulate_dmc_delete_slide(s["id"])
    r = utils_case.mds_delete_case(headers, case_id)
    print("CDS - DELETE case, status_code: ", r.status_code)
    assert r.status_code == 200
