import requests

from ....global_services.data_generators.singletons import general_settings


def mds_post_examination(headers, case_id, creator_id):
    url = f"{general_settings.mds_host}v1/examinations"
    data = {
        "case_id": case_id,
        "creator_id": creator_id,
        "creator_type": "USER",
    }
    r = requests.post(url, json=data, headers=headers)
    return r


def mds_close_open_examinations_for_case(headers, case_id):
    r = mds_get_examination_for_case(headers, case_id)
    r.raise_for_status()
    examinations = r.json()
    for ex in examinations["items"]:
        if ex["state"].lower() == "open":
            r = mds_close_examination(headers, ex["id"])
            r.raise_for_status()


def mds_get_examination_for_case(headers, case_id):
    url = f"{general_settings.mds_host}v1/examinations/query"
    data = {
        "cases": [case_id],
    }
    r = requests.put(url, json=data, headers=headers)
    return r


def mds_close_examination(headers, ex_id):
    url = f"{general_settings.mds_host}v1/examinations/{ex_id}/state"
    data = {"state": "CLOSED"}
    r = requests.put(url, json=data, headers=headers)
    return r


def mds_put_job_to_exmaination(headers, examination_id, app_id, job_id):
    url = f"{general_settings.mds_host}v1/examinations/{examination_id}/apps/{app_id}/jobs/{job_id}/add"
    r = requests.put(url, headers=headers)
    return r


def mds_put_app_to_examination(headers, examination_id, app_id):
    url = f"{general_settings.mds_host}v1/examinations/{examination_id}/apps/{app_id}/add"
    r = requests.put(url, headers=headers)
    return r
