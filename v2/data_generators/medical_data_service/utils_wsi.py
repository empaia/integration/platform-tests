import requests

from ....global_services.data_generators.singletons import general_settings


def mds_get_wsi_info(headers, slide_id):
    url = f"{general_settings.mds_host}v1/slides/{slide_id}/info"
    r = requests.get(url, headers=headers)
    return r
