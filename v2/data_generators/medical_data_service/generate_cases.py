import argparse
from pathlib import Path

from .case_generators import generate_cases, get_case_data, get_wsi_data

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--cases-file", help="Path to cases json file.")
    parser.add_argument("--wsis-file", help="Path to wsis json file.")
    args = parser.parse_args()

    cases_file = Path(args.cases_file).expanduser()
    wsis_file = Path(args.wsis_file).expanduser()
    cases = get_case_data(cases_file)
    wsis = get_wsi_data(wsis_file)
    generate_cases(cases, wsis)
