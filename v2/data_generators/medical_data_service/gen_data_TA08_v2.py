from copy import deepcopy

from ....global_services.data_generators.singletons import login_manager
from . import utils_dads


def _gen_inputs(
    ead,
    user_id,
    slide_id,
):
    inputs_by_key = {}
    inputs_type_id = []
    ead = deepcopy(ead)

    # my_wsi
    inputs_by_key["my_wsi"] = ead["inputs"]["my_wsi"]
    inputs_by_key["my_wsi"]["id"] = slide_id
    inputs_type_id.append(("wsi", slide_id))

    # my_rectangle
    data = utils_dads.generate_rectangle_json(
        slide_id,
        user_id,
        "my_rectangle",
        "user",
        upper_left=[1000, 2000],
        width=300,
        height=500,
        npp_created=499,
        npp_viewing=[1, 499123],
    )
    headers = login_manager.wbs_client()
    r = utils_dads.mds_post_annotation(headers, data)
    print("DAD_Service - POST Recangle, status_code: ", r.status_code)
    assert r.status_code == 201
    inputs_by_key["my_rectangle"] = r.json()
    inputs_type_id.append(("rectangle", r.json()["id"]))

    return inputs_by_key, inputs_type_id
