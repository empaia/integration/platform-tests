import math
from copy import deepcopy

import requests
from sample_apps.sample_apps.test.internal._ITA02_wsi_collection import v2 as app

from ....global_services.data_generators.marketplace_service import utils as utils_mps
from ....global_services.data_generators.singletons import general_settings, login_manager
from ..case_data import get_test_wsis
from ..commons import ead_to_job
from . import utils, utils_dads


def _gen_inputs(
    ead,
    user_id,
    slide_ids,
):
    inputs_by_key = {}
    inputs_type_id = []
    ead = deepcopy(ead)

    # my_wsi
    inputs_by_key["my_wsi"] = ead["inputs"]["my_wsi"]
    inputs_by_key["my_wsi"]["id"] = slide_ids[0]
    inputs_type_id.append(("wsi", slide_ids[0]))

    # my_wsis_1
    items_simple_col = []
    items_simple_col.append({"id": slide_ids[1]})
    items_simple_col.append({"id": slide_ids[2]})

    data = utils_dads.generate_collection_json(None, None, user_id, "my_wsis_1", "user", "wsi", items_simple_col)
    headers = login_manager.wbs_client()
    r = utils_dads.mds_post_collection(headers, data)
    print("DAD_Service - POST simple Collection, status_code: ", r.status_code)
    assert r.status_code == 201
    col_result = r.json()
    inputs_by_key["my_wsis_1"] = col_result
    inputs_type_id.append(("collection", col_result["id"]))

    # my_wsis_2
    items_col_col = []
    items_simple_col = []
    items_simple_col.append({"id": slide_ids[3]})
    items_simple_col.append({"id": slide_ids[4]})
    data = utils_dads.generate_collection_json(None, None, user_id, "my_wsis_2_1", "user", "wsi", items_simple_col)
    items_col_col.append(data)
    items_simple_col = []
    items_simple_col.append({"id": slide_ids[5]})
    items_simple_col.append({"id": slide_ids[6]})
    data = utils_dads.generate_collection_json(None, None, user_id, "my_wsis_2_2", "user", "wsi", items_simple_col)
    items_col_col.append(data)

    data = utils_dads.generate_collection_json(None, None, user_id, "my_wsis_2", "user", "collection", items_col_col)
    headers = login_manager.wbs_client()
    r = utils_dads.mds_post_collection(headers, data)
    print("DAD_Service - POST Collection of Collection, status_code: ", r.status_code)
    assert r.status_code == 201
    col_col_result = r.json()
    inputs_by_key["my_wsis_2"] = col_col_result
    inputs_type_id.append(("collection", col_col_result["id"]))

    return inputs_by_key, inputs_type_id


def generate(
    user_id,
    slide_ids,
    ex_id,
    set_job_ready=False,
):
    ead = app.ead
    headers = login_manager.wbs_client()
    in_job = ead_to_job(ead, utils_mps.get_app_id_by_ead(headers, ead))
    app_id = in_job["app_id"]

    inputs_by_key, inputs_type_id = _gen_inputs(ead, user_id, slide_ids)
    in_job, job_id, _token = utils.create_job(ead, in_job, user_id, ex_id, app_id)
    utils.add_inputs_to_job(job_id, inputs_by_key)
    utils.lock_to_job(job_id, inputs_type_id)
    job_status = "ERROR"
    if set_job_ready:
        job_status = "READY"
    utils.set_js_job_statuses(job_id, [job_status])
    job = utils.get_job(job_id)
    return job, job_id


def check_containerized_app_output(job_id):
    """
    Checks DADS output :
    - (1) this [_gen_inputs] are used as inputs
    - (2) the containtized app was executed using (1)
    """
    url = f"{general_settings.mds_host}v1/primitives/query"
    query = {"jobs": [job_id]}
    headers = login_manager.wbs_client()
    r = requests.put(url, json=query, headers=headers)
    print("MDS - PUT primitives query: ", r.status_code)
    assert r.status_code == 200
    assert r.json()["item_count"] == 1
    assert r.json()["items"][0]["value"] == 3


def main():
    mta_user_id = login_manager.mta_user()["user-id"]
    patho_user_id = login_manager.patho_user()["user-id"]

    n_slides = 30
    slides = []
    wsis = get_test_wsis()
    for _i in range(math.ceil(n_slides / len(wsis))):
        slides += wsis

    case_id, slide_ids, _npp_ranges = utils.new_case_with_slides(
        mta_user_id,
        slides,
        f"{__file__.split('/')[-1]}",
    )
    ex_id = utils.add_examination_to_case(
        patho_user_id,
        case_id,
    )
    _in_job, _job_id = generate(
        patho_user_id,
        slide_ids,
        ex_id,
    )


if __name__ == "__main__":
    main()
