import requests
from sample_apps.sample_apps.test.tutorial_legacy._TA01_simple_app import v2 as app

from ....global_services.data_generators.singletons import general_settings, login_manager
from ...data_generators.case_data import get_test_cases
from ...data_generators.medical_data_service import gen_data_TA01_v2, utils, utils_case
from ..shared import wait_js_job_status
from ..utils import clean_up_case, get_case_counts, job_data_from_ead, read_case


def test_wbs_client_view_cases():
    patho_user_id = login_manager.patho_user()["user-id"]

    # read case from file
    case = read_case(get_test_cases, "FULL_PLATFORM_CASES_v2", "TEST03v2", "v2")
    case_id = case["case_id"]
    slide_ids = case["slide_ids"]

    slide_id = slide_ids[0]

    clean_up_case(case_id)

    # get counts for case
    headers = login_manager.patho_user()
    _, _, _, examination_count_old = get_case_counts(headers, case_id, "v2")

    # empty examination
    _ex_id = utils.add_examination_to_case(patho_user_id, case_id)
    # add examination with 2 jobs (1 incomplete)
    ex_id = utils.add_examination_to_case(patho_user_id, case_id)
    ead = app.ead
    headers = login_manager.wbs_client()
    in_job = job_data_from_ead(ead)

    app_id = in_job["app_id"]
    # incomplete job
    _, _ = gen_data_TA01_v2._gen_inputs(ead, patho_user_id, slide_id)
    in_job, _, _token = utils.create_job(ead, in_job, patho_user_id, ex_id, app_id)
    # complete job
    _job_in, job_id = gen_data_TA01_v2.generate(patho_user_id, slide_id, ex_id, set_job_ready=True)
    wait_js_job_status(job_id=job_id, final_status="COMPLETED")

    headers = login_manager.wbs_client()
    r = utils_case.mds_get_cases(headers=headers, skip=0, limit=0)
    print("MDS - GET cases, status_code: ", r.status_code)
    assert r.status_code == 200

    # WBS
    # GET single case
    url = f"{general_settings.wbs_host}v2/cases/{case_id}"
    headers = login_manager.patho_user()
    re = requests.get(url, headers=headers)
    print("WBS - GET case, status_code: ", re.status_code)
    assert re.status_code == 200
    case = re.json()
    assert case["mds_url"]
    assert case["local_id"]
    assert len(case["tissues"]) > 0
    assert len(case["stains"]) > 0
    assert len(case["blocks"]) > 0
    assert case["slides_count"] == len(slide_ids)
    assert len(case["examinations"]) - examination_count_old == 2

    # GET slides
    url = f"{general_settings.wbs_host}v2/cases/{case_id}/slides"
    headers = login_manager.patho_user()
    re = requests.get(url, headers=headers)
    print("WBS - GET case, status_code: ", re.status_code)
    assert re.status_code == 200
    for slide in re.json()["items"]:
        assert slide["mds_url"]
        assert slide["local_id"]
        if slide["tissue"]:
            assert "tag" in slide["tissue"]
            assert "mappings" in slide["tissue"]
            assert "EN" in slide["tissue"]["mappings"]
            assert "DE" in slide["tissue"]["mappings"]
        if slide["stain"]:
            assert "tag" in slide["stain"]
            assert "mappings" in slide["stain"]
            assert "EN" in slide["stain"]["mappings"]
            assert "DE" in slide["stain"]["mappings"]
        assert slide["block"]
