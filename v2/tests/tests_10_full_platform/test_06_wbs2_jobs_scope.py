import uuid

import requests
from sample_apps.sample_apps.test.internal._ITA02_wsi_collection import v2 as ita02v2
from sample_apps.sample_apps.test.tutorial_legacy._TA01_simple_app import v2 as ta01v2

from ....global_services.data_generators.singletons import general_settings, login_manager
from ...data_generators.medical_data_service import utils_dads
from .. import utils
from ..shared import wait_js_job_status


def test_wbs_scoped_jobs():
    case_id, slide_ids = utils.read_case_from_file("FULL_PLATFORM_CASES_v2", "TEST06v2", "v2")

    utils.clean_up_case(case_id)

    job_data_1 = utils.job_data_from_ead(ta01v2.ead)
    job_data_2 = utils.job_data_from_ead(ita02v2.ead)

    app_id_1 = job_data_1["app_id"]
    app_id_2 = job_data_2["app_id"]

    ex_id = utils.add_examination_to_case(case_id)

    utils.add_app_to_examination(ex_id, app_id_1)
    utils.add_app_to_examination(ex_id, app_id_2)

    scope_id_1, scope_header_1 = utils.create_scope(ex_id, app_id_1)
    scope_id_2, scope_header_2 = utils.create_scope(ex_id, app_id_2)

    for scope_id, scope_header in [
        (scope_id_1, scope_header_1),
        (scope_id_2, scope_header_2),
    ]:
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id}/jobs"
        r = requests.get(url, headers=scope_header)
        assert r.status_code == 200
        job_list = r.json()
        assert job_list["item_count"] == 0
        assert job_list["items"] == []

    # New jobs that don't use SCOPE as creator_type must be rejected
    job_data_1["creator_id"] = scope_id_1
    job_data_1["creator_type"] = "USER"
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id_1}/jobs"
    r = requests.post(url, json=job_data_1, headers=scope_header_1)
    assert r.status_code == 412

    # New jobs that don't use scope_id as creator_id must be rejected
    job_data_1["creator_id"] = str(uuid.uuid4())
    job_data_1["creator_type"] = "SCOPE"
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id_1}/jobs"
    r = requests.post(url, json=job_data_1, headers=scope_header_1)
    assert r.status_code == 412

    # New valid jobs in different scopes need to be separated from each other
    job_data_1["creator_id"] = scope_id_1
    job_data_1["creator_type"] = "SCOPE"
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id_1}/jobs"
    r = requests.post(url, json=job_data_1, headers=scope_header_1)
    assert r.status_code == 200
    job_1 = r.json()

    job_data_2["creator_id"] = scope_id_2
    job_data_2["creator_type"] = "SCOPE"
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id_2}/jobs"
    r = requests.post(url, json=job_data_2, headers=scope_header_2)
    assert r.status_code == 200
    job_2 = r.json()

    url = f"{general_settings.wbs_host}v2/scopes/{scope_id_1}/jobs"
    r = requests.get(url, headers=scope_header_1)
    assert r.status_code == 200
    job_list = r.json()
    assert job_list["item_count"] == 1
    assert job_list["items"][0]["id"] == job_1["id"]

    url = f"{general_settings.wbs_host}v2/scopes/{scope_id_2}/jobs"
    r = requests.get(url, headers=scope_header_2)
    assert r.status_code == 200
    job_list = r.json()
    assert job_list["item_count"] == 1
    assert job_list["items"][0]["id"] == job_2["id"]

    # Adding another valid job in existing scope with pre-existing job will group both under this scope
    job_data_1["creator_id"] = scope_id_1
    job_data_1["creator_type"] = "SCOPE"
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id_1}/jobs"
    r = requests.post(url, json=job_data_1, headers=scope_header_1)
    assert r.status_code == 200
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id_1}/jobs"
    r = requests.get(url, headers=scope_header_1)
    assert r.status_code == 200
    job_list = r.json()
    assert job_list["item_count"] == 2
    assert job_list["items"][0]["creator_id"] == scope_id_1
    assert job_list["items"][1]["creator_id"] == scope_id_1
    assert job_list["items"][0]["id"] != job_list["items"][1]["id"]

    # Job specific route tests

    job_id_1 = job_1["id"]
    job_id_2 = job_2["id"]

    # Prevent adding an input to a job not belonging to given scope
    input_id = str(uuid.uuid4())
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id_2}/jobs/{job_id_1}/inputs/my_wsi"
    r = requests.put(url, json=dict(id=input_id), headers=scope_header_2)
    assert r.status_code == 412

    # Adding an unknown input to a job belonging to given scope is a 400 bad request (as defined by JS)
    input_id = str(uuid.uuid4())
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id_1}/jobs/{job_id_1}/inputs/invalid_key"
    r = requests.put(url, json=dict(id=input_id), headers=scope_header_1)
    assert r.status_code == 400

    # Allow adding an input to a job belonging to given scope
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id_1}/jobs/{job_id_1}/inputs/my_wsi"
    r = requests.put(url, json=dict(id=input_id), headers=scope_header_1)
    assert r.status_code == 200
    updated_job = r.json()
    assert updated_job["inputs"]["my_wsi"] == input_id

    # Prevent deleting an input from a job not belonging to given scope
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id_1}/jobs/{job_id_2}/inputs/my_wsi"
    r = requests.delete(url, headers=scope_header_1)
    assert r.status_code == 412

    # Deleting an unknown input from a job belonging to given scope is a 400 bad request (as defined by JS)
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id_1}/jobs/{job_id_1}/inputs/invalid_key"
    r = requests.delete(url, headers=scope_header_1)
    assert r.status_code == 400

    # Allow deleting an input from a job belonging to given scope
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id_1}/jobs/{job_id_1}/inputs/my_wsi"
    r = requests.delete(url, headers=scope_header_1)
    assert r.status_code == 200
    updated_job = r.json()
    assert not updated_job["inputs"].get("my_wsi")

    # Prevent getting the slides of a job which does not belong to the given scope
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id_2}/jobs/{job_id_1}/slides"
    r = requests.get(url, headers=scope_header_2)
    assert r.status_code == 412

    # Allow getting the slides of a job which belongs to the given scope
    # 1. empty list for jobs without wsi inputs set
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id_2}/jobs/{job_id_2}/slides"
    r = requests.get(url, headers=scope_header_2)
    assert r.status_code == 200
    slide_list = r.json()
    assert slide_list["item_count"] == 0

    # 2. create slide and set input
    slide = slide_ids[0]
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id_2}/jobs/{job_id_2}/inputs/my_wsi"
    r = requests.put(url, json=dict(id=slide), headers=scope_header_2)
    assert r.status_code == 200

    # 2. slide list is now filled with one slide
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id_2}/jobs/{job_id_2}/slides"
    r = requests.get(url, headers=scope_header_2)
    assert r.status_code == 200
    slide_list = r.json()
    assert slide_list["item_count"] == 1
    assert slide_list["items"][0]["id"] == slide

    # 3. create slide collection and set input
    slide_2 = slide_ids[1]
    slide_3 = slide_ids[2]
    wsi_collection = create_collection_of_slides(scope_id_2, slide, slide_2, slide_3)
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id_2}/jobs/{job_id_2}/inputs/my_wsis_1"
    r = requests.put(url, json=dict(id=wsi_collection["id"]), headers=scope_header_2)
    assert r.status_code == 200

    # 4. get all slides should now include all input slides inserted until now
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id_2}/jobs/{job_id_2}/slides"
    r = requests.get(url, headers=scope_header_2)
    assert r.status_code == 200
    slide_list = r.json()
    assert slide_list["item_count"] == 4

    # Prevent running a job which does not belong to the given scope
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id_1}/jobs/{job_id_2}/run"
    r = requests.put(url, headers=scope_header_1)
    assert r.status_code == 412

    # Prevent running a job with missing inputs
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id_1}/jobs/{job_id_1}/run"
    r = requests.put(url, headers=scope_header_1)
    assert r.status_code == 400
    assert "has not been set" in r.text

    # Prevent running a job with wrong inputs
    # 1. add invalid rectangle
    input_id = str(uuid.uuid4())
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id_1}/jobs/{job_id_1}/inputs/my_rectangle"
    r = requests.put(url, json=dict(id=input_id), headers=scope_header_1)
    assert r.status_code == 200

    # 2. run should fail due to invalid input ids
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id_1}/jobs/{job_id_1}/run"
    r = requests.put(url, headers=scope_header_1)
    assert r.status_code == 400

    # Prevent running a job with wrong input annotation type
    # 1. reset input
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id_1}/jobs/{job_id_1}/inputs/my_rectangle"
    r = requests.delete(url, headers=scope_header_1)
    assert r.status_code == 200

    # 2. add valid slide
    input_id = slide
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id_1}/jobs/{job_id_1}/inputs/my_wsi"
    r = requests.put(url, json=dict(id=input_id), headers=scope_header_1)
    assert r.status_code == 200

    # 3. add valid annotation of wrong type
    point = create_point_annotation(slide, scope_id)
    input_id = point["id"]
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id_1}/jobs/{job_id_1}/inputs/my_rectangle"
    r = requests.put(url, json=dict(id=input_id), headers=scope_header_1)
    assert r.status_code == 200

    # 4 run should fail due to wrong annotation type
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id_1}/jobs/{job_id_1}/run"
    r = requests.put(url, headers=scope_header_1)
    assert r.status_code == 400
    assert "must be an annotation of type rectangle" in r.text

    # Allow running a job with correct inputs
    # 1. delete wrong input
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id_1}/jobs/{job_id_1}/inputs/my_rectangle"
    r = requests.delete(url, headers=scope_header_1)
    assert r.status_code == 200

    # 2. add valid annotation of correct type
    rectangle = create_rectangle_annotation(slide, scope_id)
    input_id = rectangle["id"]
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id_1}/jobs/{job_id_1}/inputs/my_rectangle"
    r = requests.put(url, json=dict(id=input_id), headers=scope_header_1)
    assert r.status_code == 200

    # 3. run should succeed and should have put the job to scheduled state
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id_1}/jobs/{job_id_1}/run"
    r = requests.put(url, headers=scope_header_1)
    assert r.status_code == 200
    job = r.json()
    assert job["status"] == "READY"

    # Prevent deletion of job which does not belong to the given scope
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id_2}/jobs/{job_id_1}"
    r = requests.delete(url, headers=scope_header_2)
    assert r.status_code == 412

    # Allow deletion of job which belongs to the given scope
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id_2}/jobs/{job_id_2}"
    r = requests.delete(url, headers=scope_header_2)
    assert r.status_code == 200
    deleted_job = r.json()
    assert deleted_job["id"] == job_2["id"]

    wait_js_job_status(job_id=job_id_1, final_status="COMPLETED")

    # check apps in examination
    url = f"{general_settings.wbs_host}v2/examinations/{ex_id}/apps"
    headers = login_manager.patho_user()
    r = requests.get(url, headers=headers)
    assert r.status_code == 200
    apps_data = r.json()
    assert apps_data["item_count"] == 2
    for app in apps_data["items"]:
        if app["id"] == app_id_1:
            assert app["jobs_count"] == 2
            assert app["jobs_count_finished"] == 1
        if app["id"] == app_id_2:
            assert app["jobs_count"] == 0
            assert app["jobs_count_finished"] == 0


def create_point_annotation(slide_id, scope_id):
    annotation_data = utils_dads.generate_point_json(slide_id, scope_id, creator_type="scope")
    headers = login_manager.wbs_client()
    r = utils_dads.mds_post_annotation(headers, annotation_data)
    return r.json()


def create_rectangle_annotation(slide_id, scope_id):
    annotation_data = utils_dads.generate_rectangle_json(slide_id, scope_id, creator_type="scope")
    headers = login_manager.wbs_client()
    r = utils_dads.mds_post_annotation(headers, annotation_data)
    return r.json()


def create_collection_of_slides(scope_id, *slides):
    data = {
        "name": "slides",
        "creator_id": scope_id,
        "creator_type": "scope",
        "item_type": "wsi",
        "type": "collection",
        "items": [{"id": slide, "type": "wsi"} for slide in slides],
    }
    headers = login_manager.wbs_client()
    response = utils_dads.mds_post_collection(headers, data)
    return response.json()
