import requests
from sample_apps.sample_apps.test.tutorial_legacy._TA01_simple_app import v2 as ta01v2
from sample_apps.sample_apps.test.tutorial_legacy._TA05_classes import v2 as ta05v2

from ....global_services.data_generators.marketplace_service.utils import get_app_id_by_ead
from ....global_services.data_generators.singletons import general_settings, login_manager
from ...data_generators.medical_data_service import utils_dads
from .. import utils


def test_wbs_class_value_validation():
    case_id, slide_ids = utils.read_case_from_file("FULL_PLATFORM_CASES_v2", "TEST06v2", "v2")

    app_id_1 = get_app_id_by_ead(login_manager.wbs_client(), ta01v2.ead)
    app_id_2 = get_app_id_by_ead(login_manager.wbs_client(), ta05v2.ead)

    job_data_1 = utils.job_data_from_ead(ta01v2.ead)
    job_data_2 = utils.job_data_from_ead(ta05v2.ead)

    app_id_1 = job_data_1["app_id"]
    app_id_2 = job_data_2["app_id"]

    ex_id = utils.add_examination_to_case(case_id)

    utils.add_app_to_examination(ex_id, app_id_1)
    utils.add_app_to_examination(ex_id, app_id_2)

    scope_id_1, scope_header_1 = utils.create_scope(ex_id, app_id_1)
    scope_id_2, scope_header_2 = utils.create_scope(ex_id, app_id_2)

    # App 1 - no classes in EAD
    rect = create_rectangle_annotation(slide_ids[0], scope_id_1)

    # valid class, must be accepted
    status_code = create_class_wbs_api(rect["id"], "org.empaia.global.v1.classes.roi", scope_id_1, scope_header_1)
    assert status_code == 200

    # invalid class, must be rejected
    status_code = create_class_wbs_api(rect["id"], "dummy", scope_id_1, scope_header_1)
    assert status_code == 400

    # App 2 - classes in EAD
    rects = create_rectangle_collection(slide_ids[0], scope_id_2)

    # valid class, must be accepted
    status_code = create_class_wbs_api(
        rects["items"][0]["id"], "org.empaia.global.v1.classes.roi", scope_id_2, scope_header_2
    )
    assert status_code == 200

    # valid class, must be accepted
    status_code = create_class_wbs_api(
        rects["items"][0]["id"], "org.empaia.vendor_name.tutorial_app_05.v2.classes.tumor", scope_id_2, scope_header_2
    )
    assert status_code == 200

    # invalid class, must be rejected
    status_code = create_class_wbs_api(
        rects["items"][0]["id"], "org.empaia.vendor_name.tutorial_app_05.v2.classes", scope_id_2, scope_header_2
    )
    assert status_code == 400

    # invalid class, must be rejected
    status_code = create_class_wbs_api(rects["items"][0]["id"], "dummy", scope_id_2, scope_header_2)
    assert status_code == 400


def create_rectangle_annotation(slide_id, scope_id):
    annotation_data = utils_dads.generate_rectangle_json(slide_id, scope_id, creator_type="scope")
    headers = login_manager.wbs_client()
    r = utils_dads.mds_post_annotation(headers, annotation_data)
    return r.json()


def create_rectangle_collection(slide_id, scope_id):
    annotation_data = utils_dads.generate_rectangle_json(slide_id, scope_id, creator_type="scope")
    data = {
        "name": "rects",
        "creator_id": scope_id,
        "creator_type": "scope",
        "item_type": "rectangle",
        "type": "collection",
        "items": [annotation_data],
    }
    headers = login_manager.wbs_client()
    response = utils_dads.mds_post_collection(headers, data)
    return response.json()


def create_class_wbs_api(ref_id, class_value, scope_id, scope_header):
    data = {
        "value": class_value,
        "type": "class",
        "creator_id": scope_id,
        "creator_type": "scope",
        "reference_type": "annotation",
        "reference_id": ref_id,
    }
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id}/classes"
    r = requests.post(url, json=data, headers=scope_header)
    return r.status_code
