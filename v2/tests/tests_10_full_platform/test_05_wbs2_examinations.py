import requests
from sample_apps.sample_apps.test.tutorial_legacy._TA01_simple_app import v2 as app

from ....global_services.data_generators.singletons import general_settings, login_manager
from ...data_generators.case_data import get_test_cases
from ...data_generators.medical_data_service import gen_data_TA01_v2, utils
from ..shared import wait_js_job_status
from ..utils import clean_up_case, get_case_counts, job_data_from_ead, read_case


def test_wbs2_examinations():
    patho_user_id = login_manager.patho_user()["user-id"]

    # read case from file
    case = read_case(get_test_cases, "FULL_PLATFORM_CASES_v2", "TEST05v2", "v2")
    case_id = case["case_id"]
    slide_ids = case["slide_ids"]

    slide_id = slide_ids[0]

    # clean up case for test run
    headers = login_manager.patho_user()
    clean_up_case(case_id)

    # get counts for case
    _, _, _, examination_count_old = get_case_counts(headers, case_id, "v2")

    # empty examination
    _ = utils.add_examination_to_case(patho_user_id, case_id)
    # add examination with 2 jobs (1 incomplete)
    ex_id = utils.add_examination_to_case(
        patho_user_id,
        case_id,
    )
    ead = app.ead
    headers = login_manager.wbs_client()
    in_job = job_data_from_ead(ead)

    app_id = in_job["app_id"]
    # incomplete job
    _, _ = gen_data_TA01_v2._gen_inputs(ead, patho_user_id, slide_id)
    in_job, job_id_invalid, _token = utils.create_job(ead, in_job, patho_user_id, ex_id, app_id)
    # complete job
    _job_in, job_id = gen_data_TA01_v2.generate(patho_user_id, slide_id, ex_id, set_job_ready=True)
    wait_js_job_status(job_id=job_id, final_status="COMPLETED")

    # Test Job Deletion
    params = {"skip": 0, "limit": 10000}
    _, job_id, _token = utils.create_job(ead, in_job, patho_user_id, ex_id, app_id)

    url = f"{general_settings.wbs_host}v2/examinations/{ex_id}/apps"
    headers = login_manager.patho_user()
    r = requests.get(url, headers=headers, params=params)
    print("WBS - GET Ex Apps, status_code: ", r.status_code)
    assert r.status_code == 200
    data = r.json()

    jobs_count_before_delete = None
    for e in data["items"]:
        _app_id = e["id"]
        if _app_id == app_id:
            jobs_count_before_delete = e["jobs_count"]
            break

    assert jobs_count_before_delete is not None

    utils.delete_job(job_id=job_id)

    headers = login_manager.patho_user()
    r = requests.get(url, headers=headers, params=params)
    print("WBS - GET cases, status_code: ", r.status_code)
    assert r.status_code == 200
    data = r.json()
    jobs_count_after_delete = None
    for e in data["items"]:
        _app_id = e["id"]
        if _app_id == app_id:
            jobs_count_after_delete = e["jobs_count"]
            break

    assert jobs_count_after_delete is not None

    assert jobs_count_after_delete == jobs_count_before_delete - 1

    # WBS
    # POST examination, another open
    url = f"{general_settings.wbs_host}v2/cases/{case_id}/examinations"
    headers = login_manager.patho_user()
    r = requests.post(url, headers=headers)
    print("WBS - POST examinations, status_code: ", r.status_code)
    print("text", r.text)
    # MDS v2 ?
    # assert r.status_code == 409
    # MDS v1
    assert r.status_code == 400
    assert r.json()["detail"] == f"Another open examination found in case '{case_id}'"

    # PUT examination states to CLOSED - error
    url = f"{general_settings.wbs_host}v2/examinations/{ex_id}/close"
    headers = login_manager.patho_user()
    r = requests.put(url, headers=headers)
    print(r.content)
    assert r.status_code == 400
    assert r.json()["detail"].startswith("At least one job in examination")

    # check state still open
    url = f"{general_settings.wbs_host}v2/cases/{case_id}/examinations"
    headers = login_manager.patho_user()
    r = requests.get(url, headers=headers)
    print("WBS - GET examinations, status_code: ", r.status_code)
    assert r.status_code == 200
    result_json = r.json()
    for e in result_json["items"]:
        if e["id"] == ex_id:
            assert e["state"] == "OPEN"

    # SET job state for invalid job
    url = f"{general_settings.mds_host}v1/jobs/{job_id_invalid}/status"
    payload = {"status": "ERROR"}
    headers = login_manager.wbs_client()
    r = requests.put(url, headers=headers, json=payload)
    print(r.content)
    print(job_id_invalid)
    assert r.status_code == 200
    assert r.json()["status"] == "ERROR"

    # PUT examination states to CLOSED
    url = f"{general_settings.wbs_host}v2/examinations/{ex_id}/close"
    headers = login_manager.patho_user()
    r = requests.put(url, headers=headers)
    print(r.content)
    assert r.status_code == 200
    assert r.json()["state"] == "CLOSED"

    # POST examination
    url = f"{general_settings.wbs_host}v2/cases/{case_id}/examinations"
    headers = login_manager.patho_user()
    r = requests.post(url, headers=headers)
    print("WBS - POST examinations, status_code: ", r.status_code)
    assert r.status_code == 201
    posted_ex = r.json()
    assert posted_ex["creator_id"] == patho_user_id
    assert posted_ex["creator_type"] == "USER"
    assert posted_ex["jobs_count"] == 0
    assert posted_ex["jobs_count_finished"] == 0

    # GET examinations
    url = f"{general_settings.wbs_host}v2/cases/{case_id}/examinations"
    headers = login_manager.patho_user()
    r = requests.get(url, headers=headers)
    print("WBS - GET examinations, status_code: ", r.status_code)
    assert r.status_code == 200
    result_json = r.json()
    assert result_json["item_count"] - examination_count_old == 3
    for e in result_json["items"]:
        assert e["creator_id"] == patho_user_id
        assert e["creator_type"] == "USER"
        if e["id"] == ex_id:
            assert e["jobs_count"] == 2
            assert e["jobs_count_finished"] == 2
            assert e["state"] == "CLOSED"
        elif e["id"] == posted_ex["id"]:
            assert e["jobs_count"] == 0
            assert e["jobs_count_finished"] == 0
            assert e["state"] == "OPEN"

    # PUT examination state to CLOSED
    url = f"{general_settings.wbs_host}v2/examinations/{posted_ex['id']}/close"
    headers = login_manager.patho_user()
    r = requests.put(url, headers=headers)
    print(r.content)
    assert r.status_code == 200
    assert r.json()["state"] == "CLOSED"
