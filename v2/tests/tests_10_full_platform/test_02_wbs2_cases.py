import requests
from sample_apps.sample_apps.test.tutorial_legacy._TA01_simple_app import v2 as app

from ....global_services.data_generators.singletons import general_settings, login_manager
from ...data_generators.case_data import get_test_cases
from ...data_generators.medical_data_service import gen_data_TA01_v2, utils, utils_case
from ..shared import wait_js_job_status
from ..utils import clean_up_case, get_case_counts, job_data_from_ead, read_case


def test_wbs_client_view_cases():
    patho_user_id = login_manager.patho_user()["user-id"]

    # read case from file
    _case = read_case(get_test_cases, "FULL_PLATFORM_CASES_v2", "TEST02v2", "v2")
    case_id = _case["case_id"]
    slide_ids = _case["slide_ids"]

    slide_id = slide_ids[0]

    clean_up_case(case_id)

    # get counts for case
    headers = login_manager.patho_user()
    _, _, _, examination_count = get_case_counts(headers, case_id, api_version="v2")

    # add 2 empty examination in closed state
    _ = utils.add_examination_to_case(patho_user_id, case_id)
    _ = utils.add_examination_to_case(patho_user_id, case_id)
    # add examination with 2 jobs (1 incomplete)
    ex_id = utils.add_examination_to_case(patho_user_id, case_id)
    ead = app.ead
    headers = login_manager.wbs_client()
    in_job = job_data_from_ead(ead)

    app_id = in_job["app_id"]
    # incomplete job
    _, _ = gen_data_TA01_v2._gen_inputs(ead, patho_user_id, slide_id)
    in_job, _, _token = utils.create_job(ead, in_job, patho_user_id, ex_id, app_id)
    # complete job
    _job_in, job_id = gen_data_TA01_v2.generate(patho_user_id, slide_id, ex_id, set_job_ready=True)
    wait_js_job_status(job_id=job_id, final_status="COMPLETED")

    headers = login_manager.wbs_client()
    r = utils_case.mds_get_cases(headers=headers, skip=0, limit=0)
    print("MDS - GET cases, status_code: ", r.status_code)
    assert r.status_code == 200
    cases_ground_truth = r.json()

    # WBS
    # GET cases
    url = f"{general_settings.wbs_host}v2/cases"

    params = {"limit": 0}
    headers = login_manager.patho_user()
    r = requests.get(url, headers=headers, params=params)
    print("WBS - GET cases, status_code: ", r.status_code)
    # print(r.text)
    assert r.status_code == 200
    result_json = r.json()
    cases_count = result_json["item_count"]
    assert cases_count == cases_ground_truth["item_count"]

    start = 0
    case_found = False
    while start < cases_count:
        url = f"{general_settings.wbs_host}v2/cases"
        params = {"skip": start, "limit": 100}
        headers = login_manager.patho_user()
        r = requests.get(url, headers=headers, params=params)
        print("WBS - GET cases, status_code: ", r.status_code)
        # print(r.content)
        assert r.status_code == 200
        result_json = r.json()

        # check stain, block and tissue
        for case in result_json["items"]:
            if case["slides_count"] > 0:
                # TODO: Adapt test to handle cases that were created without slide block in DMC
                # (only relevant when running tests on iron/silver deployment)
                assert "blocks" in case
                # assert len(case["blocks"]) > 0
            for stain in case["stains"]:
                assert "EN" in case["stains"][stain] and "DE" in case["stains"][stain]
            for tissue in case["tissues"]:
                assert "EN" in case["tissues"][tissue] and "DE" in case["tissues"][tissue]

        for case in result_json["items"]:
            if case["id"] == case_id:
                assert case["mds_url"]
                assert case["local_id"]
                assert case["slides_count"] == len(slide_ids)
                assert len(case["examinations"]) - examination_count == 3
                case_found = True

            # GET single case
            url = f"{general_settings.wbs_host}v2/cases/{case['id']}"
            headers = login_manager.patho_user()
            re = requests.get(url, headers=headers)
            print("WBS - GET case, status_code: ", re.status_code)
            assert re.status_code == 200
            single_case = re.json()
            if case["id"] == case_id:
                assert single_case["mds_url"]
                assert single_case["local_id"]
            assert case["slides_count"] == single_case["slides_count"]
            assert len(case["examinations"]) == len(single_case["examinations"])

        start += 100

    assert case_found
