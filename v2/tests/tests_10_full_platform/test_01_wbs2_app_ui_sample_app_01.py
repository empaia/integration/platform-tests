import time
from uuid import uuid4

import jwt
import pytest
import requests
from sample_apps.sample_apps.test.tutorial_legacy._TA10_app_with_ui import v2 as app

from ....global_services.data_generators.singletons import general_settings, login_manager
from ...data_generators.medical_data_service import utils_dads
from .. import utils
from ..shared import wait_jes_job_status, wait_js_job_status


def test_wbs2_app_ui_sample_app_01():
    case_id, slide_ids = utils.read_case_from_file("FULL_PLATFORM_CASES_v2", "TEST01v2", "v2")

    utils.clean_up_case(case_id)

    ex_id = utils.add_examination_to_case(case_id)
    job_data = utils.job_data_from_ead(app.ead)
    app_id = job_data["app_id"]
    utils.add_app_to_examination(ex_id, app_id)
    scope_id, scope_header = utils.create_scope(ex_id, app_id)

    # create job
    job_data["creator_id"] = scope_id
    job_data["creator_type"] = "SCOPE"
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id}/jobs"
    r = requests.post(url, json=job_data, headers=scope_header)
    assert r.status_code == 200
    job = r.json()
    job_id = job["id"]

    # create inputs
    slide_id = slide_ids[0]
    rect = utils_dads.generate_rectangle_json(slide_id, scope_id, creator_type="scope")
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id}/annotations"
    params = {"is_roi": True}
    r = requests.post(url, json=rect, params=params, headers=scope_header)
    assert r.status_code == 200
    rect_id = r.json()["id"]

    # add inputs to job
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id}/jobs/{job_id}/inputs/slide"
    r = requests.put(url, json=dict(id=slide_id), headers=scope_header)
    assert r.status_code == 200

    url = f"{general_settings.wbs_host}v2/scopes/{scope_id}/jobs/{job_id}/inputs/region_of_interest"
    r = requests.put(url, json=dict(id=rect_id), headers=scope_header)
    assert r.status_code == 200

    # start job
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id}/jobs/{job_id}/run"
    r = requests.put(url, headers=scope_header)
    assert r.status_code == 200

    # check if job is completed in JES
    final_jes_status = "TERMINATED"
    final_job_status = "COMPLETED"
    wait_js_job_status(job_id=job_id, final_status=final_job_status, timeout=300)
    wait_jes_job_status(job_id=job_id, final_status=final_jes_status)


@pytest.mark.skipif(not general_settings.enable_mps_mock, reason="Real MPS has no sample app UI bundles at the moment")
def test_wbs2_app_ui_sample_app_01_frontend_token():
    case_id, _ = utils.read_case_from_file("FULL_PLATFORM_CASES_v2", "TEST01v2", "v2")

    utils.clean_up_case(case_id)

    ex_id = utils.add_examination_to_case(case_id)
    job_data = utils.job_data_from_ead(app.ead)
    app_id = job_data["app_id"]
    utils.add_app_to_examination(ex_id, app_id)

    headers = login_manager.patho_user()
    url = f"{general_settings.wbs_host}v2/examinations/{ex_id}/apps/{app_id}/frontend-token"
    r = requests.get(url, headers=headers)
    resp = r.json()
    decoded_token = jwt.decode(resp["access_token"], options={"verify_signature": False})
    assert r.status_code == 200
    assert decoded_token["app_id"] == app_id
    assert decoded_token["app_ui_url"] == "http://generic-app-ui-v2:80"
    assert decoded_token["exp"] > round(time.time())


@pytest.mark.skipif(not general_settings.enable_mps_mock, reason="Real MPS has no sample app UI bundles at the moment")
def test_wbs2_app_ui_sample_app_01_get_frontend_mps_v1():
    case_id, _ = utils.read_case_from_file("FULL_PLATFORM_CASES_v2", "TEST01v2", "v2")

    utils.clean_up_case(case_id)

    ex_id = utils.add_examination_to_case(case_id)
    job_data = utils.job_data_from_ead(app.ead)
    app_id = job_data["app_id"]
    utils.add_app_to_examination(ex_id, app_id)

    headers = login_manager.patho_user()
    url = f"{general_settings.wbs_host}v2/examinations/{ex_id}/apps/{app_id}/frontend-token"
    r = requests.get(url, headers=headers)
    resp = r.json()
    frontend_token = resp["access_token"]

    url = f"{general_settings.wbs_host}v2/frontends/{frontend_token}/"
    r = requests.get(url, headers=headers)
    assert r.status_code == 200
    assert "<title>Generic App Ui</title>" in str(r.content)


def test_wbs2_app_ui_sample_app_01_get_frontend_invalid_token():
    case_id, _ = utils.read_case_from_file("FULL_PLATFORM_CASES_v2", "TEST01v2", "v2")

    utils.clean_up_case(case_id)

    ex_id = utils.add_examination_to_case(case_id)
    job_data = utils.job_data_from_ead(app.ead)
    app_id = job_data["app_id"]
    utils.add_app_to_examination(ex_id, app_id)

    headers = login_manager.patho_user()
    url = f"{general_settings.wbs_host}v2/examinations/{ex_id}/apps/{app_id}/frontend-token"
    r = requests.get(url, headers=headers)
    resp = r.json()
    decoded_token = jwt.decode(resp["access_token"], options={"verify_signature": False})
    decoded_token["app_id"] = str(uuid4())
    invalid_frontend_token = jwt.encode(decoded_token, key="secret")

    url = f"{general_settings.wbs_host}v2/frontends/{invalid_frontend_token}/"
    r = requests.get(url, headers=headers)
    assert r.json()["detail"] == "Error decoding Token."
    assert r.status_code == 401


def test_wbs2_app_ui_sample_app_01_get_ui_config_mps_v1():
    case_id, _ = utils.read_case_from_file("FULL_PLATFORM_CASES_v2", "TEST01v2", "v2")

    utils.clean_up_case(case_id)

    ex_id = utils.add_examination_to_case(case_id)
    job_data = utils.job_data_from_ead(app.ead)
    app_id = job_data["app_id"]
    utils.add_app_to_examination(ex_id, app_id)

    headers = login_manager.patho_user()
    url = f"{general_settings.wbs_host}v2/examinations/{ex_id}/apps/{app_id}/app-ui-config"
    r = requests.get(url, headers=headers)
    resp = r.json()
    assert r.status_code == 200
    assert resp == {
        "csp": {
            "script_src": None,
            "style_src": {"unsafe_inline": True, "unsafe_eval": None},
            "font_src": {"unsafe_inline": True, "unsafe_eval": None},
        },
        "iframe": None,
        "tested": None,
    }
