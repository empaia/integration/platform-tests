from time import sleep

import requests
from sample_apps.sample_apps.test.tutorial_legacy._TA10_app_with_ui import v2 as app

from ....global_services.data_generators.singletons import general_settings, login_manager
from ...data_generators.medical_data_service import utils_dads
from .. import utils


def test_wbs_stop_job_execution():
    case_id, slide_ids = utils.read_case_from_file("WBS_MDS_MPS_AAA_JES_AS_CASES_v2", "TEST02v2", "v2")
    ex_id = utils.add_examination_to_case(case_id)
    job_data = utils.job_data_from_ead(app.ead)
    app_id = job_data["app_id"]
    utils.add_app_to_examination(ex_id, app_id)
    scope_id, scope_header = utils.create_scope(ex_id, app_id)

    # create job
    job_data["creator_id"] = scope_id
    job_data["creator_type"] = "SCOPE"
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id}/jobs"
    r = requests.post(url, json=job_data, headers=scope_header)
    assert r.status_code == 200
    job = r.json()
    job_id = job["id"]

    # create inputs
    slide_id = slide_ids[0]
    rect = utils_dads.generate_rectangle_json(slide_id, scope_id, creator_type="scope")
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id}/annotations"
    params = {"is_roi": True}
    r = requests.post(url, json=rect, params=params, headers=scope_header)
    assert r.status_code == 200
    rect_id = r.json()["id"]

    # add inputs to job
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id}/jobs/{job_id}/inputs/slide"
    r = requests.put(url, json=dict(id=slide_id), headers=scope_header)
    assert r.status_code == 200

    url = f"{general_settings.wbs_host}v2/scopes/{scope_id}/jobs/{job_id}/inputs/region_of_interest"
    r = requests.put(url, json=dict(id=rect_id), headers=scope_header)
    assert r.status_code == 200

    # start job
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id}/jobs/{job_id}/run"
    r = requests.put(url, headers=scope_header)
    assert r.status_code == 200

    # GET job execution status
    for _ in range(10):
        url = f"{general_settings.jes_host}v1/executions/{job_id}"
        headers_wbs = login_manager.wbs_client()
        r = requests.get(url, headers=headers_wbs)
        if r.status_code == 404:
            sleep(0.5)
            continue

    data = r.json()
    assert data["status"] in ["SCHEDULED", "RUNNING"]

    # PUT stop job executions
    url = f"{general_settings.wbs_host}v2/scopes/{scope_id}/jobs/{job_id}/stop"
    r = requests.put(url, headers=scope_header)
    assert r.status_code == 200
    assert r.json() is True

    # GET job execution status
    url = f"{general_settings.jes_host}v1/executions/{job_id}"
    r = requests.get(url, headers=headers_wbs)
    assert r.json()["status"] == "STOPPED"
