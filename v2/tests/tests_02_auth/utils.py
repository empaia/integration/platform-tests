import json

import requests

from ....global_services.data_generators.marketplace_service.utils import get_mps_apps_list
from ....global_services.data_generators.singletons import general_settings, login_manager
from ...data_generators.case_data import get_test_cases
from ...data_generators.medical_data_service import utils, utils_examination
from ..utils import read_case


def api_auth_check(
    base_url,
    headers,
    excluded_paths,
    url_suffix="",
    openapi_suffix="/openapi.json",
    auth_expected=True,
    expected_reject_code=401,
    scope_id=None,
    expected_error_message=None,
):
    base_url = str(base_url).rstrip("/")

    r = requests.get(f"{base_url}{url_suffix}{openapi_suffix}")
    openapi = json.loads(r.content)
    if headers:
        expected_status_code = "!= 401" if auth_expected else f"{expected_reject_code}"
    else:
        expected_status_code = f"{expected_reject_code}"
    for path, ops in openapi["paths"].items():
        if path in excluded_paths:
            continue
        path = path.replace("scope_id", str(scope_id))
        path_no_vars = path.replace("{", "").replace("}", "")
        url = f"{base_url}{url_suffix}{path_no_vars}"
        for op in ops.keys():
            r = requests.request(op, url, headers=headers)
            print(f"AUTH - '{op}': '{url}', - STATUS CODE: '{r.status_code}' - EXPECTED: '{expected_status_code}'")
            if auth_expected:
                assert r.status_code != 401 and r.status_code != 500
            else:
                assert r.status_code == expected_reject_code
                if expected_error_message:
                    assert r.json()["detail"] == expected_error_message


def api_auth_check_wbs2_scope(
    base_url,
    headers,
    scope_id,
    excluded_paths,
    openapi_suffix="/openapi.json",
    auth_expected=True,
    expected_reject_code=401,
):
    api_auth_check(
        base_url,
        headers,
        excluded_paths,
        url_suffix="/scopes",
        openapi_suffix=openapi_suffix,
        auth_expected=auth_expected,
        expected_reject_code=expected_reject_code,
    )


def get_scope():
    # read case from file
    case = read_case(get_test_cases, "AUTH_CASES_v2", "TEST01v2", "v2")
    case_id = case["case_id"]

    # get examinations for patho user
    headers = login_manager.patho_user()
    url = f"{general_settings.wbs_host}v2/cases/{case_id}/examinations"
    r = requests.get(url, headers=headers)
    print("WBS - GET examinations, status_code: ", r.status_code)
    assert r.status_code == 200
    exa = r.json()["items"]

    headers_wbs = login_manager.wbs_client()
    app_list = get_mps_apps_list(headers_wbs).json()

    for item in app_list["items"]:
        v2_active_app_view = item["active_app_views"]["v2"]
        if v2_active_app_view:
            app_id = v2_active_app_view["app"]["id"]
            break

    # check examinations
    if len(exa) == 0:
        # add examination if none found
        # patho user ID to create examination
        patho_user_id = login_manager.patho_user()["user-id"]
        ex_id = utils.add_examination_to_case(patho_user_id, case_id)

        # add app to examination
        headers = login_manager.wbs_client()
        r = utils_examination.mds_put_app_to_examination(headers, ex_id, app_id)
        assert r.status_code == 200
    else:
        ex_id = exa[0]["id"]
        app_id = str(exa[0]["apps"][0])

    # create or get scope for user, examination and app
    url = f"{general_settings.wbs_host}v2/examinations/{ex_id}/apps/{app_id}/scope"
    headers = login_manager.patho_user()
    r = requests.put(url, headers=headers)
    print(r.json())
    assert r.status_code == 201 or r.status_code == 200

    data = r.json()
    scope_id = data["scope_id"]
    scope_access_token = data["access_token"]
    return scope_id, {"Authorization": f"Bearer {scope_access_token}"}
