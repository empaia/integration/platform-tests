import pytest

from ....global_services.data_generators.singletons import general_settings, login_manager
from .utils import api_auth_check

EXCLUDED_PATHS = ["/alive"]
AS_URL = general_settings.as_host


# REJECTED #


# REJECTED AUTH - PATHOLOGIST
@pytest.mark.skipif(login_manager.auth_mode == "off", reason="auth_mode is set to 'off'")
def test_as_auth_rejected_patho():
    headers = login_manager.patho_user()
    api_auth_check(AS_URL, headers, EXCLUDED_PATHS, auth_expected=False)


# REJECTED AUTH - MTA
@pytest.mark.skipif(login_manager.auth_mode == "off", reason="auth_mode is set to 'off'")
def test_as_auth_rejected_mta():
    headers = login_manager.mta_user()
    api_auth_check(AS_URL, headers, EXCLUDED_PATHS, auth_expected=False)


# REJECTED AUTH - WBS
@pytest.mark.skipif(login_manager.auth_mode == "off", reason="auth_mode is set to 'off'")
def test_as_auth_rejected_wbs():
    headers = login_manager.wbs_client()
    api_auth_check(AS_URL, headers, EXCLUDED_PATHS, auth_expected=False)


# REJECTED AUTH - JES
@pytest.mark.skipif(login_manager.auth_mode == "off", reason="auth_mode is set to 'off'")
def test_as_auth_rejected_jes():
    headers = login_manager.jes_client()
    api_auth_check(AS_URL, headers, EXCLUDED_PATHS, auth_expected=False)


# NO TOKEN #


@pytest.mark.skipif(login_manager.auth_mode == "off", reason="auth_mode is set to 'off'")
def test_as_auth_rejected_no_token():
    api_auth_check(AS_URL, None, EXCLUDED_PATHS, auth_expected=False, expected_reject_code=403)
