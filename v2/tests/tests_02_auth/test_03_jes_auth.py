import pytest

from ....global_services.data_generators.singletons import general_settings, login_manager
from .utils import api_auth_check

EXCLUDED_PATHS = ["/alive"]
JES_URL = general_settings.jes_host


# ACCEPTED #


# ACCEPTED AUTH - WBS
@pytest.mark.skipif(login_manager.auth_mode == "off", reason="auth_mode is set to 'off'")
def test_jes_auth_accepted_wbs():
    headers = login_manager.wbs_client()
    api_auth_check(JES_URL, headers, EXCLUDED_PATHS)


# REJECTED #


# REJECTED AUTH - WBS WRONG USER ID
@pytest.mark.skip(reason="until final token implemented")
@pytest.mark.skipif(login_manager.auth_mode == "off", reason="auth_mode is set to 'off'")
def test_jes_auth_rejected_wrong_wbs():
    headers = login_manager.wbs_client()
    headers["organization-id"] = "random"
    api_auth_check(JES_URL, headers, EXCLUDED_PATHS, auth_expected=False, expected_reject_code=412)


# REJECTED AUTH - PATHOLOGIST
@pytest.mark.skipif(login_manager.auth_mode == "off", reason="auth_mode is set to 'off'")
def test_jes_auth_rejected_patho():
    headers = login_manager.patho_user()
    api_auth_check(JES_URL, headers, EXCLUDED_PATHS, auth_expected=False)


# REJECTED AUTH - MTA
@pytest.mark.skipif(login_manager.auth_mode == "off", reason="auth_mode is set to 'off'")
def test_jes_auth_rejected_mta():
    headers = login_manager.mta_user()
    api_auth_check(JES_URL, headers, EXCLUDED_PATHS, auth_expected=False)


# REJECTED AUTH - AS
@pytest.mark.skipif(login_manager.auth_mode == "off", reason="auth_mode is set to 'off'")
def test_jes_auth_rejected_as():
    headers = login_manager.as_client()
    api_auth_check(JES_URL, headers, EXCLUDED_PATHS, auth_expected=False)


# NO TOKEN #


@pytest.mark.skipif(login_manager.auth_mode == "off", reason="auth_mode is set to 'off'")
def test_jes_auth_rejected_no_token():
    api_auth_check(JES_URL, None, EXCLUDED_PATHS, auth_expected=False, expected_reject_code=403)
