import json
from pathlib import Path, PurePath

import requests

from ...global_services.data_generators.marketplace_service.utils import get_app_id_by_ead
from ...global_services.data_generators.singletons import general_settings, login_manager
from ..data_generators.case_data import get_test_cases
from ..data_generators.commons import ead_to_job
from ..data_generators.medical_data_service import utils as utils_mds


def read_case(get_case_data_func, case_group, case_tag, api):
    cases = get_case_data_func()
    case_path = PurePath(f"{Path.cwd()}/{cases[api][case_group][case_tag]['path']}")
    with open(case_path, encoding="UTF-8") as f:
        return json.load(f)


def read_case_from_file(case_group, case_tag, api):
    case = read_case(get_test_cases, case_group, case_tag, api)
    case_id = case["case_id"]
    slide_ids = case["slide_ids"]
    return case_id, slide_ids


def get_case_counts(headers, case_id, api_version="v2"):
    url = f"{general_settings.wbs_host}{api_version}/cases/{case_id}"
    r = requests.get(url, headers=headers)
    print(r.text)
    print("WBS - GET case, status_code: ", r.status_code)
    assert r.status_code == 200
    case = r.json()
    if api_version == "v1":
        return case["slides_count"], case["jobs_count"], case["jobs_count_finished"], case["examinations_count"]
    else:
        return case["slides_count"], None, None, len(case["examinations"])


def set_js_job_status(job_id, status):
    headers = login_manager.wbs_client()
    url = f"{general_settings.mds_host}v1/jobs/{job_id}/status"
    data = {"status": status}
    r = requests.put(url, json=data, headers=headers)
    print(f"J_Service - PUT job status [{status}], status_code: ", r.status_code)
    print(r.text)
    assert r.status_code == 200 or r.status_code == 400  # job not found


def clean_up_case(case_id, wbs_api_version="v2"):
    headers = login_manager.patho_user()
    url = f"{general_settings.wbs_host}{wbs_api_version}/cases/{case_id}/examinations"
    r = requests.get(url, headers=headers)
    print("WBS - GET examinations, status_code: ", r.status_code)
    assert r.status_code == 200
    exa = r.json()
    for ex in exa["items"]:
        if ex["state"] == "CLOSED":
            continue
        if wbs_api_version == "v1":
            url = f"{general_settings.wbs_host}v1/examinations/{ex['id']}/state?state=CLOSED"
        else:
            url = f"{general_settings.wbs_host}{wbs_api_version}/examinations/{ex['id']}/close"
        for job in ex["jobs"]:
            set_js_job_status(job, "ERROR")
        r = requests.put(url, headers=headers)
        print(r.json())
        assert r.status_code == 200


def get_annotation_count(headers, slide_id, query):
    if query is None:
        query = {}
    url = f"{general_settings.wbs_host}v1/slides/{slide_id}/annotations/query"
    r = requests.put(url, json=query, headers=headers, params={"limit": 0})
    print("WBS - PUT annotations/query, status_code: ", r.status_code)
    assert r.status_code == 200
    result_json = r.json()
    return result_json["item_count"]


def job_data_from_ead(ead):
    headers = login_manager.wbs_client()
    job_data = ead_to_job(ead, get_app_id_by_ead(headers, ead))
    return job_data


def add_app_to_examination(ex_id, app_id):
    headers = login_manager.patho_user()
    url = f"{general_settings.wbs_host}v2/examinations/{ex_id}/apps/{app_id}"
    r = requests.put(url, headers=headers)
    r.raise_for_status()
    return r.json()


def add_examination_to_case(case_id):
    patho_user_id = login_manager.patho_user()["user-id"]
    ex_id = utils_mds.add_examination_to_case(patho_user_id, case_id)
    return ex_id


def create_scope(ex_id, app_id):
    url = f"{general_settings.wbs_host}v2/examinations/{ex_id}/apps/{app_id}/scope"
    r = requests.put(url, headers=login_manager.patho_user())
    print(r.json())
    assert r.status_code == 201 or r.status_code == 200
    data = r.json()
    scope_id = data["scope_id"]
    scope_access_token = data["access_token"]
    scope_header = {"Authorization": f"Bearer {scope_access_token}"}

    return scope_id, scope_header
