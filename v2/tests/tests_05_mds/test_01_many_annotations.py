from ....global_services.data_generators.singletons import login_manager
from ...data_generators.case_data import get_test_cases
from ...data_generators.medical_data_service import many_annot_same_coords
from ..utils import read_case

N_BATCHES = 1
N_POLYS = 10


def test_many_annots_mds():
    patho_user_id = login_manager.patho_user()["user-id"]

    # read case from file
    case = read_case(get_test_cases, "MDS_CASES_v2", "TEST01v2", "v2")
    case_id = case["case_id"]
    slide_ids = case["slide_ids"]
    _npp_ranges = case["npp_ranges"]

    npp_ranges = [
        [_npp_ranges[2], _npp_ranges[2] + 10000],
        [_npp_ranges[1], _npp_ranges[2]],
        [_npp_ranges[0], _npp_ranges[1]],
        [_npp_ranges[0], (_npp_ranges[0] + _npp_ranges[1]) / 2],
    ]

    many_annot_same_coords.generate(
        patho_user_id,
        case_id,
        slide_ids[0],
        npp_ranges[0],
        n_batches=N_BATCHES,
        n_polys=N_POLYS,
    )

    many_annot_same_coords.generate(
        patho_user_id,
        case_id,
        slide_ids[1],
        npp_ranges[1],
        n_batches=N_BATCHES,
        n_polys=N_POLYS,
    )

    many_annot_same_coords.generate(
        patho_user_id,
        case_id,
        slide_ids[2],
        npp_ranges[2],
        n_batches=N_BATCHES,
        n_polys=N_POLYS,
    )

    many_annot_same_coords.generate(
        patho_user_id,
        case_id,
        slide_ids[3],
        npp_ranges[3],
        n_batches=N_BATCHES,
        n_polys=N_POLYS,
    )

    many_annot_same_coords.generate(
        patho_user_id,
        case_id,
        slide_ids[4],
        npp_ranges[0],
        n_batches=N_BATCHES,
        n_polys=N_POLYS,
        same_pos=True,
    )
