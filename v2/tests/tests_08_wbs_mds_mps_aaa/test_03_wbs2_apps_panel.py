from uuid import uuid4

import requests
from sample_apps.sample_apps.test.tutorial_legacy._TA01_simple_app import v2 as ta01v2
from sample_apps.sample_apps.test.tutorial_legacy._TA07_configuration import v2 as ta07v2

from ....global_services.data_generators.marketplace_service.utils import get_app_id_by_ead, get_apps_by_id
from ....global_services.data_generators.singletons import general_settings, login_manager
from ..tests_07_wbs_mds.shared import create_case_ex_run_5_apps


def test_wbs_apps_panel():
    _, _, ex_id, _, _ = create_case_ex_run_5_apps("WBS_MDS_MPS_AAA_CASES_v2", "TEST03v2")

    # WBS
    # GET apps for examination
    url = f"{general_settings.wbs_host}v2/examinations/{ex_id}/apps"
    headers = login_manager.patho_user()
    r = requests.get(url, headers=headers)
    print("WBS - GET apps for examination, status_code: ", r.status_code)
    assert r.status_code == 200
    result_json = r.json()
    assert len(result_json["items"]) == 5
    assert result_json["item_count"] == 5
    for _, app in enumerate(result_json["items"]):
        headers = login_manager.wbs_client()
        assert app["id"] in list(get_apps_by_id(headers).keys())

    # PUT app for examination already exists
    headers = login_manager.wbs_client()
    app_id = get_app_id_by_ead(headers, ta01v2.ead)
    print("app_id", app_id)
    url = f"{general_settings.wbs_host}v2/examinations/{ex_id}/apps/{app_id}"
    headers = login_manager.patho_user()
    r = requests.put(url, headers=headers)
    print("WBS - PUT app for examination, status_code: ", r.status_code)
    print(r.content)
    assert r.status_code == 405  # NOTE: THIS VERSION OF EXAMINATION SERVICE HAS NO 405 yet
    result_json = r.json()
    assert "already in" in result_json["detail"]["detail"]

    # PUT app for examination non-exising
    app_id = str(uuid4())
    print("app_id", app_id)
    url = f"{general_settings.wbs_host}v2/examinations/{ex_id}/apps/{app_id}"
    headers = login_manager.patho_user()
    r = requests.put(url, headers=headers)
    print("WBS - PUT app for examination, status_code: ", r.status_code)
    print(r.content)
    assert r.status_code == 404

    # PUT app for examination
    # requests mps inside to get apps. The user has no rights to do that
    # must use wbs header for that!
    headers_wbs = login_manager.wbs_client()
    app_id = get_app_id_by_ead(headers_wbs, ta07v2.ead)
    print("app_id", app_id)
    url = f"{general_settings.wbs_host}v2/examinations/{ex_id}/apps/{app_id}"
    headers = login_manager.patho_user()
    r = requests.put(url, headers=headers)
    print("WBS - PUT app for examination, status_code: ", r.status_code)
    print(r.content)
    assert r.status_code == 200
    result_json = r.json()
    assert result_json["examination_id"] == ex_id
    assert result_json["app_id"] == app_id
