import requests

from ....global_services.data_generators.marketplace_service.utils import get_apps_by_id
from ....global_services.data_generators.singletons import general_settings, login_manager
from ...data_generators.case_data import get_test_cases
from ...data_generators.medical_data_service import utils
from ..utils import read_case


def test_wbs_cases_apps():
    patho_user_id = login_manager.patho_user()["user-id"]

    # read case from file
    case = read_case(get_test_cases, "WBS_MDS_MPS_AAA_CASES_v2", "TEST02v2", "v2")
    case_id = case["case_id"]

    # add examination
    _ = utils.add_examination_to_case(patho_user_id, case_id)

    headers = login_manager.wbs_client()
    accepted_apps_ground_truth = get_apps_by_id(headers, app_versions=["v2"])

    # get case by id
    url = f"{general_settings.wbs_host}v2/cases/{case_id}"
    headers = login_manager.patho_user()
    r = requests.get(url, headers=headers)
    print("WBS - GET case, status_code: ", r.status_code)
    assert r.status_code == 200

    # get apps
    url = f"{general_settings.wbs_host}v2/cases/{case_id}/apps"
    headers = login_manager.patho_user()
    r = requests.get(url, headers=headers)
    print("WBS - GET apps, status_code: ", r.status_code)
    print(r.content)
    assert r.status_code == 200
    result_json = r.json()

    assert result_json["item_count"] == len(list(accepted_apps_ground_truth.keys()))
    assert len(result_json["items"]) == len(list(accepted_apps_ground_truth.keys()))
    for _, app in enumerate(result_json["items"]):
        assert app["id"] in list(accepted_apps_ground_truth.keys())
