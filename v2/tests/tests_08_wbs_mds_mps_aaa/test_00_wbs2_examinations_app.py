import requests

from ....global_services.data_generators.singletons import general_settings, login_manager
from ..tests_07_wbs_mds.shared import create_case_ex_run_5_apps


def test_wbs_examinations_app():
    _, _, ex_id, _, _ = create_case_ex_run_5_apps("WBS_MDS_MPS_AAA_CASES_v2", "TEST00v2")

    # WBS
    # GET cases
    url = f"{general_settings.wbs_host}v2/examinations/{ex_id}/apps"
    headers = login_manager.patho_user()
    r = requests.get(url, headers=headers)
    print("WBS - GET examinations/apps, status_code: ", r.status_code)
    assert r.status_code == 200
    result_json = r.json()
    assert result_json["item_count"] == 5
    for app in result_json["items"]:
        assert app["jobs_count"] == 1
        assert app["jobs_count_finished"] == 1
