from ....global_services.data_generators.singletons import login_manager
from ...data_generators.case_data import get_test_cases
from ...data_generators.medical_data_service import (
    gen_data_TA01_v2,
    gen_data_TA02_v2,
    gen_data_TA03_v2,
    gen_data_TA04_v2,
    gen_data_TA06_v2,
    utils,
)
from ..utils import read_case


def create_case_ex_run_5_apps(case_group, case_tag):
    patho_user_id = login_manager.patho_user()["user-id"]

    # read case from file
    case = read_case(get_test_cases, case_group, case_tag, "v2")
    case_id = case["case_id"]
    slide_ids = case["slide_ids"]
    npp_ranges = case["npp_ranges"]

    # add examination
    ex_id = utils.add_examination_to_case(
        patho_user_id,
        case_id,
    )

    in_jobs = []
    job_ids = []

    # tutorial 01
    in_job, job_id = gen_data_TA01_v2.generate(
        patho_user_id,
        slide_ids[0],
        ex_id,
    )
    in_jobs.append(in_job)
    job_ids.append(job_id)
    # tutorial 02
    in_job, job_id = gen_data_TA02_v2.generate(
        patho_user_id,
        slide_ids[0],
        ex_id,
    )
    in_jobs.append(in_job)
    job_ids.append(job_id)
    # tutorial 03
    in_job, job_id = gen_data_TA03_v2.generate(
        patho_user_id,
        slide_ids[0],
        ex_id,
    )
    in_jobs.append(in_job)
    job_ids.append(job_id)
    # tutorial 04
    in_job, job_id = gen_data_TA04_v2.generate(
        patho_user_id,
        slide_ids[0],
        ex_id,
    )
    in_jobs.append(in_job)
    job_ids.append(job_id)
    # tutorial 06
    in_job, job_id = gen_data_TA06_v2.generate(patho_user_id, slide_ids[0], ex_id, npp_ranges=npp_ranges)
    in_jobs.append(in_job)
    job_ids.append(job_id)

    return case_id, slide_ids, ex_id, in_jobs, job_ids
