import requests
from sample_apps.sample_apps.test.internal._ITA12_job_input_class_constraint import v2 as ita12v2
from sample_apps.sample_apps.test.tutorial_legacy._TA05_classes import v2 as ta05v2

from ....global_services.data_generators.singletons import general_settings
from .. import utils


def test_run_job_lock_classes():
    # read cases from file
    case_id, slide_ids = utils.read_case_from_file("WBS_MDS_CASES_v2", "TEST06Cv2", "v2")

    # add examination / app
    ex_id1 = utils.add_examination_to_case(case_id)
    job_data = utils.job_data_from_ead(ta05v2.ead)
    job_data2 = utils.job_data_from_ead(ita12v2.ead)
    app_id = job_data["app_id"]
    app_id2 = job_data2["app_id"]
    utils.add_app_to_examination(ex_id1, app_id)
    utils.add_app_to_examination(ex_id1, app_id2)

    # Create scopes
    scope_id, scope_header = utils.create_scope(ex_id1, app_id)
    scope_id2, scope_header2 = utils.create_scope(ex_id1, app_id2)

    try:
        # Post new job
        job_data["creator_id"] = scope_id
        job_data["creator_type"] = "SCOPE"
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id}/jobs"
        r = requests.post(url, json=job_data, headers=scope_header)
        assert r.status_code == 200
        job = r.json()
        job_id = job["id"]

        # Post wsi input
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id}/jobs/{job_id}/inputs/my_wsi"
        r = requests.put(url, json=dict(id=slide_ids[0]), headers=scope_header)
        assert r.status_code == 200

        # Post annotation as ROI for scope
        rectangle = {
            "name": "Rectangle ROI",
            "type": "rectangle",
            "upper_left": [0, 26151],
            "width": 1770,
            "height": 1638,
            "npp_created": 5000,
            "npp_viewing": [1, 50000],
            "creator_id": scope_id,
            "creator_type": "scope",
            "reference_id": slide_ids[0],
            "reference_type": "wsi",
        }
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id}/annotations"
        params = {}
        params["is_roi"] = True
        r = requests.post(url, json=rectangle, params=params, headers=scope_header)
        assert r.status_code == 200
        assert r.json()["name"] == "Rectangle ROI"
        assert r.json()["classes"][0]["value"] == "org.empaia.global.v1.classes.roi"
        annotation_id = r.json()["id"]

        # post collection
        collection = {
            "name": "Collection name",
            "description": "Collection description",
            "creator_id": str(scope_id),
            "creator_type": "scope",
            "reference_id": slide_ids[0],
            "reference_type": "wsi",
            "type": "collection",
            "item_type": "rectangle",
            "items": [{"id": annotation_id}],
        }
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id}/collections"
        r = requests.post(url, json=collection, headers=scope_header)
        result_json = r.json()
        assert r.status_code == 200
        assert result_json["reference_type"] == "wsi"
        assert result_json["item_type"] == "rectangle"
        collection_id = result_json["id"]

        # post annotation input
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id}/jobs/{job_id}/inputs/my_rectangles"
        r = requests.put(url, json=dict(id=collection_id), headers=scope_header)
        assert r.status_code == 200

        # Run job
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id}/jobs/{job_id}/run"
        r = requests.put(url, headers=scope_header)
        assert r.status_code == 200

        # Post new job
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id}/jobs"
        r = requests.post(url, json=job_data, headers=scope_header)
        assert r.status_code == 200
        job = r.json()
        job_id = job["id"]

        # Post wsi input
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id}/jobs/{job_id}/inputs/my_wsi"
        r = requests.put(url, json=dict(id=slide_ids[0]), headers=scope_header)
        assert r.status_code == 200

        # post annotation without class
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id}/annotations"
        rectangle["name"] = "Rectangle ROI 2"
        params = {}
        r = requests.post(url, json=rectangle, params=params, headers=scope_header)
        assert r.status_code == 200
        assert r.json()["name"] == "Rectangle ROI 2"
        assert r.json()["classes"] == []
        annotation_id = r.json()["id"]

        # post collection
        collection["items"] = [{"id": annotation_id}]
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id}/collections"
        r = requests.post(url, json=collection, headers=scope_header)
        result_json = r.json()
        assert r.status_code == 200
        assert result_json["reference_type"] == "wsi"
        assert result_json["item_type"] == "rectangle"
        collection_id = result_json["id"]

        # post annotation input
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id}/jobs/{job_id}/inputs/my_rectangles"
        r = requests.put(url, json=dict(id=collection_id), headers=scope_header)
        assert r.status_code == 200

        # Run job with unfullfilled class constraint in inputs
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id}/jobs/{job_id}/run"
        r = requests.put(url, headers=scope_header)
        assert r.status_code == 400
        assert r.json()["detail"] == "Class constraint for annotation not fullfilled"

        # query some class
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id}/classes"
        query = {
            "creator_id": str(scope_id),  # correct scope
            "creator_type": "scope",  # correct type type
            "type": "class",
            "value": "org.empaia.vendor_name.tutorial_app_05.v2.classes.tumor",  # from ead
            "reference_id": annotation_id,
            "reference_type": "annotation",
        }
        r = requests.post(url, json=query, headers=scope_header)
        result_json = r.json()
        assert r.status_code == 200

        # Run job with invalid annotation class
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id}/jobs/{job_id}/run"
        r = requests.put(url, headers=scope_header)
        assert r.status_code == 400
        assert r.json()["detail"] == "Class constraint for annotation not fullfilled"

        # CLASS VALIDATION

        # Post new job
        job_data2["creator_id"] = scope_id2
        job_data2["creator_type"] = "SCOPE"
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id2}/jobs"
        r = requests.post(url, json=job_data2, headers=scope_header2)
        assert r.status_code == 200
        job2 = r.json()
        job_id2 = job2["id"]

        # Post wsi input
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id2}/jobs/{job_id2}/inputs/my_wsi"
        r = requests.put(url, json=dict(id=slide_ids[0]), headers=scope_header2)
        assert r.status_code == 200

        # Post annotation as ROI for scope
        rectangle = {
            "name": "Rectangle ROI",
            "type": "rectangle",
            "upper_left": [0, 26151],
            "width": 1770,
            "height": 1638,
            "npp_created": 5000,
            "npp_viewing": [1, 50000],
            "creator_id": scope_id2,
            "creator_type": "scope",
            "reference_id": slide_ids[0],
            "reference_type": "wsi",
        }
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id2}/annotations"
        params = {}
        r = requests.post(url, json=rectangle, params=params, headers=scope_header2)
        assert r.status_code == 200
        assert r.json()["name"] == "Rectangle ROI"
        annotation_id2 = r.json()["id"]

        # post class that is allowed in class constraint
        post_class = {
            "creator_id": str(scope_id2),
            "creator_type": "scope",
            "type": "class",
            "value": "org.empaia.vendor_name.internal_test_app_12.v2.classes.tumor",
            "reference_id": annotation_id2,
            "reference_type": "annotation",
        }
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id2}/classes"
        params = {}
        r = requests.post(url, json=post_class, params=params, headers=scope_header2)
        assert r.status_code == 200

        # post collection
        collection = {
            "name": "Collection name",
            "description": "Collection description",
            "creator_id": str(scope_id2),
            "creator_type": "scope",
            "reference_id": slide_ids[0],
            "reference_type": "wsi",
            "type": "collection",
            "item_type": "rectangle",
            "items": [{"id": annotation_id2}],
        }
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id2}/collections"
        r = requests.post(url, json=collection, headers=scope_header2)
        result_json = r.json()
        assert r.status_code == 200
        assert result_json["reference_type"] == "wsi"
        assert result_json["item_type"] == "rectangle"
        collection_id2 = result_json["id"]

        # post annotation input
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id2}/jobs/{job_id2}/inputs/my_rectangles"
        r = requests.put(url, json=dict(id=collection_id2), headers=scope_header2)
        assert r.status_code == 200

        # Run job
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id2}/jobs/{job_id2}/run"
        r = requests.put(url, headers=scope_header2)
        assert r.status_code == 400
        assert r.json()["detail"] == "Class constraint for annotation not fullfilled"

        # post invalid class
        post_class["value"] = "org.empaia.vendor_name.internal_test_app_12.v2.classes.input_classes.roi_class3"
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id2}/classes"
        params = {}
        r = requests.post(url, json=post_class, params=params, headers=scope_header2)
        assert r.status_code == 400
        assert r.json()["detail"] == "Invalid class name for EAD"

        # post valid class
        post_class["value"] = "org.empaia.vendor_name.internal_test_app_12.v2.classes.input_classes.roi_class1"
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id2}/classes"
        params = {}
        r = requests.post(url, json=post_class, params=params, headers=scope_header2)
        assert r.status_code == 200

        # Run job again
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id2}/jobs/{job_id2}/run"
        r = requests.put(url, headers=scope_header2)
        assert r.status_code == 200

    finally:
        utils.clean_up_case(case_id)
