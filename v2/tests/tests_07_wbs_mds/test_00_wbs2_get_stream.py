import io

import requests
from PIL import Image

from ....global_services.data_generators.singletons import general_settings, login_manager
from ...data_generators.case_data import get_test_cases
from ..utils import read_case

PARAMS = {"image_format": "jpg", "image_quality": 99}


def test_wbs_streaming_responses():
    # read case from file
    case = read_case(get_test_cases, "WBS_MDS_CASES_v2", "TEST00v2", "v2")
    slide_ids = case["slide_ids"]

    slide_id = slide_ids[0]

    # WBS
    # get streaming responses
    # macro
    headers = login_manager.patho_user()
    url = f"{general_settings.wbs_host}v2/slides/{slide_id}/macro/max_size/100/100"
    r = requests.get(url, headers=headers, params=PARAMS)
    print("WBS - GET slide macro image, status_code: ", r.status_code)
    print(r.content)
    assert r.status_code == 200
    assert r.headers["content-type"] == "image/jpeg"
    Image.open(io.BytesIO(r.content))

    # label
    headers = login_manager.patho_user()
    url = f"{general_settings.wbs_host}v2/slides/{slide_id}/label/max_size/100/100"
    r = requests.get(url, headers=headers, params=PARAMS)
    print("WBS - GET slide label image, status_code: ", r.status_code)
    assert r.status_code == 200
    assert r.headers["content-type"] == "image/jpeg"
    Image.open(io.BytesIO(r.content))

    # thumbnail
    headers = login_manager.patho_user()
    url = f"{general_settings.wbs_host}v2/slides/{slide_id}/thumbnail/max_size/{100}/{100}"
    r = requests.get(url, headers=headers, params=PARAMS)
    print("WBS - GET slide thumbnail image, status_code: ", r.status_code)
    assert r.status_code == 200
    assert r.headers["content-type"] == "image/jpeg"
    Image.open(io.BytesIO(r.content))

    # tile
    headers = login_manager.patho_user()
    url = f"{general_settings.wbs_host}v2/slides/{slide_id}/tile/level/{0}/tile/{0}/{0}"
    r = requests.get(url, headers=headers, params=PARAMS)
    print("WBS - GET slide tile image, status_code: ", r.status_code)
    assert r.status_code == 200
    assert r.headers["content-type"] == "image/jpeg"
    Image.open(io.BytesIO(r.content))
