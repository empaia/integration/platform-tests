import uuid

import requests
from sample_apps.sample_apps.test.tutorial_legacy._TA05_classes import v2 as app

from ....global_services.data_generators.singletons import general_settings, login_manager
from ...data_generators.case_data import get_test_cases
from ...data_generators.medical_data_service import utils
from ..utils import add_app_to_examination, clean_up_case, create_scope, job_data_from_ead, read_case


def test_data_scope():
    patho_user_id = login_manager.patho_user()["user-id"]

    # read cases from file
    case1 = read_case(get_test_cases, "WBS_MDS_CASES_v2", "TEST11v2", "v2")
    case2 = read_case(get_test_cases, "WBS_MDS_CASES_v2", "TEST11Bv2", "v2")
    case_id1 = case1["case_id"]
    case_id2 = case2["case_id"]

    # clean up case for test run
    clean_up_case(case_id1)
    clean_up_case(case_id2)

    # add examination
    ex_id1 = utils.add_examination_to_case(patho_user_id, case_id1)
    ex_id2 = utils.add_examination_to_case(patho_user_id, case_id2)

    # requests mps inside to get apps. The user has no rights to do that
    # must use wbs header for that!
    job_data = job_data_from_ead(app.ead)
    app_id = job_data["app_id"]

    add_app_to_examination(ex_id1, app_id)
    add_app_to_examination(ex_id2, app_id)

    try:
        # Create scopes
        scope_id1, scope_header1 = create_scope(ex_id1, app_id)
        scope_id2, scope_header2 = create_scope(ex_id2, app_id)

        # Post annotation, is not roi
        params = {}
        point = {
            "name": "annotation_1",
            "type": "point",
            "coordinates": [10000, 20000],
            "npp_created": 5000,
            "npp_viewing": [1, 50000],
            "creator_id": scope_id1,
            "creator_type": "scope",
            "reference_id": "some-id",
            "reference_type": "wsi",
        }
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/annotations"
        r = requests.post(url, json=point, params=params, headers=scope_header1)
        print("Create annotation in scope:", r.text)
        assert r.status_code == 200
        annotation_id = r.json()["id"]

        for data_type in ["primitives", "classes", "collections"]:
            # Post incorrect scope
            url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/{data_type}"
            post_data = {
                "creator_id": str(scope_id2),  # wrong scope
                "creator_type": "scope",
            }

            if data_type == "primitives":
                post_data["type"] = "integer"
                post_data["value"] = 100
                post_data["name"] = f"{data_type} Name"
                post_data["description"] = f"{data_type} Description"
            if data_type == "classes":
                post_data["type"] = "class"
                post_data["value"] = "dummy"
                post_data["reference_id"] = annotation_id
                post_data["reference_type"] = "annotation"
            if data_type == "collections":
                post_data["type"] = "collection"
                post_data["item_type"] = "point"

            r = requests.post(url, json=post_data, headers=scope_header1)
            assert r.status_code == 412
            assert r.json()["detail"] == "Creator_id must be set to scope_id"

            # Post incorrect scope type
            url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/{data_type}"
            post_data["creator_id"] = str(scope_id1)  # correct scope
            post_data["creator_type"] = "user"  # but wrong type
            r = requests.post(url, json=post_data, headers=scope_header1)
            assert r.status_code == 412
            assert r.json()["detail"] == "Creator_type must be set to scope"

            if data_type == "classes":
                url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/{data_type}"
                post_data["creator_id"] = str(scope_id1)  # correct scope
                post_data["creator_type"] = "scope"  # correct type type
                r = requests.post(url, json=post_data, headers=scope_header1)
                assert r.status_code == 400
                assert r.json()["detail"] == "Invalid class name for EAD"
                # set valid class value
                post_data["value"] = "org.empaia.vendor_name.tutorial_app_05.v2.classes.tumor"

            # Valid post
            url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/{data_type}"
            post_data["creator_id"] = str(scope_id1)  # correct scope
            post_data["creator_type"] = "scope"  # correct type type
            r = requests.post(url, json=post_data, headers=scope_header1)
            assert r.status_code == 200
            assert r.json()["creator_id"] == str(scope_id1)
            data_type_id = r.json()["id"]

            # Query, no query params set
            url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/{data_type}/query"
            query = {}
            r = requests.put(url, json=query, headers=scope_header1)
            assert r.status_code == 400
            assert r.json()["detail"] == "Either a valid creator_id or job list must be set as query parameter"

            # Query, valid scope id in query params set
            url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/{data_type}/query"
            query = {"creators": [str(scope_id1)]}
            r = requests.put(url, json=query, headers=scope_header1)
            assert r.status_code == 200
            assert r.json()["item_count"] == 1

            # Query, invalid scope id in query params set
            url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/{data_type}/query"
            query = {"creators": [str(scope_id2)]}
            r = requests.put(url, json=query, headers=scope_header1)
            assert r.status_code == 400
            assert r.json()["detail"] == "Either a valid creator_id or job list must be set as query parameter"

            # Query, invalid job id in query params set
            url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/{data_type}/query"
            query = {"jobs": [str(uuid.uuid4())]}
            r = requests.put(url, json=query, headers=scope_header1)
            assert r.status_code == 412
            assert r.json()["detail"] == "Invalid job list query parameter for current scope"

            # Query unique references
            url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/{data_type}/query/unique-references"
            query = {"creators": [str(scope_id1)]}
            r = requests.put(url, json=query, headers=scope_header1)
            assert r.status_code == 200

            if data_type == "primitives" or data_type == "collections":
                assert r.json()["contains_items_without_reference"] is True
            if data_type == "classes":
                assert r.json()["annotation"] == [annotation_id]

            # Get correct scope
            url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/{data_type}/{data_type_id}"
            r = requests.get(url, headers=scope_header1)
            assert r.status_code == 200
            assert r.json()["creator_id"] == str(scope_id1)

            # Delete incorrect scope
            url = f"{general_settings.wbs_host}v2/scopes/{scope_id2}/{data_type}/{data_type_id}"
            r = requests.delete(url, headers=scope_header2)
            assert r.status_code == 412
            assert "Not allowed to delete" in r.json()["detail"]

            # Delete correct scope
            url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/{data_type}/{data_type_id}"
            r = requests.delete(url, headers=scope_header1)
            assert r.status_code == 200
            assert r.json()["id"] == str(data_type_id)

        # Get class namespaces, invalid scope
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id2}/class-namespaces"
        r = requests.get(url, headers=scope_header1)
        assert r.status_code == 412

        # Get class namespaces
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/class-namespaces"
        r = requests.get(url, headers=scope_header1)
        assert r.status_code == 200
        assert len(r.json()["org.empaia.global.v1"]["classes"]) == 1
        assert len(r.json()["org.empaia.vendor_name.tutorial_app_05.v2"]["classes"]) == 2

    finally:
        # clean up case for test run
        clean_up_case(case_id1)
        clean_up_case(case_id2)
