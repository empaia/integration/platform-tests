import requests
from sample_apps.sample_apps.test.tutorial_legacy._TA05_classes import v2 as app

from ....global_services.data_generators.singletons import general_settings, login_manager
from ...data_generators.case_data import get_test_cases
from ...data_generators.medical_data_service import utils
from ..utils import add_app_to_examination, clean_up_case, create_scope, job_data_from_ead, read_case


def test_collections_scope():
    patho_user_id = login_manager.patho_user()["user-id"]

    # read cases from file
    case1 = read_case(get_test_cases, "WBS_MDS_CASES_v2", "TEST13v2", "v2")
    case2 = read_case(get_test_cases, "WBS_MDS_CASES_v2", "TEST13Bv2", "v2")
    case_id1 = case1["case_id"]
    case_id2 = case2["case_id"]

    # clean up case for test run
    clean_up_case(case_id1)
    clean_up_case(case_id2)

    # add examination
    ex_id1 = utils.add_examination_to_case(patho_user_id, case_id1)
    ex_id2 = utils.add_examination_to_case(patho_user_id, case_id2)

    # requests mps inside to get apps. The user has no rights to do that
    # must use wbs header for that!
    job_data = job_data_from_ead(app.ead)
    app_id = job_data["app_id"]

    add_app_to_examination(ex_id1, app_id)
    add_app_to_examination(ex_id2, app_id)

    try:
        # Create scopes
        scope_id1, scope_header1 = create_scope(ex_id1, app_id)
        scope_id2, _ = create_scope(ex_id2, app_id)

        # Post annotation
        point = {
            "name": "annotation_1",
            "type": "point",
            "coordinates": [10000, 20000],
            "npp_created": 5000,
            "npp_viewing": [1, 50000],
            "creator_id": scope_id1,
            "creator_type": "scope",
            "reference_id": "some-id",
            "reference_type": "wsi",
        }
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/annotations"
        params = {}
        r = requests.post(url, json=point, params=params, headers=scope_header1)
        assert r.status_code == 200
        annotation_id = r.json()["id"]

        # Post class for annotation
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/classes"
        post_class = {
            "type": "class",
            "value": "org.empaia.vendor_name.tutorial_app_05.v2.classes.tumor",
            "creator_id": str(scope_id1),
            "creator_type": "scope",
            "reference_id": annotation_id,
            "reference_type": "annotation",
        }
        r = requests.post(url, json=post_class, headers=scope_header1)
        assert r.status_code == 200

        # Get annotation
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/annotations/{annotation_id}"
        params = {
            "with_classes": "true",
        }
        r = requests.get(url, params=params, headers=scope_header1)
        assert r.status_code == 200
        assert r.json()["classes"][0]["value"] == "org.empaia.vendor_name.tutorial_app_05.v2.classes.tumor"

        # Post collection
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/collections"
        point["name"] = "Annotation in collection"
        items = [point]
        collection_query = {
            "name": "Collection name",
            "description": "Collection description",
            "creator_id": str(scope_id1),
            "creator_type": "scope",
            "reference_id": "wsi_id",
            "reference_type": "wsi",
            "type": "collection",
            "item_type": "point",
            "items": items,
        }
        r = requests.post(url, json=collection_query, headers=scope_header1)
        assert r.status_code == 200
        assert len(r.json()["items"]) == 1
        collection_id = r.json()["id"]

        # Post collection, invalid item scope
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/collections"
        collection_query["items"][0]["creator_id"] = str(scope_id2)
        r = requests.post(url, json=collection_query, headers=scope_header1)
        assert r.status_code == 412
        assert r.json()["detail"] == "Creator_id must be set to scope_id"

        # Post collection without items
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/collections"
        collection_query["items"] = []
        r = requests.post(url, json=collection_query, headers=scope_header1)
        assert r.status_code == 200

        # Post collection, invalid scope
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/collections"
        collection_query["creator_id"] = str(scope_id2)
        r = requests.post(url, json=collection_query, headers=scope_header1)
        assert r.status_code == 412
        assert r.json()["detail"] == "Creator_id must be set to scope_id"

        # Put collection item query
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/collections/{collection_id}/items/query"
        empty_query = {}
        r = requests.put(url, json=empty_query, headers=scope_header1)
        assert r.status_code == 200

        # Put collection item query, invalid scope
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id2}/collections/{collection_id}/items/query"
        r = requests.put(url, json=empty_query, headers=scope_header1)
        assert r.status_code == 412

        # Put collection item query unique references
        url = (
            f"{general_settings.wbs_host}v2/scopes/{scope_id1}/collections/{collection_id}/items/"
            "query/unique-references"
        )
        r = requests.put(url, json=empty_query, headers=scope_header1)
        assert r.status_code == 200

        # Put collection item query unique references, invalid scope
        url = (
            f"{general_settings.wbs_host}v2/scopes/{scope_id2}/collections/{collection_id}/items/"
            "query/unique-references"
        )
        r = requests.put(url, json=empty_query, headers=scope_header1)
        assert r.status_code == 412

        # post single new item

        # post item
        point["creator_id"] = scope_id1
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/collections/{collection_id}/items"
        r = requests.post(url, json=point, headers=scope_header1)
        print(r.text)
        assert r.status_code == 200
        item_id = r.json()["id"]

        # post item, invalid scope
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id2}/collections/{collection_id}/items"
        r = requests.post(url, json=point, headers=scope_header1)
        assert r.status_code == 412

        # post item, invalid creator
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/collections/{collection_id}/items"
        p = point.copy()
        p["creator_id"] = scope_id2
        r = requests.post(url, json=p, headers=scope_header1)
        assert r.status_code == 412
        assert r.json()["detail"] == "Creator_id must be set to scope_id"

        # post list of new items

        points = {"items": [point, point]}

        # post items
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/collections/{collection_id}/items"
        r = requests.post(url, json=points, headers=scope_header1)
        assert r.status_code == 200
        assert len(r.json()["items"]) == len(points["items"])

        # post items, invalid scope
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id2}/collections/{collection_id}/items"
        r = requests.post(url, json=points, headers=scope_header1)
        assert r.status_code == 412

        # post items, invalid creator
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/collections/{collection_id}/items"
        p = point.copy()
        p["creator_id"] = scope_id2
        points = {"items": [p, p]}
        r = requests.post(url, json=p, headers=scope_header1)
        assert r.status_code == 412
        assert r.json()["detail"] == "Creator_id must be set to scope_id"

        # post item by id

        # Post annotations
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/annotations"
        params = {}
        r = requests.post(url, json=point, params=params, headers=scope_header1)
        assert r.status_code == 200
        point_id1 = r.json()["id"]
        r = requests.post(url, json=point, params=params, headers=scope_header1)
        assert r.status_code == 200
        point_id2 = r.json()["id"]
        r = requests.post(url, json=point, params=params, headers=scope_header1)
        assert r.status_code == 200
        point_id3 = r.json()["id"]

        item = {"id": point_id1}

        # post item
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/collections/{collection_id}/items"
        r = requests.post(url, json=item, headers=scope_header1)
        assert r.status_code == 200

        # post item, invalid scope
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id2}/collections/{collection_id}/items"
        r = requests.post(url, json=item, headers=scope_header1)
        assert r.status_code == 412

        # post items by id

        items = {"items": [{"id": point_id2}, {"id": point_id3}]}

        # post items
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/collections/{collection_id}/items"
        r = requests.post(url, json=items, headers=scope_header1)
        assert r.status_code == 200

        # post items, invalid scope
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id2}/collections/{collection_id}/items"
        r = requests.post(url, json=items, headers=scope_header1)
        assert r.status_code == 412

        # delete item

        # delete item, invalid scope
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id2}/collections/{collection_id}/items/{item_id}"
        r = requests.delete(url, headers=scope_header1)
        assert r.status_code == 412

        # delete item
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/collections/{collection_id}/items/{item_id}"
        r = requests.delete(url, headers=scope_header1)
        assert r.status_code == 200
        assert r.json()["id"] == item_id

    finally:
        # clean up case for test run
        clean_up_case(case_id1)
        clean_up_case(case_id2)
