import uuid

import requests
from sample_apps.sample_apps.test.tutorial_legacy._TA01_simple_app import v2 as ta01v2
from sample_apps.sample_apps.test.tutorial_legacy._TA07_configuration import v2 as ta07v2

from ....global_services.data_generators.singletons import general_settings, login_manager
from ...data_generators.case_data import get_test_cases
from ...data_generators.medical_data_service import gen_data_TA01_v2, utils
from ..utils import clean_up_case, job_data_from_ead, read_case


def test_wbs_examinations_scope():
    patho_user_id = login_manager.patho_user()["user-id"]

    # read case from file
    case = read_case(get_test_cases, "WBS_MDS_CASES_v2", "TEST05Bv2", "v2")
    case_id = case["case_id"]
    slide_ids = case["slide_ids"]

    slide_id = slide_ids[0]

    # clean up case for test run
    headers = login_manager.patho_user()
    clean_up_case(case_id)

    # add examination with 1 job
    ex_id = utils.add_examination_to_case(
        patho_user_id,
        case_id,
    )

    app_id, job_id_invalid = create_job(patho_user_id, ex_id, slide_id, ta01v2.ead)
    app_id_2, job_id_invalid_2 = create_job(patho_user_id, ex_id, slide_id, ta07v2.ead)

    try:
        # Create scope
        url = f"{general_settings.wbs_host}v2/examinations/{ex_id}/apps/{app_id}/scope"
        headers = login_manager.patho_user()
        r = requests.put(url, headers=headers)
        assert r.status_code == 201

        # Create second scope
        url = f"{general_settings.wbs_host}v2/examinations/{ex_id}/apps/{app_id_2}/scope"
        headers = login_manager.patho_user()
        r = requests.put(url, headers=headers)
        assert r.status_code == 201
        data = r.json()
        scope_id_2 = data["scope_id"]
        scope_access_token_2 = data["access_token"]

        # Get scope
        url = f"{general_settings.wbs_host}v2/examinations/{ex_id}/apps/{app_id}/scope"
        r = requests.put(url, headers=headers)
        assert r.status_code == 200
        data = r.json()
        scope_id = data["scope_id"]
        scope_access_token = data["access_token"]

        # Get scope data
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id}"
        r = requests.get(url, headers={"Authorization": f"Bearer {scope_access_token}"})
        assert r.status_code == 200
        data = r.json()
        assert data["id"] == scope_id
        assert data["user_id"] == patho_user_id
        assert data["app_id"] == app_id
        assert data["examination_id"] == ex_id
        assert data["examination_state"] == "OPEN"
        assert data["case_id"] == case_id
        assert "ead" in data
        assert data["ead"] is not None
        assert "created_at" in data

        # Get second scope data
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id_2}"
        r = requests.get(url, headers={"Authorization": f"Bearer {scope_access_token_2}"})
        assert r.status_code == 200
        data = r.json()
        assert data["id"] == scope_id_2
        assert data["user_id"] == patho_user_id
        assert data["app_id"] == app_id_2
        assert data["examination_id"] == ex_id
        assert data["examination_state"] == "OPEN"
        assert data["case_id"] == case_id
        assert "ead" in data
        assert data["ead"] is not None
        assert "created_at" in data

        # No scope token must be rejected
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id}"
        r = requests.get(url)
        assert r.status_code == 403

        # Invalid scope token must be rejected
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id}"
        r = requests.get(url, headers={"Authorization": f"Bearer {str(uuid.uuid4())}"})
        assert r.status_code == 401

        # Valid scope token with wrong scope_id must be rejected
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id}"
        r = requests.get(url, headers={"Authorization": f"Bearer {scope_access_token_2}"})
        assert r.status_code == 412

    finally:
        # MDS
        set_job_failed(job_id_invalid)
        set_job_failed(job_id_invalid_2)

        # clean up case for test run
        headers = login_manager.patho_user()
        clean_up_case(case_id)


def create_job(patho_user_id, ex_id, slide_id, ead):
    in_job = job_data_from_ead(ead)

    app_id = in_job["app_id"]
    # incomplete job
    _, _ = gen_data_TA01_v2._gen_inputs(ead, patho_user_id, slide_id)
    in_job, job_id_invalid, _token = utils.create_job(ead, in_job, patho_user_id, ex_id, app_id)
    return app_id, job_id_invalid


def set_job_failed(job_id):
    url = f"{general_settings.mds_host}v1/jobs/{job_id}/status"
    payload = {"status": "ERROR"}
    headers = login_manager.wbs_client()
    r = requests.put(url, headers=headers, json=payload)
    print(r.content)
    print(job_id)
    assert r.status_code == 200
    assert r.json()["status"] == "ERROR"
