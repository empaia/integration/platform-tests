import requests
from sample_apps.sample_apps.test.tutorial_legacy._TA05_classes import v2 as app

from ....global_services.data_generators.singletons import general_settings, login_manager
from ...data_generators.case_data import get_test_cases
from ...data_generators.medical_data_service import utils
from ..utils import add_app_to_examination, clean_up_case, create_scope, job_data_from_ead, read_case


def test_annotations_scope():
    patho_user_id = login_manager.patho_user()["user-id"]

    # read cases from file
    case1 = read_case(get_test_cases, "WBS_MDS_CASES_v2", "TEST12v2", "v2")
    case2 = read_case(get_test_cases, "WBS_MDS_CASES_v2", "TEST12Bv2", "v2")
    case_id1 = case1["case_id"]
    case_id2 = case2["case_id"]

    # clean up case for test run
    clean_up_case(case_id1)
    clean_up_case(case_id2)

    # add examination
    ex_id1 = utils.add_examination_to_case(patho_user_id, case_id1)
    ex_id2 = utils.add_examination_to_case(patho_user_id, case_id2)

    # requests mps inside to get apps. The user has no rights to do that
    # must use wbs header for that!
    job_data = job_data_from_ead(app.ead)
    app_id = job_data["app_id"]

    add_app_to_examination(ex_id1, app_id)
    add_app_to_examination(ex_id2, app_id)

    try:
        # Create scopes
        scope_id1, scope_header1 = create_scope(ex_id1, app_id)
        scope_id2, scope_header2 = create_scope(ex_id2, app_id)

        # Post annotation as ROI for scope
        point = {
            "name": "annotation_1",
            "type": "point",
            "coordinates": [10000, 20000],
            "npp_created": 5000,
            "npp_viewing": [1, 50000],
            "creator_id": scope_id1,
            "creator_type": "scope",
            "reference_id": "some-id",
            "reference_type": "wsi",
        }
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/annotations"
        params = {}
        params["is_roi"] = True
        r = requests.post(url, json=point, params=params, headers=scope_header1)
        assert r.status_code == 200
        assert r.json()["name"] == "annotation_1"
        assert r.json()["classes"][0]["value"] == "org.empaia.global.v1.classes.roi"
        annotation_id = r.json()["id"]

        # Post annotation, is not roi
        params = {}
        point["name"] = "annotation_2"
        r = requests.post(url, json=point, params=params, headers=scope_header1)
        assert r.status_code == 200
        assert r.json()["name"] == "annotation_2"
        assert r.json()["classes"] == []

        # Get annotation
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/annotations/{annotation_id}"
        r = requests.get(url, headers=scope_header1)
        assert r.status_code == 200
        assert r.json()["name"] == "annotation_1"

        # Post annotation as ROI for wrong scope
        point["creator_id"] = scope_id2
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/annotations"
        r = requests.post(url, json=point, params=params, headers=scope_header1)
        assert r.status_code == 412

        # Put annotation query, no query params set
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/annotations/query"
        query = {}
        r = requests.put(url, json=query, headers=scope_header1)
        assert r.status_code == 400
        assert r.json()["detail"] == "Either a valid creator_id or job list must be set as query parameter"

        # Put annotation query, scope id in query params set
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/annotations/query"
        query = {"creators": [str(scope_id1)]}
        r = requests.put(url, json=query, headers=scope_header1)
        assert r.status_code == 200
        assert r.json()["item_count"] == 2
        assert len(r.json()["items"]) == 2

        # Put annotation query, scope id in query params set
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/annotations/query"
        query = {"creators": [str(scope_id2)]}
        r = requests.put(url, json=query, headers=scope_header1)
        assert r.status_code == 400
        assert r.json()["detail"] == "Either a valid creator_id or job list must be set as query parameter"

        for suffix in ["count", "viewer", "unique-class-values", "unique-references"]:
            # Query annotations
            url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/annotations/query/{suffix}"
            query = {"creators": [str(scope_id1)]}
            if suffix == "viewer":
                query["npp_viewing"] = [5.67, 7.89]
            r = requests.put(url, json=query, headers=scope_header1)
            assert r.status_code == 200

            # Query annotations, no query params set
            query = {}
            r = requests.put(url, json=query, headers=scope_header1)
            assert r.status_code == 400
            assert r.json()["detail"] == "Either a valid creator_id or job list must be set as query parameter"

            # Query annotations, no query params set, wrong header
            query = {}
            r = requests.put(url, json=query, headers=scope_header2)
            assert r.status_code == 412
            assert r.json()["detail"] == "Precondition Failed"

        # Delete annotation, wrong scope
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id2}/annotations/{annotation_id}"
        r = requests.delete(url, headers=scope_header2)
        assert r.status_code == 412

        # Delete annotation
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/annotations/{annotation_id}"
        r = requests.delete(url, headers=scope_header1)
        assert r.status_code == 200
        assert r.json()["id"] == annotation_id

        # Get annotation
        url = f"{general_settings.wbs_host}v2/scopes/{scope_id1}/annotations/{annotation_id}"
        r = requests.get(url, headers=scope_header1)
        assert r.status_code == 404

    finally:
        # clean up case for test run
        clean_up_case(case_id1)
        clean_up_case(case_id2)
