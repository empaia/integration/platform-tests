import requests
from sample_apps.sample_apps.test.tutorial_legacy._TA03_annotation_results import v2 as app

from ....global_services.data_generators.marketplace_service.utils import get_app_id_by_ead
from ....global_services.data_generators.singletons import general_settings, login_manager
from ...data_generators.case_data import get_test_cases
from ...data_generators.commons import ead_to_job
from ...data_generators.medical_data_service import gen_data_TA03_v2, utils
from ..shared import wait_jes_job_status, wait_js_job_status
from ..utils import read_case


def test_app():
    patho_user_id = login_manager.patho_user()["user-id"]

    # read case from file
    case = read_case(get_test_cases, "JES_AS_MDS_MPS_CASES_v2", "TEST99v2", "v2")
    case_id = case["case_id"]
    slide_ids = case["slide_ids"]

    # add examination
    ex_id = utils.add_examination_to_case(patho_user_id, case_id)

    # tutorial 03 - only create inputs
    ead = app.ead
    headers = login_manager.wbs_client()
    in_job = ead_to_job(ead, get_app_id_by_ead(headers, ead))

    app_id = in_job["app_id"]

    inputs_by_key, inputs_type_id = gen_data_TA03_v2._gen_inputs(ead, patho_user_id, slide_ids[0])
    in_job, job_id, _token = utils.create_job(ead, in_job, patho_user_id, ex_id, app_id)
    utils.add_inputs_to_job(job_id, inputs_by_key)
    utils.lock_to_job(job_id, inputs_type_id)
    utils.set_js_job_statuses(job_id, ["READY"])

    # check runtime: None
    check_runtime(job_id=job_id, is_none=True)

    final_jes_status = "TERMINATED"
    final_job_status = "COMPLETED"

    wait_jes_job_status(job_id=job_id, final_status=final_jes_status)
    wait_js_job_status(job_id=job_id, final_status=final_job_status)
    gen_data_TA03_v2.check_containerized_app_output(job_id)

    # check runtime: not None
    check_runtime(job_id=job_id, is_none=False)


def check_runtime(job_id, is_none: bool):
    url = f"{general_settings.mds_host}v1/jobs/{job_id}"
    headers = login_manager.wbs_client()
    r = requests.get(url, headers=headers, timeout=3)
    r.raise_for_status()
    job = r.json()
    print(job)
    if job["status"] != "RUNNING":
        return
    if is_none:
        assert job["runtime"] is None
    else:
        assert job["runtime"] is not None
