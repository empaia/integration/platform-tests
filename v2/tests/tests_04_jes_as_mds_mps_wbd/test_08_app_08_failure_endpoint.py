from sample_apps.sample_apps.test.tutorial_legacy._TA08_failure_endpoint import v2 as app

from ....global_services.data_generators.marketplace_service.utils import get_app_id_by_ead, get_apps_by_namespace
from ....global_services.data_generators.singletons import login_manager
from ...data_generators.case_data import get_test_cases
from ...data_generators.commons import ead_to_job
from ...data_generators.medical_data_service import utils
from ...data_generators.medical_data_service.gen_data_TA08_v2 import _gen_inputs
from ..shared import check_js_job_error_message, wait_jes_job_status, wait_js_job_status
from ..utils import read_case


def test_app():
    patho_user_id = login_manager.patho_user()["user-id"]

    case = read_case(get_test_cases, "JES_AS_MDS_MPS_CASES_v2", "TEST08v2", "v2")
    case_id = case["case_id"]
    slide_ids = case["slide_ids"]

    ex_id = utils.add_examination_to_case(patho_user_id, case_id)

    ead = app.ead
    from pprint import pprint

    headers = login_manager.wbs_client()
    pprint(get_apps_by_namespace(headers))
    in_job = ead_to_job(ead, get_app_id_by_ead(headers, ead))

    app_id = in_job["app_id"]

    inputs_by_key, inputs_type_id = _gen_inputs(ead, patho_user_id, slide_ids[0])
    in_job, job_id, _token = utils.create_job(ead, in_job, patho_user_id, ex_id, app_id)
    utils.add_inputs_to_job(job_id, inputs_by_key)
    utils.lock_to_job(job_id, inputs_type_id)
    utils.set_js_job_statuses(job_id, ["READY"])

    final_jes_status = "TERMINATED"
    final_job_status = "ERROR"

    wait_jes_job_status(job_id=job_id, final_status=final_jes_status, timeout=300)
    wait_js_job_status(job_id=job_id, final_status=final_job_status)
    check_js_job_error_message(job_id=job_id, error_message="Input rectangle must have width greater than 1024")
