# CHANGELOG - Platform Tests

# 0.5.0 - 0.5.2

* extend tests for FHIR Resource usage to support EMP-0105
* Use new `Questionnaire Definition File` via data generator to add one or multiple Questionnaire Resources
* raise Python version to ^3.12

# 0.4.0

* updated tests and generators to suport case partitioning EMP-0100

# 0.3.31

* added workbench service tests for EMP-0107 and EMP-0108

# 0.3.30

* updated resources and frontend tests

# 0.3.29

* added test for class locking in v3 API

# 0.3.28

* updated v3 tests for multi / single user

# 0.3.27

* renamed `PATH_TO_WSIS` env variables

# 0.3.26

* fix for pydantic settings in LoginManager

# 0.3.24 & 0.3.25

* updated tests for cds integration and wbs bug

# 0.3.23

* Added pixelmap tests in v3

# 0.3.21 & 0.3.22

* fix alive test for auth service (due to disabling of admin routes)

# 0.3.19 / 20

* increased timeout for 2 tests.

# 0.3.18

* changes due to update in py tus client lib

# 0.3.16 & 0.3.17

* increased & decreased timeout for output validation in large collections tests

# 0.3.14 & 0.3.15

* exclude alive route from idms auth test (v2 and v3)

# 0.3.13

* fix v2/tests/tests_04_jes_as_mds_mps_wbd/test_08_app_08_failure_endpoint.py

# 0.3.11 / 12

* made jes_client_id and secret optional in AuthSettings

# 0.3.10

* post timestamps as integers to MPS/MPS mock

# 0.3.9

* updated test 12b v3 for annotation service changes

# 0.3.8

* migration to pydantic 2

# 0.3.7

* adapted tests for v1 API removal

# 0.3.6

* fix for test wbs_cases

# 0.3.5

* removed v1 tests

# 0.3.4

* changed app used in outputs delete test

# 0.3.3

* added test in v3 API for delete job outputs

# 0.3.2

* updated tests for changes in WBS v3 scopes API data query validation

# 0.3.1

* update utils for auth service / mock

# 0.3.0

* removed auth v1 compatibility

# 0.2.16

* fix v2 v3 auth tests (scope) to make sure to pick an app with the approrpiate active app view not being None

# 0.2.15

* increasd timeout for v3 reports test as test system was too slow

# 0.2.14

* rmeoved auth service and mps authentication tests
  * not suited here, better tested in global deployment
  * upcoming role concept requires refactoring of authentication tests
* only auth service and mps remaining tests are alive tests
  * added pytest env settings:
    * aaa_api_version: str = "v2" # v1 v2
    * **ATTENTION**: default value are to be using new APIs
* updated aaa data-generators to support auth v2

# 0.2.11 / 12 / 13

* fix report tests"

# 0.2.10

* fix report tests"

# 0.2.9

* fixed missing roi class in TA11 data generator
* adapted test_11_app_11_preprocessing

# 0.2.8

* adapted WBS tests to reduced job validation (is now done seperately by WBD)

# 0.2.7

* changed env name PYTEST_CONTAINER_PATH_TO_WSIS to more propper name PYTEST_PATH_TO_WSIS
* fixed data-generator for apps
  * running twice with different resource files had overwritten previous organizations from firt run

# 0.2.6

* fix preprocessing test for adapted ead

# 0.2.5

* fix preprocessing trigger test

# 0.2.4

* fix preprocessing trigger test

# 0.2.3

* fixed test for deployments

# 0.2.2

* tests for reports feature

# 0.2.1

* more testss to fix some WBS bug

# 0.2.0

* refactored for different MDS API versions
* added additional tests for MDS v3 API

# 0.1.52

* added test files to generate data for frontend e2e tests
  * frontend_ci_cases.json
  * frontend_ci_wsis.json
  * frontend_ci_sample_apps_wbc1.json
  * frontend_ci_sample_apps_wbc2.json

# 0.1.51

* closing examinations properly when opening new ones
* jes tests now dont send the job execution requests explicitly or set job statuses unnecessarily
  * instead they let the WBD do its work
* added test for preprocessing app `test_11_app_11_preprocessing.py`
  * marked as skip (no v3 EAD scheme in use yet, no app service v3 routes yet)

# 0.1.50

* adapted login manager

# 0.1.49

* adapted mps mock data

# 0.1.48

* adapted platform tests

# 0.1.47

* updated tests for new WBD

# 0.1.46

* skip get app ui config test

# 0.1.45

* updated tests for app ui routes in WBS

# 0.1.44

* adapted post portal app model

# 0.1.43

* adapted app mock MPS v1

# 0.1.42

* optimized wbs2_mds scope jobs test

# 0.1.41

* added test for wbs2 stop job execution

# 0.1.40

* bugfix tests/wbs_mds/test_04_wbs_client_view_slides.py
  * always acquire new token before any request

# 0.1.39

* added test for stop job execution and fixed some tests

# 0.1.38

* fix misplaced env var (moved from pytest settings to compose settings)

# 0.1.37

* added switch and adapted tests for mps v1 routes

# 0.1.36

* fixed test_container_path_to_wsis
  * moved to pytest settings
  * assigned default value

# 0.1.35

* Quickfix default value for settings:
  * test_container_path_to_wsis: str = None

# 0.1.34

* added new test_container_path_to_wsis option for the upload service tests
* removed workaround from upload service tests

## 0.1.33

* changed sample-app url for updated platform-deployment

## 0.1.32

* removed skip from AS auth and read_only tests
* changed class-namespaces tests for updated WBS /v2/scopes API

## 0.1.31

* added explicit check for error message in WBS auth tests when audience not matching or missing

## 0.1.30

* fixed aaa auth tests
* reintroduced tests from 0.1.27 (kind of lost update)

## 0.1.29

* updated tests and generators for marketplace and aaa service

## 0.1.28

* updated tests for slide regions (scopes) endpoint

## 0.1.27

* adapted vs and sss auth tests (testing mps) to exlclude paths respectively

## 0.1.26

* Adapt to charite hosted global services
* several bugfixes where patho_user auth headers were send to services which should reject normally
  * it seems vita audiences were too generous at sss
  * and sss mock has no auth

## 0.1.25 ¹/²

* it seems this verison was forgot.
* if i remember correct. tutorial app failure was added (and test) for failure endpoint

## 0.1.25

* updated tests for job status changes

## 0.1.24

* updated test for job input class constraint

## 0.1.23

* added test for wbs route /v1/examinations/{examination_id}/apps/{app_id}/ead

## 0.1.22

* updated auth scope tests
* added wbs2 test for app ui sample app 01

## 0.1.21

* added tests for input classes validation

## 0.1.20

* skip auth scope tests

## 0.1.19

* updated job endpoint tests

## 0.1.18

* added more tests for annotation endpoints

## 0.1.17

* updated tests

## 0.1.16

* updated tests

## 0.1.15

* changed test data for test case

## 0.1.14

* add tests for scoped job routes of WBS v2 API

## 0.1.13

* fixed a bug concerning generate_apps.py with wrong organization_ids
* replaced get_apps_by_name() with get_apps_by_namespace (should be unique)

## 0.1.12

* updated tests for changes in WBS v1

## 0.1.11

* wbsv2 slides and data routes tests

## 0.1.10

* added read only tests

## 0.1.9

* more wbsv2 endpoint tests

## 0.1.8

* added tests for service to service authentication

## 0.1.7

* add missing wbs2 cases/id/apps test
* add missing wbs2 examinations/id/apps tests
* added test for wbs v2 route /v2/scopes/{scope_id} including scope-access-token validation

## 0.1.6

* functionality to generate deleted slides with data-generator
* some wbsv2 endpointtests

## 0.1.5

* test for upload-service added

## 0.1.4

* updated test_02_wbs_cases.py to reflect WBS 0.5.54 behaviour show all examinations not only the ones created by the current user

## 0.1.3

* refactored for auth deployment

## 0.1.2

* generated cases from data generators are now placed under `deployments/<deployment>/data` instead of `deployments//data`

## 0.1.1

* refactored case generation (this was actually last merge, but version increment was forgotten)
* refactored app generation

## 0.1.0

* innitial commit
