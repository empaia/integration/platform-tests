# Platform Tests

Repository for data generator scripts and integration tests.

Intended use as part of [Platform Deployment](https://gitlab.com/empaia/integration/platform-deployment)

## Code Style

```bash
sudo apt update
sudo apt install python3-venv python3-pip
cd platform-tests
python3 -m venv .venv
source .venv/bin/activate
poetry install
```

```bash
black .
isort .
pycodestyle v2 v3 global_services
pylint v2 v3 global_services
```
