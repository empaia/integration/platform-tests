import requests

from ....global_services.data_generators.marketplace_service.utils import get_apps_by_id
from ....global_services.data_generators.singletons import general_settings, login_manager


def test_wbs_cases_apps():
    headers = login_manager.wbs_client()
    accepted_apps_ground_truth = get_apps_by_id(headers, status=["LISTED", "DELISTED"], app_versions=["v3"])

    # get apps
    url = f"{general_settings.wbs_host}v3/apps/query"
    headers = login_manager.patho_user()
    r = requests.put(url, headers=headers, json={})
    print("WBS - GET apps, status_code: ", r.status_code)
    print(r.content)
    assert r.status_code == 200
    result_json = r.json()
    assert result_json["item_count"] == len(list(accepted_apps_ground_truth.keys()))
    assert len(result_json["items"]) == len(list(accepted_apps_ground_truth.keys()))
    for _, app in enumerate(result_json["items"]):
        assert app["app_id"] in list(accepted_apps_ground_truth.keys())
