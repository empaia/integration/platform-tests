from uuid import uuid4

import requests
from sample_apps.sample_apps.tutorial._TA01_simple_app import v3 as ta01v3

from ....global_services.data_generators.marketplace_service import utils as utils_mps
from ....global_services.data_generators.singletons import general_settings, login_manager
from ...data_generators.case_data import get_test_cases
from ...data_generators.medical_data_service import utils
from ..utils import read_case


def test_wbs_apps_panel():
    patho_user_id = login_manager.patho_user()["user-id"]

    # read case from file
    case = read_case(get_test_cases, "WBS_MDS_MPS_AAA_CASES_v3", "TEST03v3", "v3")
    case_id = case["case_id"]

    headers = login_manager.wbs_client()
    app_id_1 = utils_mps.get_app_id_by_ead(headers, ta01v3.ead)

    ex_id_1 = utils.add_or_get_examination_to_case(patho_user_id, case_id, app_id_1)

    # PUT apps query
    url = f"{general_settings.wbs_host}v3/apps/query"
    payload = {}
    headers = login_manager.patho_user()
    r = requests.put(url, headers=headers, json=payload)
    assert r.status_code == 200
    apps_query_response = r.json()
    app_ids_from_query = [a["app_id"] for a in apps_query_response["items"]]

    # WBS
    # GET app for examination
    url = f"{general_settings.wbs_host}v3/examinations/{ex_id_1}"
    headers = login_manager.patho_user()
    r = requests.get(url, headers=headers)
    print("WBS - GET examination, status_code: ", r.status_code)
    assert r.status_code == 200
    result_json = r.json()
    assert result_json["app"]["app_id"] == app_id_1
    assert result_json["jobs_count"] == 0
    assert app_id_1 in app_ids_from_query

    # PUT examination again via wbs api - get existing
    url = f"{general_settings.wbs_host}v3/examinations"
    payload = {
        "case_id": case_id,
        "app_id": app_id_1,
    }
    headers = login_manager.patho_user()
    r = requests.put(url, headers=headers, json=payload)
    print("WBS - PUT examination, status_code: ", r.status_code)
    assert r.status_code == 200

    # PUT examination non-exising app_id
    app_id_no_exist = str(uuid4())
    url = f"{general_settings.wbs_host}v3/examinations"
    payload = {
        "case_id": case_id,
        "app_id": app_id_no_exist,
    }
    headers = login_manager.patho_user()
    r = requests.put(url, headers=headers, json=payload)
    print("WBS - PUT examination non existing app, status_code: ", r.status_code)
    assert r.status_code == 404


def test_wbs_apps_panel_filter():
    # PUT apps query 1
    url = f"{general_settings.wbs_host}v3/apps/query"
    query = {"job_modes": ["PREPROCESSING"]}
    headers = login_manager.patho_user()
    r = requests.put(url, headers=headers, json=query)
    print(r.text)
    assert r.status_code == 200
    apps_query_response = r.json()
    apps_names = [a["name_short"] for a in apps_query_response["items"]]
    assert "TA11v3" in apps_names
    assert "ITA13v3" in apps_names
