import requests

from ....global_services.data_generators.marketplace_service.utils import get_app_id_by_ead
from ....global_services.data_generators.singletons import general_settings, login_manager
from ...data_generators.workbench_service import gen_data_TA01_v3
from .. import utils


def test_wbs_examinations_app():
    # read cases from file
    case_id, slide_ids = utils.read_case_from_file("WBS_MDS_MPS_AAA_CASES_v3", "TEST00v3", "v3")
    app_id_1 = get_app_id_by_ead(login_manager.wbs_client(), gen_data_TA01_v3.app.ead)
    ex_id_1 = utils.add_or_get_examination_to_case(case_id, app_id_1)
    scope_id, scope_header = utils.create_scope(ex_id_1)

    # WBS
    # GET ex
    url = f"{general_settings.wbs_host}v3/examinations/{ex_id_1}"
    headers = login_manager.patho_user()
    r = requests.get(url, headers=headers)
    print("WBS - GET examination, status_code: ", r.status_code)
    assert r.status_code == 200
    result_json = r.json()
    jobs_count_old = result_json["jobs_count"]
    jobs_count_fin_old = result_json["jobs_count_finished"]

    # create new jobs
    for _ in range(2):
        gen_data_TA01_v3.generate(scope_id, scope_header, slide_ids[0], ex_id_1, case_id)

    # WBS
    # GET ex
    url = f"{general_settings.wbs_host}v3/examinations/{ex_id_1}"
    headers = login_manager.patho_user()
    r = requests.get(url, headers=headers)
    print("WBS - GET examination, status_code: ", r.status_code)
    assert r.status_code == 200
    result_json = r.json()
    assert result_json["jobs_count"] == jobs_count_old + 2
    assert result_json["jobs_count_finished"] == jobs_count_fin_old + 2
