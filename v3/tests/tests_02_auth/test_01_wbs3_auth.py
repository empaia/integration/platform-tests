from uuid import uuid4

import pytest

from ....global_services.data_generators.singletons import general_settings, login_manager
from .utils import api_auth_check, api_auth_check_wbs3_scope, get_scope

EXCLUDED_PATHS = []
WBS_URL = general_settings.wbs_host


# ACCEPTED #


# /alive
def test_wbs_general_auth_accepted_patho():
    headers = login_manager.patho_user()
    api_auth_check(WBS_URL, headers, EXCLUDED_PATHS)


# ACCEPTED AUTH - PATHOLOGIST - NON SCOPE ENDPOINTS
@pytest.mark.skipif(login_manager.auth_mode == "off", reason="auth_mode is set to 'off'")
def test_wbs_3_auth_accepted_patho():
    headers = login_manager.patho_user()
    api_auth_check(str(WBS_URL) + "v3", headers, EXCLUDED_PATHS)


# ACCEPTED AUTH - SCOPE ENDPOINTS
@pytest.mark.skipif(login_manager.auth_mode == "off", reason="auth_mode is set to 'off'")
def test_wbs_3_auth_accepted_scope():
    scope_id, headers = get_scope()
    api_auth_check_wbs3_scope(str(WBS_URL) + "v3", headers, scope_id, EXCLUDED_PATHS)


# REJECTED #


# REJECTED AUTH - PATHOLOGIST WRONG USER ID
@pytest.mark.skipif(login_manager.auth_mode == "off", reason="auth_mode is set to 'off'")
def test_wbs_3_auth_rejected_wrong_patho():
    headers = login_manager.patho_user()
    headers["user-id"] = "random"
    api_auth_check(str(WBS_URL) + "v3", headers, EXCLUDED_PATHS, auth_expected=False, expected_reject_code=412)


# REJECTED AUTH - SCOPE WRONG SCOPE ID
@pytest.mark.skipif(login_manager.auth_mode == "off", reason="auth_mode is set to 'off'")
def test_wbs_3_auth_rejected_wrong_scope():
    _, scope_header = get_scope()
    scope_id = uuid4()
    api_auth_check_wbs3_scope(
        str(WBS_URL) + "v3", scope_header, scope_id, EXCLUDED_PATHS, auth_expected=False, expected_reject_code=412
    )


# REJECTED AUTH - MTA
# @pytest.mark.skipif(login_manager.auth_mode == "off", reason="auth_mode is set to 'off'")
@pytest.mark.skip(reason="no distinction between organization members'")
def test_wbs_3_auth_rejected_mta():
    headers = login_manager.mta_user()
    api_auth_check(str(WBS_URL) + "v3", headers, EXCLUDED_PATHS, auth_expected=False)


# REJECTED AUTH - AS
@pytest.mark.skipif(login_manager.auth_mode == "off", reason="auth_mode is set to 'off'")
def test_wbs_3_auth_rejected_as():
    headers = login_manager.as_client()
    api_auth_check(
        str(WBS_URL) + "v3",
        headers,
        EXCLUDED_PATHS,
        auth_expected=False,
        expected_error_message="API access denied.",
    )


# REJECTED AUTH - JES
@pytest.mark.skipif(login_manager.auth_mode == "off", reason="auth_mode is set to 'off'")
def test_wbs_3_auth_rejected_jes():
    headers = login_manager.jes_client()
    api_auth_check(
        str(WBS_URL) + "v3",
        headers,
        EXCLUDED_PATHS,
        auth_expected=False,
        expected_error_message="API access denied.",
    )


# # NO TOKEN #


@pytest.mark.skipif(login_manager.auth_mode == "off", reason="auth_mode is set to 'off'")
def test_wbs_3_auth_rejected_no_token():
    api_auth_check(str(WBS_URL) + "v3", None, EXCLUDED_PATHS, auth_expected=False, expected_reject_code=403)
