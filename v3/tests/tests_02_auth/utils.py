import json

import requests

from ....global_services.data_generators.marketplace_service.utils import get_mps_apps_list
from ....global_services.data_generators.singletons import login_manager
from ...data_generators.case_data import get_test_cases
from ...data_generators.medical_data_service import utils, utils_examination
from ..utils import read_case


def api_auth_check(
    base_url,
    headers,
    excluded_paths,
    url_suffix="",
    openapi_suffix="/openapi.json",
    auth_expected=True,
    expected_reject_code=401,
    scope_id=None,
    expected_error_message=None,
):
    base_url = str(base_url).rstrip("/")

    r = requests.get(f"{base_url}{url_suffix}{openapi_suffix}")
    openapi = json.loads(r.content)
    if headers:
        expected_status_code = "!= 401" if auth_expected else f"{expected_reject_code}"
    else:
        expected_status_code = f"{expected_reject_code}"
    for path, ops in openapi["paths"].items():
        if path in excluded_paths:
            continue
        path = path.replace("scope_id", str(scope_id))
        path_no_vars = path.replace("{", "").replace("}", "")
        url = f"{base_url}{url_suffix}{path_no_vars}"
        for op in ops.keys():
            r = requests.request(op, url, headers=headers)
            print(f"AUTH - '{op}': '{url}', - STATUS CODE: '{r.status_code}' - EXPECTED: '{expected_status_code}'")
            if auth_expected:
                assert r.status_code != 401 and r.status_code != 500
            else:
                assert r.status_code == expected_reject_code
                if expected_error_message:
                    assert r.json()["detail"] == expected_error_message


def api_auth_check_wbs3_scope(
    base_url,
    headers,
    scope_id,
    excluded_paths,
    openapi_suffix="/openapi.json",
    auth_expected=True,
    expected_reject_code=401,
):
    api_auth_check(
        base_url,
        headers,
        excluded_paths,
        url_suffix="/scopes",
        openapi_suffix=openapi_suffix,
        auth_expected=auth_expected,
        expected_reject_code=expected_reject_code,
    )


def get_scope():
    # read case from file
    case = read_case(get_test_cases, "AUTH_CASES_v3", "TEST01v3", "v3")
    case_id = case["case_id"]

    headers_wbs = login_manager.wbs_client()
    app_list = get_mps_apps_list(headers_wbs).json()

    for item in app_list["items"]:
        v3_active_app_view = item["active_app_views"]["v3"]
        if v3_active_app_view:
            app_id = v3_active_app_view["app"]["id"]
            break

    patho_user_id = login_manager.patho_user()["user-id"]
    ex_id = utils.add_or_get_examination_to_case(patho_user_id, case_id, app_id)

    r = utils_examination.mds_put_scope(headers_wbs, ex_id, patho_user_id)
    assert r.status_code == 201 or r.status_code == 200
    scope_id = r.json()["id"]
    r = utils_examination.mds_get_scope_token(headers_wbs, scope_id)
    assert r.status_code == 200

    data = r.json()
    scope_access_token = data["access_token"]
    return scope_id, {"Authorization": f"Bearer {scope_access_token}"}
