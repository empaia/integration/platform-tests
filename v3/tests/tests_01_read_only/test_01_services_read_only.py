from ....global_services.data_generators.singletons import general_settings
from .utils import api_check, api_check_wbs2_scope


# MDS
def test_mds_read_only():
    api_check(general_settings.mds_host, None)
    api_check(str(general_settings.mds_host) + "v1", None)


# WBS
def test_wbs_read_only():
    api_check(general_settings.wbs_host, None)
    api_check(str(general_settings.wbs_host) + "v2", None)
    api_check_wbs2_scope(str(general_settings.wbs_host) + "v2", None)


# JES
def test_jes_read_only():
    api_check(general_settings.jes_host, None)


# IDMS
def test_idms_read_only():
    api_check(general_settings.idm_host, None)


# US
def test_us_read_only():
    api_check(general_settings.us_host, None)


# AS
def test_as_read_only():
    api_check(general_settings.as_host, None)
