import json

import requests

EXPECTED_RESPONSE_CODES = [200, 204, 400, 403, 404, 413, 415, 422]


def api_check(base_url, headers, url_suffix="", openapi_suffix="openapi.json"):
    base_url = str(base_url).rstrip("/")
    if url_suffix and url_suffix != "":
        base_url = f"{base_url}/{url_suffix}"
    r = requests.get(f"{base_url}/{openapi_suffix}")
    r.raise_for_status()
    openapi = json.loads(r.content)
    for path, ops in openapi["paths"].items():
        path_no_vars = path.replace("{", "").replace("}", "")
        url = f"{base_url}{url_suffix}{path_no_vars}"
        for op in ops.keys():
            r = requests.request(op, url, headers=headers)
            print(f"API TEST - '{op}': '{url}', - STATUS CODE: '{r.status_code}'")
            print(r.content)
            assert r.status_code in EXPECTED_RESPONSE_CODES


def api_check_wbs2_scope(base_url, headers, openapi_suffix="openapi.json"):
    api_check(base_url, headers, url_suffix="scopes", openapi_suffix=openapi_suffix)
