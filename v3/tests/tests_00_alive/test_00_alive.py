from pprint import pprint

import requests

from ....global_services.data_generators.singletons import general_settings, login_manager


def test_alive_mds_and_subservices():
    url = f"{general_settings.mds_host}alive"
    headers = login_manager.wbs_client()
    r = requests.get(url, headers=headers)
    print("MDS alive: ", r.status_code, r.content)
    assert r.status_code == 200
    r_json = r.json()
    alive_services = []
    dead_services = []
    for service in r_json["services"]:
        print("MDS alive, sub service: ", service)
        if r_json["services"][service]["status"] == "ok":
            alive_services.append(service)
        else:
            dead_services.append({service: r_json["services"][service]})
    pprint(dead_services)
    assert len(alive_services) == len(r_json["services"])


def test_alive_wbs():
    url = f"{general_settings.wbs_host}alive"
    headers = login_manager.patho_user()
    r = requests.get(url, headers=headers)
    print("WBS alive: ", r.status_code, r.content)
    assert r.status_code == 200


def test_alive_app_service():
    url = f"{general_settings.as_host}alive"
    headers = login_manager.as_client()
    r = requests.get(url, headers=headers)
    print("AS alive: ", r.status_code, r.content)
    assert r.status_code == 200


def test_alive_jes():
    url = f"{general_settings.jes_host}alive"
    headers = login_manager.wbs_client()
    r = requests.get(url, headers=headers)
    print("JES alive: ", r.status_code, r.content)
    assert r.status_code == 200


def test_alive_idms():
    url = f"{general_settings.idm_host}alive"
    headers = login_manager.mta_user()
    r = requests.get(url, headers=headers)
    print("IDMS alive: ", r.status_code, r.content)
    assert r.status_code == 200


def test_alive_us():
    url = f"{general_settings.us_host}alive"
    headers = login_manager.mta_user()
    r = requests.get(url, headers=headers)
    print("IDMS alive: ", r.status_code, r.content)
    assert r.status_code == 200
