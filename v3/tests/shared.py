from time import sleep

import requests

from ...global_services.data_generators.singletons import general_settings, login_manager


def _wait_job_status(url, expected_status, timeout):
    _wait_job_status_field(url, "status", expected_status, timeout)


def _wait_job_validation_status(url, i_or_o, expected_status, timeout):
    _wait_job_status_field(url, f"{i_or_o}_validation_status", expected_status, timeout)


def _wait_job_status_field(url, status_field, expected_status, timeout):
    for _ in range(timeout):
        headers = login_manager.wbs_client()
        r = requests.get(url, headers=headers)
        print("Get job:", r.text)
        r.raise_for_status()
        data = r.json()
        if data[status_field] == expected_status:
            return
        elif data[status_field] == "ERROR":
            print(f"JES / MDS Response status / content: {r.status_code} / {r.content}")
            error_msg = f"Job ERROR at MDS/JES [{url}]"
            raise AssertionError(error_msg)
        sleep(1)

    print(f"JES / MDS Response status / content: {r.status_code} / {r.content}")
    actual_status = data[status_field]
    error_msg = (
        f"Job did not change to expected {status_field} {expected_status} (it is {actual_status}) at MDS/JES [{url}]"
    )
    raise AssertionError(error_msg)


def _wait_for_job_at_jes(url, timeout):
    for _ in range(timeout):
        headers = login_manager.wbs_client()
        r = requests.get(url, headers=headers)
        if r.status_code == 200:
            return

        assert r.status_code == 404
        sleep(1)

    print(f"JES / MDS Response status / content: {r.status_code} / {r.content}")
    error_msg = f"Job not found at JES [{url}]"
    raise AssertionError(error_msg)


def wait_js_job_status(job_id, final_status, timeout=180):
    url = f"{general_settings.mds_host}v3/jobs/{job_id}"
    _wait_job_status(url=url, expected_status=final_status, timeout=timeout)


def wait_js_job_input_validation_status(job_id, final_status, timeout=180):
    url = f"{general_settings.mds_host}v3/jobs/{job_id}"
    _wait_job_validation_status(url, "input", expected_status=final_status, timeout=timeout)


def wait_js_job_output_validation_status(job_id, final_status, timeout=180):
    url = f"{general_settings.mds_host}v3/jobs/{job_id}"
    _wait_job_validation_status(url, "output", expected_status=final_status, timeout=timeout)


def get_job(job_id):
    url = f"{general_settings.mds_host}v3/jobs/{job_id}"
    headers = login_manager.wbs_client()
    r = requests.get(url, headers=headers)
    r.raise_for_status()
    data = r.json()
    return data


def wait_jes_job_status(job_id, final_status, timeout=180):
    url = f"{general_settings.jes_host}v1/executions/{job_id}"
    _wait_for_job_at_jes(url=url, timeout=timeout)
    _wait_job_status(url=url, expected_status=final_status, timeout=timeout)


def check_js_job_status(job_id, final_status):
    url = f"{general_settings.mds_host}v3/jobs/{job_id}"
    _wait_job_status(url=url, expected_status=final_status, timeout=1)


def check_jes_job_status(job_id, final_status):
    url = f"{general_settings.jes_host}v1/executions/{job_id}"
    _wait_job_status(url=url, expected_status=final_status, timeout=1)


def check_js_job_error_message(job_id, error_message):
    url = f"{general_settings.mds_host}v3/jobs/{job_id}"
    headers = login_manager.wbs_client()
    r = requests.get(url, headers=headers)
    r.raise_for_status()
    job = r.json()
    print(f"JOB: {str(job)}")
    assert error_message == job.get("error_message")
