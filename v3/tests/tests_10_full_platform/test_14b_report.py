from time import sleep

import requests
from sample_apps.sample_apps.test.internal._ITA13_job_report import v3 as app

from platform_tests.global_services.data_generators.marketplace_service.utils import get_app_id_by_ead
from platform_tests.global_services.data_generators.singletons import general_settings, login_manager
from platform_tests.v3.data_generators.case_data import get_test_cases
from platform_tests.v3.data_generators.medical_data_service import utils, utils_job
from platform_tests.v3.data_generators.workbench_service import gen_data_ITA13_v3
from platform_tests.v3.data_generators.workbench_service import utils as wbs_utils
from platform_tests.v3.tests.shared import wait_jes_job_status, wait_js_job_status
from platform_tests.v3.tests.utils import create_scope, read_case

from .test_14a_report import close_examination


def test_app():
    patho_user_id = login_manager.patho_user()["user-id"]

    # read case from file
    case = read_case(get_test_cases, "FULL_PLATFORM_CASES_v3", "TEST14cv3", "v3")
    case_id = case["case_id"]
    slide_ids = case["slide_ids"]

    # add examination or get existing
    ead = app.ead
    headers = login_manager.wbs_client()
    app_id = get_app_id_by_ead(headers, ead)
    ex_id, status_code = utils.add_or_get_examination_to_case(patho_user_id, case_id, app_id, return_status=True)
    if status_code == 200:
        close_examination(ex_id)
        ex_id = utils.add_or_get_examination_to_case(patho_user_id, case_id, app_id)

    scope_id, scope_header = create_scope(ex_id)

    # only create inputs
    inputs_by_key, _ = gen_data_ITA13_v3._gen_inputs_standalone(ead, slide_ids[0])
    job_mode = "STANDALONE"
    _in_job, job_id, _token = utils.create_job(ead, app_id, patho_user_id, ex_id)
    wbs_utils.add_inputs_to_job(job_id, inputs_by_key, scope_id, scope_header)
    wbs_utils.run_job(job_id, scope_id, scope_header)

    final_jes_status = "TERMINATED"
    final_job_status = "COMPLETED"

    wait_jes_job_status(job_id=job_id, final_status=final_jes_status)
    wait_js_job_status(job_id=job_id, final_status=final_job_status)
    gen_data_ITA13_v3.check_containerized_app_output(job_id, job_mode)

    # new report job
    payload = {
        "creator_id": scope_id,
        "creator_type": "SCOPE",
        "mode": "REPORT",
        "containerized": False,
    }
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/jobs"
    r = requests.post(url, headers=scope_header, json=payload)
    print(r.text)
    assert r.status_code == 201
    report_job = r.json()
    report_job_id = report_job["id"]

    # Close Examination WITHOUT outputs on report
    # should succeed
    r = close_examination(ex_id)
    assert r.status_code == 200
    ex = r.json()
    assert ex["state"] == "CLOSED"

    headers = login_manager.wbs_client()
    r = utils_job.get_job(headers, report_job_id)
    assert r.status_code == 200
    report_job = r.json()
    assert report_job["status"] == "COMPLETED"

    # wait job-valdiation to set output_validation_status
    wait_js_job_validation(
        job_id=report_job["id"],
        output_val_status="ERROR",
        output_val_msg="Missing output my_string in job",
    )


def wait_js_job_validation(
    job_id,
    input_val_status=None,
    output_val_status=None,
    input_val_msg=None,
    output_val_msg=None,
    timeout=30,
):
    timeout_count = 0
    headers = login_manager.wbs_client()
    r = utils_job.get_job(headers, job_id)
    job = r.json()
    final_states = ["ERROR", "COMPLETED"]
    while job["output_validation_status"] not in final_states or job["input_validation_status"] not in final_states:
        if timeout_count >= timeout:
            raise AssertionError("job validation timeout exceeded")
        headers = login_manager.wbs_client()
        r = utils_job.get_job(headers, job_id)
        assert r.status_code == 200
        job = r.json()
        sleep(2)
        timeout_count += 2
    # print(job["input_validation_status"])
    # print(job["output_validation_status"])
    # print(job["input_validation_error_message"])
    # print(job["output_validation_error_message"])
    if input_val_status:
        assert job["input_validation_status"] == input_val_status
    if output_val_status:
        assert job["output_validation_status"] == output_val_status
    if input_val_msg:
        assert job["input_validation_error_message"] == input_val_msg
    if output_val_msg:
        assert job["output_validation_error_message"] == output_val_msg
