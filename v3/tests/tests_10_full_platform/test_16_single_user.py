import pytest
import requests
from sample_apps.sample_apps.tutorial._TA01_simple_app import v3 as app

from ....global_services.data_generators.marketplace_service import utils as utils_mps
from ....global_services.data_generators.singletons import general_settings, login_manager, multi_user_settings
from ...data_generators.medical_data_service import utils
from ..shared import wait_js_job_status
from ..utils import create_scope, create_scope_mta, query_scopes, read_case_from_file, wbs_post_job_data


@pytest.mark.skipif(not multi_user_settings.disable_multi_user, reason="WBS_DISABLE_MULTI_USER set to False in .env")
def test_single_user():
    patho_user_id = login_manager.patho_user()["user-id"]

    # read cases from file
    case_id, slide_ids = read_case_from_file("WBS_MDS_CASES_v3", "TEST11v3", "v3")
    slide = slide_ids[0]

    headers = login_manager.wbs_client()
    app_id_1 = utils_mps.get_app_id_by_ead(headers, app.ead)

    ex_id_1 = utils.add_or_get_examination_to_case(patho_user_id, case_id, app_id_1)

    scope_id_1, scope_header_1 = create_scope(ex_id_1)
    scope_id_2, scope_header_2 = create_scope_mta(ex_id_1)

    # scopes query only must return own scope
    scopes = query_scopes(scope_id_1, scope_header_1)
    assert scopes["item_count"] == 1
    assert len(scopes["items"]) == 1
    assert scopes["items"][0]["id"] == scope_id_1

    scopes = query_scopes(scope_id_2, scope_header_2)
    assert scopes["item_count"] == 1
    assert len(scopes["items"]) == 1
    assert scopes["items"][0]["id"] == scope_id_2

    # data query only accepts own scope
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/annotations/query"
    query = {"creators": [scope_id_1]}
    r = requests.put(url, json=query, headers=scope_header_1)
    assert r.status_code == 200

    expected_error = "Either a valid creator_id or job list or a list of annotations must be set as query parameter"
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/annotations/query"
    query = {"creators": [scope_id_2]}
    r = requests.put(url, json=query, headers=scope_header_1)
    assert r.status_code == 400
    assert r.json()["detail"] == expected_error

    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/annotations/query"
    query = {"creators": [scope_id_1, scope_id_2]}
    r = requests.put(url, json=query, headers=scope_header_1)
    assert r.status_code == 400
    assert r.json()["detail"] == expected_error

    # Post annotation as user 1
    rect = {
        "type": "rectangle",
        "upper_left": [10000, 20000],
        "width": 100,
        "height": 100,
        "npp_created": 5000,
        "npp_viewing": [1, 50000],
        "creator_type": "scope",
        "reference_id": "some-id",
        "reference_type": "wsi",
    }

    rect_1 = rect
    rect_1["name"] = "rect_1"
    rect_1["creator_id"] = scope_id_1

    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/annotations"
    r = requests.post(url, json=rect_1, headers=scope_header_1)
    print("Create annotation in scope:", r.text)
    assert r.status_code == 201
    rect_id_1 = r.json()["id"]

    # Post annotation as user 2
    rect_2 = rect
    rect_2["name"] = "rect_2"
    rect_2["creator_id"] = scope_id_2

    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/annotations"
    r = requests.post(url, json=rect_2, headers=scope_header_2)
    print("Create annotation in scope:", r.text)
    assert r.status_code == 201
    rect_id_2 = r.json()["id"]

    # query annotations as user 1
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/annotations/query"
    query = {"creators": [scope_id_1]}
    r = requests.put(url, json=query, headers=scope_header_1)
    assert r.status_code == 200
    data = r.json()

    creators = [item["creator_id"] for item in data["items"]]
    assert str(scope_id_1) in creators
    assert str(scope_id_2) not in creators

    # query annotations as user 2
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/annotations/query"
    query = {"creators": [scope_id_2]}
    r = requests.put(url, json=query, headers=scope_header_2)
    assert r.status_code == 200
    data = r.json()

    creators = [item["creator_id"] for item in data["items"]]
    assert str(scope_id_1) not in creators
    assert str(scope_id_2) in creators

    # create job for both users
    job_data_1 = wbs_post_job_data(scope_id_1, "SCOPE")
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/jobs"
    r = requests.post(url, json=job_data_1, headers=scope_header_1)
    assert r.status_code == 201
    job_1 = r.json()

    job_data_2 = wbs_post_job_data(scope_id_2, "SCOPE")
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/jobs"
    r = requests.post(url, json=job_data_2, headers=scope_header_2)
    assert r.status_code == 201
    job_2 = r.json()

    job_id_1 = job_1["id"]
    job_id_2 = job_2["id"]

    # only jobs of user for status ASSEMBLY
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/jobs"
    r = requests.get(url, headers=scope_header_1)
    assert r.status_code == 200
    job_list = r.json()
    job_ids = [j["id"] for j in job_list["items"]]
    assert job_1["id"] in job_ids
    assert job_2["id"] not in job_ids

    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/jobs"
    r = requests.get(url, headers=scope_header_2)
    assert r.status_code == 200
    job_list = r.json()
    job_ids = [j["id"] for j in job_list["items"]]
    assert job_2["id"] in job_ids
    assert job_1["id"] not in job_ids

    # add inputs to jobs
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/jobs/{job_id_1}/inputs/my_wsi"
    r = requests.put(url, json=dict(id=slide), headers=scope_header_1)
    assert r.status_code == 200
    updated_job = r.json()
    assert updated_job["inputs"]["my_wsi"] == slide

    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/jobs/{job_id_1}/inputs/my_rectangle"
    r = requests.put(url, json=dict(id=rect_id_1), headers=scope_header_1)
    assert r.status_code == 200

    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/jobs/{job_id_2}/inputs/my_wsi"
    r = requests.put(url, json=dict(id=slide), headers=scope_header_2)
    assert r.status_code == 200
    updated_job = r.json()
    assert updated_job["inputs"]["my_wsi"] == slide

    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/jobs/{job_id_2}/inputs/my_rectangle"
    r = requests.put(url, json=dict(id=rect_id_2), headers=scope_header_2)
    assert r.status_code == 200

    # set jobs READY
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/jobs/{job_id_1}/run"
    r = requests.put(url, headers=scope_header_1)
    assert r.status_code == 200
    job = r.json()
    assert job["status"] == "READY"

    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/jobs/{job_id_2}/run"
    r = requests.put(url, headers=scope_header_2)
    assert r.status_code == 200
    job = r.json()
    assert job["status"] == "READY"

    wait_js_job_status(job_id=job_id_1, final_status="COMPLETED")
    wait_js_job_status(job_id=job_id_2, final_status="COMPLETED")

    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/jobs"
    r = requests.get(url, headers=scope_header_1)
    assert r.status_code == 200
    job_list = r.json()
    job_ids = [j["id"] for j in job_list["items"]]
    assert job_1["id"] in job_ids
    assert job_2["id"] not in job_ids

    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/jobs"
    r = requests.get(url, headers=scope_header_2)
    assert r.status_code == 200
    job_list = r.json()
    job_ids = [j["id"] for j in job_list["items"]]
    assert job_1["id"] not in job_ids
    assert job_2["id"] in job_ids
