import uuid

import requests
from sample_apps.sample_apps.test.internal._ITA02_wsi_collection import v3 as ita02v3
from sample_apps.sample_apps.test.internal._ITA13_job_report import v3 as ita13v3
from sample_apps.sample_apps.tutorial._TA01_simple_app import v3 as ta01v3

from ....global_services.data_generators.marketplace_service.utils import get_app_id_by_ead
from ....global_services.data_generators.singletons import general_settings, login_manager
from ...data_generators.workbench_service import utils as wbs_utils
from .. import utils
from ..shared import wait_js_job_status


def test_wbs_scoped_jobs():
    case_id, slide_ids = utils.read_case_from_file("FULL_PLATFORM_CASES_v3", "TEST06v3", "v3")

    app_id_1 = get_app_id_by_ead(login_manager.wbs_client(), ta01v3.ead)
    app_id_2 = get_app_id_by_ead(login_manager.wbs_client(), ita02v3.ead)

    ex_id_1 = utils.add_or_get_examination_to_case(case_id, app_id_1)
    ex_id_2 = utils.add_or_get_examination_to_case(case_id, app_id_2)

    scope_id_1, scope_header_1 = utils.create_scope(ex_id_1)
    scope_id_2, scope_header_2 = utils.create_scope(ex_id_2)

    # New jobs that don't use SCOPE as creator_type must be rejected
    job_data_1 = utils.wbs_post_job_data(scope_id_1, "USER")
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/jobs"
    r = requests.post(url, json=job_data_1, headers=scope_header_1)
    print(r.text)
    assert r.status_code == 412

    # New jobs that don't use scope_id as creator_id must be rejected
    job_data_1 = utils.wbs_post_job_data(str(uuid.uuid4()), "SCOPE")
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/jobs"
    r = requests.post(url, json=job_data_1, headers=scope_header_1)
    assert r.status_code == 412

    # New valid jobs in different scopes need to be separated from each other
    job_data_1["creator_id"] = scope_id_1
    job_data_1["creator_type"] = "SCOPE"
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/jobs"
    r = requests.post(url, json=job_data_1, headers=scope_header_1)
    assert r.status_code == 201
    job_1 = r.json()

    job_data_2 = utils.wbs_post_job_data(scope_id_2, "SCOPE")
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/jobs"
    r = requests.post(url, json=job_data_2, headers=scope_header_2)
    assert r.status_code == 201
    job_2 = r.json()

    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/jobs"
    r = requests.get(url, headers=scope_header_1)
    assert r.status_code == 200
    job_list_1 = r.json()
    job_ids_1 = [j["id"] for j in job_list_1["items"]]
    assert job_1["id"] in job_ids_1
    assert job_2["id"] not in job_ids_1

    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/jobs"
    r = requests.get(url, headers=scope_header_2)
    assert r.status_code == 200
    job_list_2 = r.json()
    job_ids_2 = [j["id"] for j in job_list_2["items"]]
    assert job_2["id"] in job_ids_2
    assert job_1["id"] not in job_ids_2

    # Adding another valid job in existing scope with pre-existing job will group both under this scope
    job_data_1["creator_id"] = scope_id_1
    job_data_1["creator_type"] = "SCOPE"
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/jobs"
    r = requests.post(url, json=job_data_1, headers=scope_header_1)
    job_new_1 = r.json()
    assert r.status_code == 201
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/jobs"
    r = requests.get(url, headers=scope_header_1)
    assert r.status_code == 200
    job_list_1_new = r.json()
    assert job_list_1_new["item_count"] == job_list_1["item_count"] + 1
    assert job_new_1["creator_id"] == scope_id_1

    # Job specific route tests
    job_id_1 = job_1["id"]
    job_id_2 = job_2["id"]

    # Prevent adding an input to a job not belonging to given scope
    input_id = str(uuid.uuid4())
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/jobs/{job_id_1}/inputs/my_wsi"
    r = requests.put(url, json=dict(id=input_id), headers=scope_header_2)
    assert r.status_code == 412

    # Adding an unknown input to a job belonging to given scope is a 400 bad request (as defined by JS)
    input_id = str(uuid.uuid4())
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/jobs/{job_id_1}/inputs/invalid_key"
    r = requests.put(url, json=dict(id=input_id), headers=scope_header_1)
    assert r.status_code == 400

    # Allow adding an input to a job belonging to given scope
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/jobs/{job_id_1}/inputs/my_wsi"
    r = requests.put(url, json=dict(id=input_id), headers=scope_header_1)
    assert r.status_code == 200
    updated_job = r.json()
    assert updated_job["inputs"]["my_wsi"] == input_id

    # Prevent deleting an input from a job not belonging to given scope
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/jobs/{job_id_2}/inputs/my_wsi"
    r = requests.delete(url, headers=scope_header_1)
    assert r.status_code == 412

    # Deleting an unknown input from a job belonging to given scope is a 400 bad request (as defined by JS)
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/jobs/{job_id_1}/inputs/invalid_key"
    r = requests.delete(url, headers=scope_header_1)
    assert r.status_code == 400

    # Allow deleting an input from a job belonging to given scope
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/jobs/{job_id_1}/inputs/my_wsi"
    r = requests.delete(url, headers=scope_header_1)
    assert r.status_code == 200
    updated_job = r.json()
    assert not updated_job["inputs"].get("my_wsi")

    # Prevent getting the slides of a job which does not belong to the given scope
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/jobs/{job_id_1}/slides"
    r = requests.get(url, headers=scope_header_2)
    assert r.status_code == 412

    # Allow getting the slides of a job which belongs to the given scope
    # 1. empty list for jobs without wsi inputs set
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/jobs/{job_id_2}/slides"
    r = requests.get(url, headers=scope_header_2)
    assert r.status_code == 200
    slide_list = r.json()
    assert slide_list["item_count"] == 0

    # 2. create slide and set input
    slide = slide_ids[0]
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/jobs/{job_id_2}/inputs/my_wsi"
    r = requests.put(url, json=dict(id=slide), headers=scope_header_2)
    assert r.status_code == 200

    # 2. slide list is now filled with one slide
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/jobs/{job_id_2}/slides"
    r = requests.get(url, headers=scope_header_2)
    assert r.status_code == 200
    slide_list = r.json()
    assert slide_list["item_count"] == 1
    assert slide_list["items"][0]["id"] == slide

    # 3. create slide collection and set input
    slide_2 = slide_ids[1]
    slide_3 = slide_ids[2]
    wsi_collection = create_collection_of_slides(scope_id_2, scope_header_2, slide, slide_2, slide_3)
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/jobs/{job_id_2}/inputs/my_wsis_1"
    r = requests.put(url, json=dict(id=wsi_collection["id"]), headers=scope_header_2)
    assert r.status_code == 200

    # 4. get all slides should now include all input slides inserted until now
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/jobs/{job_id_2}/slides"
    r = requests.get(url, headers=scope_header_2)
    assert r.status_code == 200
    slide_list = r.json()
    assert slide_list["item_count"] == 4

    # Prevent running a job which does not belong to the given scope
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/jobs/{job_id_2}/run"
    r = requests.put(url, headers=scope_header_1)
    assert r.status_code == 412

    # add valid slide
    input_id = slide
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/jobs/{job_id_1}/inputs/my_wsi"
    r = requests.put(url, json=dict(id=input_id), headers=scope_header_1)
    assert r.status_code == 200

    # 2. add valid annotation
    rectangle = create_rectangle_annotation(slide, scope_id_1, scope_header_1)
    input_id = rectangle["id"]
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/jobs/{job_id_1}/inputs/my_rectangle"
    r = requests.put(url, json=dict(id=input_id), headers=scope_header_1)
    assert r.status_code == 200

    # 3. run should succeed and should have put the job to scheduled state
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/jobs/{job_id_1}/run"
    r = requests.put(url, headers=scope_header_1)
    assert r.status_code == 200
    job = r.json()
    assert job["status"] == "READY"

    # Prevent deletion of job which does not belong to the given scope
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/jobs/{job_id_1}"
    r = requests.delete(url, headers=scope_header_2)
    assert r.status_code == 412

    # Allow deletion of job which belongs to the given scope
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/jobs/{job_id_2}"
    r = requests.delete(url, headers=scope_header_2)
    assert r.status_code == 200
    deleted_job = r.json()
    assert deleted_job["id"] == job_2["id"]

    wait_js_job_status(job_id=job_id_1, final_status="COMPLETED")

    # check apps in examination
    for ex_id, app_id in [[ex_id_1, app_id_1], [ex_id_2, app_id_2]]:
        url = f"{general_settings.wbs_host}v3/examinations/{ex_id}"
        headers = login_manager.patho_user()
        r = requests.get(url, headers=headers)
        assert r.status_code == 200
        ex = r.json()
        assert ex["app_id"] == app_id
        app = ex["app"]
        assert app["app_id"] == app_id
        if app["app_id"] == app_id_1:
            assert ex["jobs_count"] > len(job_ids_1)
        if app["app_id"] == app_id_2:
            assert ex["jobs_count"] < len(job_ids_2)  # deleted job


def test_wbs_scoped_jobs_delete_outputs():
    case_id, _ = utils.read_case_from_file("FULL_PLATFORM_CASES_v3", "TEST06v3", "v3")

    app_id = get_app_id_by_ead(login_manager.wbs_client(), ita13v3.ead)

    ex_id = utils.add_or_get_examination_to_case(case_id, app_id)

    scope_id, scope_header = utils.create_scope(ex_id)

    # create job
    job_output_data = utils.wbs_post_job_data(scope_id, "SCOPE")
    job_output_data["mode"] = "POSTPROCESSING"
    job_output_data["containerized"] = False
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/jobs"
    r = requests.post(url, json=job_output_data, headers=scope_header)
    assert r.status_code == 201
    job_output = r.json()
    job_output_id = job_output["id"]

    # add output
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/jobs/{job_output_id}/outputs/my_string"
    r = requests.put(url, json=dict(id="my_id"), headers=scope_header)
    assert r.status_code == 200
    updated_job = r.json()
    assert updated_job["outputs"]["my_string"] == "my_id"

    # delete output
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/jobs/{job_output_id}/outputs/my_string"
    r = requests.delete(url, headers=scope_header)
    assert r.status_code == 200
    updated_job = r.json()
    assert not updated_job["outputs"].get("my_string")

    # delete job
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/jobs/{job_output_id}"
    r = requests.delete(url, headers=scope_header)
    assert r.status_code == 200


def create_point_annotation(slide_id, scope_id, scope_headers):
    annotation_data = wbs_utils.generate_point_json(slide_id, scope_id, creator_type="scope")
    r = wbs_utils.wbs_post_annotation(scope_headers, scope_id, annotation_data)
    return r.json()


def create_rectangle_annotation(slide_id, scope_id, scope_headers):
    annotation_data = wbs_utils.generate_rectangle_json(slide_id, scope_id, creator_type="scope")
    r = wbs_utils.wbs_post_annotation(scope_headers, scope_id, annotation_data)
    return r.json()


def create_collection_of_slides(scope_id, scope_headers, *slides):
    data = {
        "name": "slides",
        "creator_id": scope_id,
        "creator_type": "scope",
        "item_type": "wsi",
        "type": "collection",
        "items": [{"id": slide, "type": "wsi"} for slide in slides],
    }
    response = wbs_utils.wbs_post_collection(scope_headers, scope_id, data)
    return response.json()
