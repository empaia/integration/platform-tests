import requests
from sample_apps.sample_apps.test.internal._ITA13_job_report import v3 as app

from platform_tests.global_services.data_generators.marketplace_service.utils import get_app_id_by_ead
from platform_tests.global_services.data_generators.singletons import general_settings, login_manager
from platform_tests.v3.data_generators.case_data import get_test_cases
from platform_tests.v3.data_generators.medical_data_service import utils, utils_job
from platform_tests.v3.data_generators.workbench_service import gen_data_ITA13_v3
from platform_tests.v3.data_generators.workbench_service import utils as wbs_utils
from platform_tests.v3.tests.shared import wait_jes_job_status, wait_js_job_status
from platform_tests.v3.tests.utils import create_scope, read_case


def test_app():
    patho_user_id = login_manager.patho_user()["user-id"]

    # read case from file
    case = read_case(get_test_cases, "FULL_PLATFORM_CASES_v3", "TEST14av3", "v3")
    case_id = case["case_id"]
    slide_ids = case["slide_ids"]

    # add examination or get existing
    ead = app.ead
    headers = login_manager.wbs_client()
    app_id = get_app_id_by_ead(headers, ead)
    ex_id, status_code = utils.add_or_get_examination_to_case(patho_user_id, case_id, app_id, return_status=True)
    if status_code == 200:
        close_examination(ex_id)
        ex_id = utils.add_or_get_examination_to_case(patho_user_id, case_id, app_id)

    scope_id, scope_header = create_scope(ex_id)

    # only create inputs
    inputs_by_key, _ = gen_data_ITA13_v3._gen_inputs_standalone(ead, slide_ids[0])
    job_mode = "STANDALONE"
    _, job_id, _ = utils.create_job(ead, app_id, patho_user_id, ex_id)
    wbs_utils.add_inputs_to_job(job_id, inputs_by_key, scope_id, scope_header)
    wbs_utils.run_job(job_id, scope_id, scope_header)

    final_jes_status = "TERMINATED"
    final_job_status = "COMPLETED"

    wait_jes_job_status(job_id=job_id, final_status=final_jes_status)
    wait_js_job_status(job_id=job_id, final_status=final_job_status)
    gen_data_ITA13_v3.check_containerized_app_output(job_id, job_mode)

    # new report job
    payload = {
        "creator_id": scope_id,
        "creator_type": "SCOPE",
        "mode": "REPORT",
        "containerized": False,
    }
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/jobs"
    r = requests.post(url, headers=scope_header, json=payload)
    print(r.text)
    assert r.status_code == 201
    report_job = r.json()
    report_job_id = report_job["id"]

    # 2nd report job should fail
    r = requests.post(url, headers=scope_header, json=payload)
    print(r.text)
    assert r.status_code == 409
    assert "There already exists a report job" in r.text

    # finalize - EXPECT ERROR (reports can only be closed be closing examination)
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/jobs/{report_job_id}/finalize"
    r = requests.put(url, headers=scope_header, json=payload)
    print(r.text)
    assert r.status_code == 412
    assert "Report jobs are automatically finalized when closing the examination" in r.text

    # create output
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/primitives"
    my_string = {
        "name": "my_string",
        "value": "REPORT",
        "type": "string",
        "creator_type": "scope",  # NEW required in v3 apps
        "creator_id": scope_id,  # NEW required in v3 apps
    }
    r = requests.post(url, headers=scope_header, json=my_string)
    assert r.status_code == 201
    my_string_id = r.json()["id"]
    # add output
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/jobs/{report_job_id}/outputs/my_string"
    r = requests.put(url, headers=scope_header, json={"id": my_string_id})
    print(r.text)
    assert r.status_code == 200

    # Close Examination
    r = close_examination(ex_id)
    assert r.status_code == 200
    ex = r.json()
    assert ex["state"] == "CLOSED"

    headers = login_manager.wbs_client()
    r = utils_job.get_job(headers, report_job_id)
    assert r.status_code == 200
    report_job = r.json()
    assert report_job["status"] == "COMPLETED"


def close_examination(ex_id):
    url = f"{general_settings.wbs_host}v3/examinations/{ex_id}/close"
    r = requests.put(url, headers=login_manager.patho_user())
    print(r.text)
    return r
