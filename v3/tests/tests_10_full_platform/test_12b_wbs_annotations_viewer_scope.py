import requests

from ....global_services.data_generators.marketplace_service.utils import get_app_id_by_ead
from ....global_services.data_generators.singletons import general_settings, login_manager
from ...data_generators.case_data import get_test_cases
from ...data_generators.medical_data_service import utils, utils_job
from ...data_generators.workbench_service import gen_data_TA06_v3
from ..shared import wait_js_job_status
from ..utils import create_scope, read_case


def get_test_app_06_ids(case_id):
    url = f"{general_settings.wbs_host}v3/examinations/query"
    headers = login_manager.patho_user()
    r = requests.put(url, headers=headers, json={"cases": [case_id]})
    examinations = r.json()
    print(examinations)

    if len(examinations["items"]) == 0:
        return None, None, None

    if len(examinations["items"][0]["jobs"]) == 0:
        return None, None, None

    ex_id = examinations["items"][0]["id"]
    job_id = examinations["items"][0]["jobs"][0]["id"]
    job = utils.utils_job.get_job(login_manager.wbs_client(), job_id).json()
    app_id = job["app_id"]
    return job_id, ex_id, app_id


def test_annotations_viewer_scope():
    # read cases from file
    case = read_case(get_test_cases, "FULL_PLATFORM_CASES_v3", "TEST12Bv3", "v3")
    case_id = case["case_id"]
    slide_ids = case["slide_ids"]
    npp_ranges = case["npp_ranges"]

    slide_id = slide_ids[0]

    job_id, ex_id, app_id = get_test_app_06_ids(case_id)

    if job_id:
        scope_id, scope_header = create_scope(ex_id)

    if job_id is None:
        patho_user_id = login_manager.patho_user()["user-id"]
        app_id = get_app_id_by_ead(login_manager.wbs_client(), gen_data_TA06_v3.app.ead)
        ex_id = utils.add_or_get_examination_to_case(patho_user_id, case_id, app_id)
        scope_id, scope_header = create_scope(ex_id)
        # tutorial 06
        _, job_id = gen_data_TA06_v3.generate(
            scope_id, scope_header, slide_id, ex_id, case_id, npp_ranges=npp_ranges, set_job_ready=True
        )

    wait_js_job_status(job_id=job_id, final_status="COMPLETED", timeout=300)

    # queries
    query_high = {
        "references": [slide_id],
        "jobs": [job_id],
        "viewport": {"x": 1, "y": 1, "width": 73985, "height": 27305},
        "npp_viewing": [npp_ranges[-1] - 250, npp_ranges[-1] + 250],
    }

    query_low = {
        "references": [slide_id],
        "jobs": [job_id],
        "viewport": {"x": 1, "y": 1, "width": 73985, "height": 27305},
        "npp_viewing": [npp_ranges[0] - 100, npp_ranges[0] + 100],
    }

    # Test - high npp, many centroids, few annots
    params = {"with_classes": True, "with_low_npp_centroids": True}
    # query - all data
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/annotations/query"
    r = requests.put(url, headers=scope_header, params=params, json=query_high)
    print(r.text)
    assert r.status_code == 200
    result = r.json()
    assert result["item_count"] == 3
    assert len(result["items"]) == 3
    assert len(result["low_npp_centroids"]) == 37035

    # viewer query
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/annotations/query/viewer"
    r = requests.put(url, headers=scope_header, json=query_high)
    assert r.status_code == 200
    result = r.json()
    assert len(result["annotations"]) == 3
    assert len(result["low_npp_centroids"]) == 37035

    # query - by ids
    query_high["annotations"] = result["annotations"]
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/annotations/query"
    r = requests.put(url, headers=scope_header, params=params, json=query_high)
    assert r.status_code == 200
    result = r.json()
    assert result["item_count"] == 3
    assert len(result["items"]) == 3
    assert len(result["low_npp_centroids"]) == 0

    # Test - low npp, few centroids, many annots
    params = {"with_classes": True, "with_low_npp_centroids": True, "limit": 10000}
    # query - all data
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/annotations/query"
    r = requests.put(url, headers=scope_header, params=params, json=query_low)
    assert r.status_code == 200
    result = r.json()
    assert result["item_count"] == 37038
    assert len(result["items"]) == 10000
    assert len(result["low_npp_centroids"]) == 0

    # viewer query
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/annotations/query/viewer"
    r = requests.put(url, headers=scope_header, json=query_low)
    assert r.status_code == 200
    result = r.json()
    assert len(result["annotations"]) == 37038
    assert len(result["low_npp_centroids"]) == 0

    # query - by ids
    query = query_low.copy()
    query["annotations"] = result["annotations"]
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/annotations/query"
    r = requests.put(url, headers=scope_header, params=params, json=query)
    assert r.status_code == 200
    result = r.json()
    assert result["item_count"] == 37038
    assert len(result["items"]) == 10000
    assert len(result["low_npp_centroids"]) == 0

    # Test - unique class values
    query = query_low.copy()
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/annotations/query/unique-class-values"
    r = requests.put(url, headers=scope_header, json=query)
    assert r.status_code == 200
    result = r.json()
    assert len(result["unique_class_values"]) == 0
    assert result["unique_class_values"] == []

    # Test - Collection Leafs

    job = utils_job.get_job(headers=login_manager.wbs_client(), job_id=job_id).json()
    collection_id = job["outputs"]["my_cells"]

    # not shallow -> too many -> error
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/collections/{collection_id}"
    r = requests.get(url, headers=scope_header)
    assert r.status_code == 413

    # not shallow and with_leaf_ids -> too many -> error
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/collections/{collection_id}"
    r = requests.get(url, headers=scope_header, params={"with_leaf_ids": True})
    assert r.status_code == 413

    # shallow=True
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/collections/{collection_id}"
    r = requests.get(url, headers=scope_header, params={"shallow": True})
    assert r.status_code == 200
    outer_col = r.json()
    assert outer_col["item_count"] > 0
    assert len(outer_col["items"]) > 0
    for inner_col in outer_col["items"]:
        assert inner_col["item_count"] > 0
        assert inner_col["items"] == []

    # shallow=True, with_leaf_ids=True
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/collections/{collection_id}"
    r = requests.get(url, headers=scope_header, params={"shallow": True, "with_leaf_ids": True})
    assert r.status_code == 200
    outer_col = r.json()
    assert outer_col["item_count"] > 0
    assert len(outer_col["items"]) > 0
    for inner_col in outer_col["items"]:
        assert inner_col["item_count"] > 0
        assert inner_col["items"] == []
        assert "item_ids" in inner_col
        assert inner_col["item_count"] == len(inner_col["item_ids"])
