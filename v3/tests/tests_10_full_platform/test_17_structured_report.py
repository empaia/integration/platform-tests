import time

import requests
from sample_apps.sample_apps.tutorial._TA14_structured_report import v3 as app

from ....global_services.data_generators.marketplace_service.utils import get_app_id_by_ead, get_apps_by_id
from ....global_services.data_generators.singletons import general_settings, login_manager
from .. import utils
from ..shared import wait_jes_job_status, wait_js_job_status

POST_TRIGGER = {
    "portal_app_id": "TBD",
    "stain": "H_AND_E",
    "tissue": "PROSTATE",
}


def test_preprocessing_incl_wbd():
    _test_preprocessing_with_fhir_questionnaire()


# test FHIR Questionnaire in Platform Deployment
def _test_preprocessing_with_fhir_questionnaire(post_trigger=POST_TRIGGER):
    # read Cases from file
    case_id, slide_ids = utils.read_case_from_file("FULL_PLATFORM_CASES_v3", "TEST15v3", "v3")
    headers = login_manager.wbs_client()
    app_id_1 = get_app_id_by_ead(headers, app.ead)
    apps = get_apps_by_id(headers)
    preprocessing_app = apps[app_id_1]
    portal_app_id = preprocessing_app["portal_app_id"]
    ex_id = utils.add_or_get_examination_to_case(case_id, app_id_1)

    # get existing Jobs
    headers = login_manager.mta_user()
    url = f"{general_settings.mds_host}v3/examinations/{ex_id}"
    r = requests.get(url, headers=headers)
    assert r.status_code == 200
    job_ids_old = r.json()["jobs"]

    # add preprocessing trigger
    _get_trigger_delete_trigger()
    _add_trigger(portal_app_id, post_trigger)

    # simulate DMC
    patho_user_id = login_manager.patho_user()["user-id"]
    _simulate_dmc(patho_user_id, case_id, slide_ids[0])

    # wait for WBD to create Job with inputs
    time.sleep(10)

    # assert that Job got added
    headers = login_manager.mta_user()
    url = f"{general_settings.mds_host}v3/examinations/{ex_id}"
    r = requests.get(url, headers=headers)
    assert r.status_code == 200
    job_ids_new = r.json()["jobs"]
    assert len(job_ids_new) == len(job_ids_old) + 1
    job_id = list(set(job_ids_new) - set(job_ids_old))[0]

    # assert Job status
    wait_jes_job_status(job_id, "TERMINATED", 180)
    wait_js_job_status(job_id, "COMPLETED", 20)

    # assert all inputs, output and Selector of newly added Job
    url = f"{general_settings.mds_host}v3/jobs/{job_id}"
    r = requests.get(url)
    assert r.status_code == 200
    new_job = r.json()
    assert "my_wsi" in new_job["inputs"].keys()
    assert "my_questionnaire" in new_job["inputs"].keys()
    assert "my_questionnaire_response" in new_job["outputs"].keys()


def _simulate_dmc(patho_user_id, case_id, slide_id):
    # create preprocessing requests
    url = f"{general_settings.mds_host}v3/preprocessing-requests"
    headers = login_manager.mta_user()
    preprocessing_request = {
        "creator_id": headers["user-id"],
        "creator_type": "USER",
        "slide_id": slide_id,
    }
    r = requests.post(url, json=preprocessing_request, headers=headers)
    print(r.text, r.status_code)
    assert r.status_code == 201


# check contents of EAD and ensure compatibility of Questionnaire in EAD with Case
def _check_ead(ead, case_id):
    # fetch Case
    r = requests.get(f"{general_settings.mds_host}v3/cases/{case_id}")
    case = r.json()
    # check if selector in EAD is compatible with indication and procedure of Case
    for input_key in ead["modes"]["preprocessing"]["inputs"]:
        if "selectors" in ead["io"][input_key]:
            result = requests.get(f"{general_settings.mds_host}v3/fhir/selector-taggings")
            selector_taggings = result.json()
            for selector_tagging in selector_taggings["items"]:
                if (
                    case["indication"] == selector_tagging["indication"]
                    and case["procedure"] == selector_tagging["procedure"]
                ):
                    return selector_tagging["selector_value"]


# fetch logical Id of FHIR Questionnaire Resource
def _fetch_logical_id(types, selector_value):
    payload = {"types": [types], "selector_values": [selector_value]}
    r = requests.put(f"{general_settings.mds_host}v3/fhir/selectors/query", json=payload)
    print(r.json())
    return r.json()["items"][0]["logical_id"]


# fetch version Id of FHIR Questionnaire Resource
def _fetch_version_id(logical_id: str):
    r = requests.get(f"{general_settings.mds_host}v3/fhir/questionnaires/{logical_id}")
    return r.json()["meta"]["versionId"]


# add input key with Id to Job
def _add_input(job_id, input_key, input_id):
    r = requests.put(
        f"{general_settings.mds_host}v3/jobs/{job_id}/inputs/{input_key}",
        json={"id": f"{input_id}"},
        headers=login_manager.wbs_client(),
    )
    assert r.status_code == 200


# add preprocessing trigger for App
def _add_trigger(portal_app_id, post_trigger):
    url = f"{general_settings.wbs_host}v3/configuration/preprocessing-triggers"
    headers = login_manager.patho_user()
    post_trigger["portal_app_id"] = portal_app_id
    r = requests.post(url, headers=headers, json=post_trigger)
    trigger = r.json()
    assert r.status_code == 200
    assert trigger["creator_id"] == headers["user-id"]
    assert trigger["creator_type"] == "USER"
    trigger.pop("creator_id")
    trigger.pop("creator_type")
    trigger.pop("id")
    trigger.pop("app")
    assert trigger == post_trigger


# delete all triggers
def _get_trigger_delete_trigger(n_items_before=None):
    # get all trigger
    url = f"{general_settings.wbs_host}v3/configuration/preprocessing-triggers"
    headers = login_manager.patho_user()
    r = requests.get(url, headers=headers)
    triggers = r.json()["items"]
    assert r.status_code == 200
    if n_items_before:
        assert len(triggers) == n_items_before

    # delete all trigger
    for t in triggers:
        headers = login_manager.patho_user()
        url = f"{general_settings.wbs_host}v3/configuration/preprocessing-triggers/{t['id']}"
        # get before deleting
        r = requests.get(url, headers=headers)
        if "No valid portal app for ID" not in r.text:
            assert r.status_code == 200
            r_t = r.json()
            assert r_t["id"] == t["id"]
            assert r_t["creator_id"] == t["creator_id"]
            assert r_t["tissue"] == t["tissue"]
            assert r_t["stain"] == t["stain"]
            assert r_t["creator_type"] == t["creator_type"]
            assert r_t["app"]["app_id"] == t["app"]["app_id"]
            assert r_t["app"]["portal_app_id"] == t["app"]["portal_app_id"]

        # delete
        r = requests.delete(url, headers=headers)
        assert r.status_code == 200
        # get after delete
        r = requests.get(url, headers=headers)
        assert r.status_code == 404

    # get all trigger
    url = f"{general_settings.wbs_host}v3/configuration/preprocessing-triggers"
    headers = login_manager.patho_user()
    r = requests.get(url, headers=headers)
    print(r.text, r.status_code)
    triggers = r.json()["items"]
    assert len(triggers) == 0
