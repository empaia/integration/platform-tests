import requests
from sample_apps.sample_apps.test.internal._ITA13_job_report import v3 as app

from platform_tests.global_services.data_generators.marketplace_service.utils import get_app_id_by_ead
from platform_tests.global_services.data_generators.singletons import general_settings, login_manager
from platform_tests.v3.data_generators.case_data import get_test_cases
from platform_tests.v3.data_generators.medical_data_service import utils, utils_job
from platform_tests.v3.data_generators.workbench_service import gen_data_ITA13_v3
from platform_tests.v3.data_generators.workbench_service import utils as wbs_utils
from platform_tests.v3.tests.shared import wait_jes_job_status, wait_js_job_status
from platform_tests.v3.tests.utils import read_case

from .. import utils as utils_test
from .test_14a_report import close_examination
from .test_14b_report import wait_js_job_validation


def test_app():
    patho_user_id = login_manager.patho_user()["user-id"]

    # read case from file
    case = read_case(get_test_cases, "FULL_PLATFORM_CASES_v3", "TEST14cv3", "v3")
    case_id = case["case_id"]
    slide_ids = case["slide_ids"]

    # add examination or get existing
    ead = app.ead
    headers = login_manager.wbs_client()
    app_id = get_app_id_by_ead(headers, ead)
    ex_id, status_code = utils.add_or_get_examination_to_case(patho_user_id, case_id, app_id, return_status=True)
    if status_code == 200:
        r = close_examination(ex_id)
        assert r.status_code == 200
        ex = r.json()
        assert ex["state"] == "CLOSED"
        ex_id = utils.add_or_get_examination_to_case(patho_user_id, case_id, app_id)

    scope_id, scope_header = utils_test.create_scope(ex_id)

    # CONTAINERIZED JOB(s): only create inputs
    containerized_jobs = []
    for _ in range(4):
        inputs_by_key, inputs_type_id = gen_data_ITA13_v3._gen_inputs_standalone(ead, slide_ids[0])
        job_mode = "STANDALONE"
        _in_job, job_id, _token = utils.create_job(ead, app_id, patho_user_id, ex_id)
        wbs_utils.add_inputs_to_job(job_id, inputs_by_key, scope_id, scope_header)
        wbs_utils.lock_to_job(job_id, inputs_type_id, case_id)
        containerized_jobs.append(job_id)

    # REPORT: new report job
    payload = {
        "creator_id": scope_id,
        "creator_type": "SCOPE",
        "mode": "REPORT",
        "containerized": False,
    }
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/jobs"
    r = requests.post(url, headers=scope_header, json=payload)
    print(r.text)
    assert r.status_code == 201
    report_job = r.json()
    report_job_id = report_job["id"]
    # REPORT: create output
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/primitives"
    my_string = {
        "name": "my_string",
        "value": "REPORT",
        "type": "string",
        "creator_type": "scope",  # NEW required in v3 apps
        "creator_id": scope_id,  # NEW required in v3 apps
    }
    r = requests.post(url, headers=scope_header, json=my_string)
    assert r.status_code == 201
    my_string_id = r.json()["id"]
    # REPORT: add output
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/jobs/{report_job_id}/outputs/my_string"
    r = requests.put(url, headers=scope_header, json={"id": my_string_id})
    print(r.text)
    assert r.status_code == 200

    # CONTAINERIZED JOB(s): start [0], wait for complete
    utils.set_js_job_statuses(containerized_jobs[0], ["READY"])
    wait_jes_job_status(job_id=containerized_jobs[0], final_status="TERMINATED", timeout=90)
    wait_js_job_status(job_id=containerized_jobs[0], final_status="COMPLETED", timeout=90)

    # CONTAINERIZED JOB(s): start [1], wait for RUNNING
    utils.set_js_job_statuses(containerized_jobs[1], ["READY"])
    wait_jes_job_status(job_id=containerized_jobs[1], final_status="RUNNING", timeout=90)

    # Close Examination EXPECT 200
    # CONTAINERIZED JOB(s): start [2], dont wait, immediatly close
    utils.set_js_job_statuses(containerized_jobs[2], ["READY"])
    r = close_examination(ex_id)
    ex = r.json()
    assert ex["state"] == "CLOSED"
    headers = login_manager.wbs_client()
    r = utils_job.get_job(headers, report_job_id)
    assert r.status_code == 200
    report_job = r.json()
    assert report_job["status"] == "COMPLETED"

    # CONTAINERIZED JOB(s):
    # start [3]
    utils.set_js_job_statuses(containerized_jobs[3], ["READY"], expected_status_code=400)
    # JES job doesnt exist here
    wait_js_job_status(job_id=containerized_jobs[3], final_status="ERROR", timeout=90)
    wait_js_job_validation(
        job_id=containerized_jobs[3], output_val_status="ERROR", input_val_status="COMPLETED", timeout=90
    )

    # check [0] (already COMPLETED before close)
    wait_jes_job_status(job_id=containerized_jobs[0], final_status="TERMINATED", timeout=90)
    wait_js_job_status(job_id=containerized_jobs[0], final_status="COMPLETED", timeout=90)
    wait_js_job_validation(
        job_id=containerized_jobs[0], output_val_status="COMPLETED", input_val_status="COMPLETED", timeout=90
    )
    gen_data_ITA13_v3.check_containerized_app_output(containerized_jobs[0], job_mode)

    # check [1] (was RUNNING before close)
    wait_jes_job_status(job_id=containerized_jobs[1], final_status="STOPPED")
    wait_js_job_status(job_id=containerized_jobs[1], final_status="ERROR")
    wait_js_job_validation(
        job_id=containerized_jobs[1], output_val_status="ERROR", input_val_status="COMPLETED", timeout=90
    )

    # check [2] (was READY before close, JES might not know about)
    try:
        wait_jes_job_status(job_id=containerized_jobs[1], final_status="STOPPED", timeout=90)
    except AssertionError as ex:
        assert "Job not found at JES" in ex.__str__()
    wait_js_job_status(job_id=containerized_jobs[1], final_status="ERROR", timeout=90)
    wait_js_job_validation(
        job_id=containerized_jobs[1], output_val_status="ERROR", input_val_status="COMPLETED", timeout=90
    )
