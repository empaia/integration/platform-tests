import time

import pytest
import requests
from sample_apps.sample_apps.tutorial._TA11_preprocessing import v3 as app

from ....global_services.data_generators.marketplace_service.utils import get_app_id_by_ead, get_apps_by_id
from ....global_services.data_generators.singletons import general_settings, login_manager
from .. import utils
from ..shared import wait_jes_job_status, wait_js_job_status

POST_TRIGGER_1 = {
    "portal_app_id": "TBD",
    "stain": "CALRETININ",
    "tissue": "FALLOPIAN_TUBES",
}
POST_TRIGGER_2 = {
    "portal_app_id": "TBD",
    "stain": "BETA_CATENIN",
    "tissue": "NASAL_CAVITY_AND_NASOPHARYNX",
}


def test_preprocessing_incl_wbd():
    _test_preprocessing(post_trigger=POST_TRIGGER_1, slide_idx=0)


@pytest.mark.skip(reason="Only use for debugging")
def test_preprocessing_simulate_wbd():
    _test_preprocessing(post_trigger=POST_TRIGGER_2, slide_idx=1)


def _test_preprocessing(post_trigger, slide_idx):
    case_id, slide_ids = utils.read_case_from_file("FULL_PLATFORM_CASES_v3", "TEST13v3", "v3")
    headers = login_manager.wbs_client()
    app_id_1 = get_app_id_by_ead(headers, app.ead)
    apps = get_apps_by_id(headers)
    preprocessing_app = apps[app_id_1]
    portal_app_id = preprocessing_app["portal_app_id"]
    ex_id_1 = utils.add_or_get_examination_to_case(case_id, app_id_1)

    # get existing jobs
    headers = login_manager.mta_user()
    url = f"{general_settings.mds_host}v3/examinations/{ex_id_1}"
    r = requests.get(url, headers=headers)
    assert r.status_code == 200
    job_ids_old = r.json()["jobs"]

    get_trigger_delete_trigger()
    add_trigger(portal_app_id, post_trigger)
    get_trigger_delete_trigger(n_items_before=1)
    add_trigger(portal_app_id, post_trigger)

    patho_user_id = login_manager.patho_user()["user-id"]
    # simulate dmc
    simulate_dmc(patho_user_id, case_id, slide_ids[slide_idx])

    # wait for WBD to create Job with inputs
    time.sleep(10)

    # get jobs, assert job got added
    # get existing jobs
    headers = login_manager.mta_user()
    url = f"{general_settings.mds_host}v3/examinations/{ex_id_1}"
    r = requests.get(url, headers=headers)
    assert r.status_code == 200
    job_list_new = r.json()["jobs"]
    assert len(job_list_new) == len(job_ids_old) + 1
    job_id_new = set(job_list_new) - set(job_ids_old)
    job_id_new = list(job_id_new)[0]
    pre_pro_job_id = job_id_new
    # get job, assert job running
    wait_jes_job_status(pre_pro_job_id, "TERMINATED", 180)
    wait_js_job_status(pre_pro_job_id, "COMPLETED", 20)

    # use scopes API to do some post-processing
    scope_id, scope_header = utils.create_scope(ex_id_1)
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/jobs"
    r = requests.get(url, headers=scope_header)
    assert r.status_code == 200
    job_list = r.json()
    job_ids = [j["id"] for j in job_list["items"]]
    assert pre_pro_job_id in job_ids

    # new post-processing job
    payload = {
        "creator_id": scope_id,
        "creator_type": "SCOPE",
        "containerized": False,
        "mode": "POSTPROCESSING",
    }
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/jobs"
    r = requests.post(url, headers=scope_header, json=payload)
    assert r.status_code == 201
    post_pro_job = r.json()
    post_pro_job_id = post_pro_job["id"]

    # we get some outputs of last job
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/jobs/{pre_pro_job_id}"
    r = requests.get(url, headers=scope_header)
    assert r.status_code == 200
    pre_pro_job = r.json()
    my_wsi_id = pre_pro_job["inputs"]["my_wsi"]
    my_cells_id = pre_pro_job["outputs"]["my_cells"]
    my_cell_classes_id = pre_pro_job["outputs"]["my_cell_classes"]

    # add outputs (pre-) of last job (and wsi) as inputs for new (post-) job as defined by EAD
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/jobs/{post_pro_job_id}/inputs/my_wsi"
    r = requests.put(url, headers=scope_header, json={"id": my_wsi_id})
    print(r.text)
    assert r.status_code == 200
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/jobs/{post_pro_job_id}/inputs/my_cells"
    r = requests.put(url, headers=scope_header, json={"id": my_cells_id})
    assert r.status_code == 200
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/jobs/{post_pro_job_id}/inputs/my_cell_classes"
    r = requests.put(url, headers=scope_header, json={"id": my_cell_classes_id})
    assert r.status_code == 200

    # new input for post pro job
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/annotations"
    payload = {
        "name": "my_rectangle",
        "type": "rectangle",
        "upper_left": [1000, 2000],
        "width": 512,
        "height": 512,
        "reference_id": my_wsi_id,
        "reference_type": "wsi",
        "creator_id": scope_id,
        "creator_type": "scope",
        "npp_created": 250,
        "npp_viewing": [250, 250 * 2**3],
    }
    params = {"is_roi": True}
    r = requests.post(url, headers=scope_header, params=params, json=payload)
    print(r.text)
    assert r.status_code == 201
    my_rectangle = r.json()
    my_rectangle_id = my_rectangle["id"]
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/jobs/{post_pro_job_id}/inputs/my_rectangle"
    r = requests.put(url, headers=scope_header, json={"id": my_rectangle_id})
    assert r.status_code == 200

    # add postprocessing output
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/primitives"
    payload = {
        "name": "tumor_ratio",
        "type": "float",
        "value": 69.42,
        "reference_id": my_rectangle_id,
        "reference_type": "annotation",
        "creator_id": scope_id,
        "creator_type": "scope",
    }
    r = requests.post(url, headers=scope_header, json=payload)
    print(r.text)
    assert r.status_code == 201
    tumor_ratio = r.json()
    tumor_ratio_id = tumor_ratio["id"]
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/jobs/{post_pro_job_id}/outputs/tumor_ratio"
    r = requests.put(url, headers=scope_header, json={"id": tumor_ratio_id})
    print(r.text)
    assert r.status_code == 200

    # finalize postprocessing job
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/jobs/{post_pro_job_id}/finalize"
    r = requests.put(url, headers=scope_header)
    print(r.text)
    print(my_rectangle)
    assert r.status_code == 200
    _finalized_job = r.json()

    # delete trigger
    get_trigger_delete_trigger(n_items_before=1)


def simulate_dmc(patho_user_id, case_id, slide_id):
    # create preprocessing requests
    url = f"{general_settings.mds_host}v3/preprocessing-requests"
    headers = login_manager.mta_user()
    preprocessing_request = {
        "creator_id": headers["user-id"],
        "creator_type": "USER",
        "slide_id": slide_id,
    }
    r = requests.post(url, json=preprocessing_request, headers=headers)
    print(r.text, r.status_code)
    assert r.status_code == 201


def add_trigger(portal_app_id, post_trigger):
    url = f"{general_settings.wbs_host}v3/configuration/preprocessing-triggers"
    headers = login_manager.patho_user()
    post_trigger["portal_app_id"] = portal_app_id
    r = requests.post(url, headers=headers, json=post_trigger)
    trigger = r.json()
    assert r.status_code == 200
    assert trigger["creator_id"] == headers["user-id"]
    assert trigger["creator_type"] == "USER"
    trigger.pop("creator_id")
    trigger.pop("creator_type")
    trigger.pop("id")
    trigger.pop("app")
    assert trigger == post_trigger


def get_trigger_delete_trigger(n_items_before=None):
    # Get all trigger
    url = f"{general_settings.wbs_host}v3/configuration/preprocessing-triggers"
    headers = login_manager.patho_user()
    r = requests.get(url, headers=headers)
    triggers = r.json()["items"]
    assert r.status_code == 200
    if n_items_before:
        assert len(triggers) == n_items_before

    # Delete all trigger
    for t in triggers:
        headers = login_manager.patho_user()
        url = f"{general_settings.wbs_host}v3/configuration/preprocessing-triggers/{t['id']}"
        # get b4 delete
        r = requests.get(url, headers=headers)
        if "No valid portal app for ID" not in r.text:
            assert r.status_code == 200
            r_t = r.json()
            assert r_t["id"] == t["id"]
            assert r_t["creator_id"] == t["creator_id"]
            assert r_t["tissue"] == t["tissue"]
            assert r_t["stain"] == t["stain"]
            assert r_t["creator_type"] == t["creator_type"]
            assert r_t["app"]["app_id"] == t["app"]["app_id"]
            assert r_t["app"]["portal_app_id"] == t["app"]["portal_app_id"]

        # delete
        r = requests.delete(url, headers=headers)
        assert r.status_code == 200
        # get after delete
        r = requests.get(url, headers=headers)
        assert r.status_code == 404

    # Get all trigger
    url = f"{general_settings.wbs_host}v3/configuration/preprocessing-triggers"
    headers = login_manager.patho_user()
    r = requests.get(url, headers=headers)
    print(r.text, r.status_code)
    triggers = r.json()["items"]
    assert len(triggers) == 0
