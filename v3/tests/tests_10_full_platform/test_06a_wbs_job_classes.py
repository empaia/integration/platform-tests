import requests
from sample_apps.sample_apps.test.internal._ITA05_input_templates import v3 as ita05v3
from sample_apps.sample_apps.tutorial._TA01_simple_app import v3 as ta01v3
from sample_apps.sample_apps.tutorial._TA05_classes import v3 as ta05v3

from ....global_services.data_generators.marketplace_service.utils import get_app_id_by_ead
from ....global_services.data_generators.singletons import general_settings, login_manager
from ...data_generators.workbench_service import utils as wbs_utils
from .. import utils
from ..shared import wait_js_job_status


def test_wbs_class_locking():
    case_id, slide_ids = utils.read_case_from_file("FULL_PLATFORM_CASES_v3", "TEST06v3", "v3")

    app_id_1 = get_app_id_by_ead(login_manager.wbs_client(), ta01v3.ead)
    app_id_2 = get_app_id_by_ead(login_manager.wbs_client(), ta05v3.ead)
    app_id_3 = get_app_id_by_ead(login_manager.wbs_client(), ita05v3.ead)

    ex_id_1 = utils.add_or_get_examination_to_case(case_id, app_id_1)
    ex_id_2 = utils.add_or_get_examination_to_case(case_id, app_id_2)
    ex_id_3 = utils.add_or_get_examination_to_case(case_id, app_id_3)

    scope_id_1, scope_header_1 = utils.create_scope(ex_id_1)
    scope_id_2, scope_header_2 = utils.create_scope(ex_id_2)
    scope_id_3, scope_header_3 = utils.create_scope(ex_id_3)

    # App 1 - no classes
    job_data_1 = utils.wbs_post_job_data(scope_id_1, "SCOPE")
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/jobs"
    r = requests.post(url, json=job_data_1, headers=scope_header_1)
    assert r.status_code == 201
    job_1 = r.json()
    job_id_1 = job_1["id"]

    slide = slide_ids[0]
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/jobs/{job_id_1}/inputs/my_wsi"
    r = requests.put(url, json=dict(id=slide), headers=scope_header_1)
    assert r.status_code == 200

    rectangle = create_rectangle_annotation(slide, scope_id_1, scope_header_1)
    input_id = rectangle["id"]
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/jobs/{job_id_1}/inputs/my_rectangle"
    r = requests.put(url, json=dict(id=input_id), headers=scope_header_1)
    assert r.status_code == 200

    create_class(input_id, "dummy", scope_id_1, scope_header_1)

    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/jobs/{job_id_1}/run"
    r = requests.put(url, headers=scope_header_1)
    assert r.status_code == 200
    job = r.json()
    assert job["status"] == "READY"

    query = {"jobs": [job_id_1]}
    url = f"{general_settings.mds_host}v3/classes/query"
    headers = login_manager.wbs_client()
    r = requests.put(url, json=query, headers=headers)
    assert r.status_code == 200
    data = r.json()
    assert data["item_count"] == 0

    wait_js_job_status(job_id=job_id_1, final_status="COMPLETED")

    # App 2 - class contraint
    job_data_2 = utils.wbs_post_job_data(scope_id_2, "SCOPE")
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/jobs"
    r = requests.post(url, json=job_data_2, headers=scope_header_2)
    assert r.status_code == 201
    job_2 = r.json()
    job_id_2 = job_2["id"]

    slide = slide_ids[0]
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/jobs/{job_id_2}/inputs/my_wsi"
    r = requests.put(url, json=dict(id=slide), headers=scope_header_2)
    assert r.status_code == 200

    collection = create_rectangle_collection(slide, scope_id_2, scope_header_2)
    input_id = collection["id"]
    rect_id = collection["items"][0]["id"]
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/jobs/{job_id_2}/inputs/my_rectangles"
    r = requests.put(url, json=dict(id=input_id), headers=scope_header_2)
    assert r.status_code == 200

    create_class(rect_id, "org.empaia.global.v1.classes.roi", scope_id_2, scope_header_2)
    create_class(rect_id, "dummy", scope_id_2, scope_header_2)

    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/jobs/{job_id_2}/run"
    r = requests.put(url, headers=scope_header_2)
    assert r.status_code == 200
    job = r.json()
    assert job["status"] == "READY"

    query = {"jobs": [job_id_2]}
    url = f"{general_settings.mds_host}v3/classes/query"
    headers = login_manager.wbs_client()
    r = requests.put(url, json=query, headers=headers)
    assert r.status_code == 200
    data = r.json()
    assert data["item_count"] == 1
    assert data["items"][0]["value"] == "org.empaia.global.v1.classes.roi"

    wait_js_job_status(job_id=job_id_2, final_status="COMPLETED")

    # App 3 - EAD classes
    job_data_3 = utils.wbs_post_job_data(scope_id_3, "SCOPE")
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_3}/jobs"
    r = requests.post(url, json=job_data_3, headers=scope_header_3)
    assert r.status_code == 201
    job_3 = r.json()
    job_id_3 = job_3["id"]

    slide = slide_ids[0]
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_3}/jobs/{job_id_3}/inputs/single_wsi"
    r = requests.put(url, json=dict(id=slide), headers=scope_header_3)
    assert r.status_code == 200

    rectangle = create_point_annotation(slide, scope_id_3, scope_header_3)
    input_id = rectangle["id"]
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_3}/jobs/{job_id_3}/inputs/single_point"
    r = requests.put(url, json=dict(id=input_id), headers=scope_header_3)
    assert r.status_code == 200

    cls = create_class(
        input_id, "org.empaia.vendor_name.internal_test_app_05.v3.0.classes.tumor", scope_id_3, scope_header_3
    )
    class_id = cls["id"]
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_3}/jobs/{job_id_3}/inputs/single_class"
    r = requests.put(url, json=dict(id=class_id), headers=scope_header_3)
    assert r.status_code == 200

    classes = create_class_collection(
        input_id, "org.empaia.vendor_name.internal_test_app_05.v3.0.classes.tumor", scope_id_3, scope_header_3
    )
    collection_id = classes["id"]
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_3}/jobs/{job_id_3}/inputs/collection_class"
    r = requests.put(url, json=dict(id=collection_id), headers=scope_header_3)
    assert r.status_code == 200

    create_class(input_id, "dummy", scope_id_3, scope_header_3)

    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_3}/jobs/{job_id_3}/run"
    r = requests.put(url, headers=scope_header_3)
    assert r.status_code == 200
    job = r.json()
    assert job["status"] == "READY"

    query = {"jobs": [job_id_3]}
    url = f"{general_settings.mds_host}v3/classes/query"
    headers = login_manager.wbs_client()
    r = requests.put(url, json=query, headers=headers)
    assert r.status_code == 200
    data = r.json()
    assert data["item_count"] == 2
    for cls in data["items"]:
        assert cls["value"] == "org.empaia.vendor_name.internal_test_app_05.v3.0.classes.tumor"

    wait_js_job_status(job_id=job_id_3, final_status="ERROR")


def test_wbs_class_value_validation():
    case_id, slide_ids = utils.read_case_from_file("FULL_PLATFORM_CASES_v3", "TEST06v3", "v3")

    app_id_1 = get_app_id_by_ead(login_manager.wbs_client(), ta01v3.ead)
    app_id_2 = get_app_id_by_ead(login_manager.wbs_client(), ta05v3.ead)

    ex_id_1 = utils.add_or_get_examination_to_case(case_id, app_id_1)
    ex_id_2 = utils.add_or_get_examination_to_case(case_id, app_id_2)

    scope_id_1, scope_header_1 = utils.create_scope(ex_id_1)
    scope_id_2, scope_header_2 = utils.create_scope(ex_id_2)

    # App 1 - no classes in EAD
    rect = create_rectangle_annotation(slide_ids[0], scope_id_1, scope_header_1)

    # valid class, must be accepted
    status_code = create_class_wbs_api(rect["id"], "org.empaia.global.v1.classes.roi", scope_id_1, scope_header_1)
    assert status_code == 201

    # invalid class, must be rejected
    status_code = create_class_wbs_api(rect["id"], "dummy", scope_id_1, scope_header_1)
    assert status_code == 400

    # App 2 - classes in EAD
    rects = create_rectangle_collection(slide_ids[0], scope_id_2, scope_header_2)

    # valid class, must be accepted
    status_code = create_class_wbs_api(
        rects["items"][0]["id"], "org.empaia.global.v1.classes.roi", scope_id_2, scope_header_2
    )
    assert status_code == 201

    # valid class, must be accepted
    status_code = create_class_wbs_api(
        rects["items"][0]["id"], "org.empaia.vendor_name.tutorial_app_05.v3.0.classes.tumor", scope_id_2, scope_header_2
    )
    assert status_code == 201

    # invalid class, must be rejected
    status_code = create_class_wbs_api(
        rects["items"][0]["id"], "org.empaia.vendor_name.tutorial_app_05.v3.0.classes", scope_id_2, scope_header_2
    )
    assert status_code == 400

    # invalid class, must be rejected
    status_code = create_class_wbs_api(rects["items"][0]["id"], "dummy", scope_id_2, scope_header_2)
    assert status_code == 400


def create_point_annotation(slide_id, scope_id, scope_headers):
    annotation_data = wbs_utils.generate_point_json(slide_id, scope_id, creator_type="scope")
    r = wbs_utils.wbs_post_annotation(scope_headers, scope_id, annotation_data)
    return r.json()


def create_rectangle_annotation(slide_id, scope_id, scope_headers):
    annotation_data = wbs_utils.generate_rectangle_json(slide_id, scope_id, creator_type="scope")
    r = wbs_utils.wbs_post_annotation(scope_headers, scope_id, annotation_data)
    return r.json()


def create_rectangle_collection(slide_id, scope_id, scope_headers):
    annotation_data = wbs_utils.generate_rectangle_json(slide_id, scope_id, creator_type="scope")
    data = {
        "name": "rects",
        "creator_id": scope_id,
        "creator_type": "scope",
        "item_type": "rectangle",
        "type": "collection",
        "items": [annotation_data],
    }
    response = wbs_utils.wbs_post_collection(scope_headers, scope_id, data)
    return response.json()


def create_class(ref_id, class_value, scope_id, scope_headers):
    data = {
        "value": class_value,
        "type": "class",
        "creator_id": scope_id,
        "creator_type": "scope",
        "reference_type": "annotation",
        "reference_id": ref_id,
    }
    response = wbs_utils.wbs_post_class(scope_headers, scope_id, data)
    return response.json()


def create_class_wbs_api(ref_id, class_value, scope_id, scope_header):
    data = {
        "value": class_value,
        "type": "class",
        "creator_id": scope_id,
        "creator_type": "scope",
        "reference_type": "annotation",
        "reference_id": ref_id,
    }
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/classes"
    r = requests.post(url, json=data, headers=scope_header)
    return r.status_code


def create_class_collection(ref_id, class_value, scope_id, scope_headers):
    class_data = {
        "value": class_value,
        "type": "class",
        "creator_id": scope_id,
        "creator_type": "scope",
        "reference_type": "annotation",
        "reference_id": ref_id,
    }
    collection_data = {
        "name": "classes",
        "creator_id": scope_id,
        "creator_type": "scope",
        "item_type": "class",
        "type": "collection",
        "items": [class_data],
    }
    response = wbs_utils.wbs_post_collection(scope_headers, scope_id, collection_data)
    return response.json()
