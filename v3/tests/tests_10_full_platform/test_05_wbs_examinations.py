import requests
from sample_apps.sample_apps.tutorial._TA01_simple_app import v3 as app

from ....global_services.data_generators.marketplace_service.utils import get_app_id_by_ead
from ....global_services.data_generators.singletons import general_settings, login_manager
from ...data_generators.case_data import get_test_cases
from ...data_generators.medical_data_service import utils, utils_examination
from ...data_generators.workbench_service import gen_data_TA01_v3
from ..shared import wait_js_job_status
from ..utils import add_or_get_examination_to_case, create_scope, get_case_counts, read_case


def test_wbs2_examinations():
    patho_user_id = login_manager.patho_user()["user-id"]

    # read case from file
    case = read_case(get_test_cases, "FULL_PLATFORM_CASES_v3", "TEST05v3", "v3")
    case_id = case["case_id"]
    slide_ids = case["slide_ids"]
    slide_id = slide_ids[0]

    # get counts for case
    headers = login_manager.patho_user()
    _, examination_count_old = get_case_counts(headers, case_id, "v3")

    ead = app.ead
    app_id = get_app_id_by_ead(login_manager.wbs_client(), app.ead)

    # empty examination
    headers = login_manager.wbs_client()
    ex_id = add_or_get_examination_to_case(case_id, app_id)
    utils_examination.mds_close_examination(headers, ex_id)
    # add examination with 2 jobs (1 incomplete)
    ex_id = add_or_get_examination_to_case(case_id, app_id)
    scope_id, scope_header = create_scope(ex_id)

    # incomplete job
    _, _ = gen_data_TA01_v3._gen_inputs(ead, scope_id, scope_header, slide_id)
    _in_job, job_id_invalid, _token = utils.create_job(ead, app_id, patho_user_id, ex_id)
    # complete job
    _job_in, job_id = gen_data_TA01_v3.generate(scope_id, scope_header, slide_id, ex_id, case_id, set_job_ready=True)
    wait_js_job_status(job_id=job_id, final_status="COMPLETED")

    # Test Job Deletion
    params = {"skip": 0, "limit": 10000}
    _, job_id, _token = utils.create_job(ead, app_id, patho_user_id, ex_id)

    url = f"{general_settings.wbs_host}v3/examinations/{ex_id}"
    headers = login_manager.patho_user()
    r = requests.get(url, headers=headers, params=params)
    print("WBS - GET Ex status_code: ", r.status_code)
    assert r.status_code == 200
    result_json = r.json()
    jobs_count_before_delete = result_json["jobs_count"]

    utils.delete_job(job_id=job_id)

    headers = login_manager.patho_user()
    r = requests.get(url, headers=headers, params=params)
    print("WBS - GET Ex status_code: ", r.status_code)
    assert r.status_code == 200
    result_json = r.json()
    jobs_count_after_delete = result_json["jobs_count"]

    assert jobs_count_after_delete == jobs_count_before_delete - 1

    # WBS
    # PUT examination - get existing
    url = f"{general_settings.wbs_host}v3/examinations"
    payload = {"case_id": case_id, "app_id": app_id}
    headers = login_manager.patho_user()
    r = requests.put(url, headers=headers, json=payload)
    print("WBS - PUT examinations, status_code: ", r.status_code)
    print("text", r.text)
    assert r.status_code == 200

    # PUT examination states to CLOSED
    url = f"{general_settings.wbs_host}v3/examinations/{ex_id}/close"
    headers = login_manager.patho_user()
    r = requests.put(url, headers=headers)
    print(r.content)
    assert r.status_code == 200

    # GET job state for invalid job
    url = f"{general_settings.mds_host}v3/jobs/{job_id_invalid}"
    headers = login_manager.wbs_client()
    r = requests.get(url, headers=headers)
    assert r.status_code == 200
    assert r.json()["status"] == "ERROR"

    # PUT examination states to CLOSED
    url = f"{general_settings.wbs_host}v3/examinations/{ex_id}/close"
    headers = login_manager.patho_user()
    r = requests.put(url, headers=headers)
    print(r.content)
    assert r.status_code == 200
    assert r.json()["state"] == "CLOSED"

    # PUT examination - new
    url = f"{general_settings.wbs_host}v3/examinations"
    payload = {"case_id": case_id, "app_id": app_id}
    headers = login_manager.patho_user()
    r = requests.put(url, headers=headers, json=payload)
    print("WBS - PUT examinations, status_code: ", r.status_code)
    print("text", r.text)
    assert r.status_code == 201
    posted_ex = r.json()
    assert posted_ex["creator_id"] == patho_user_id
    assert posted_ex["creator_type"] == "USER"
    assert posted_ex["jobs_count"] == 0
    assert posted_ex["jobs_count_finished"] == 0

    # GET examinations
    url = f"{general_settings.wbs_host}v3/examinations/query"
    headers = login_manager.patho_user()
    payload = {"cases": [case_id]}
    r = requests.put(url, headers=headers, json=payload)
    print("WBS - GET examinations, status_code: ", r.status_code)
    assert r.status_code == 200
    result_json = r.json()
    assert result_json["item_count"] > examination_count_old
    for e in result_json["items"]:
        assert e["creator_id"] == patho_user_id
        assert e["creator_type"] == "USER"
        if e["id"] == ex_id:
            assert e["jobs_count"] == 2
            assert e["jobs_count_finished"] == 2
            assert e["state"] == "CLOSED"
        elif e["id"] == posted_ex["id"]:
            assert e["jobs_count"] == 0
            assert e["jobs_count_finished"] == 0
            assert e["state"] == "OPEN"

    # PUT examination states to CLOSED
    url = f"{general_settings.wbs_host}v3/examinations/{posted_ex['id']}/close"
    headers = login_manager.patho_user()
    r = requests.put(url, headers=headers)
    print(r.content)
    assert r.status_code == 200
    assert r.json()["state"] == "CLOSED"
