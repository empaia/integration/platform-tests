from sample_apps.sample_apps.tutorial._TA13_pixelmap import v3 as app

from platform_tests.global_services.data_generators.marketplace_service.utils import get_app_id_by_ead
from platform_tests.global_services.data_generators.singletons import login_manager
from platform_tests.v3.data_generators.case_data import get_test_cases
from platform_tests.v3.data_generators.workbench_service import gen_data_TA13_v3
from platform_tests.v3.tests.shared import (
    wait_jes_job_status,
    wait_js_job_input_validation_status,
    wait_js_job_output_validation_status,
    wait_js_job_status,
)
from platform_tests.v3.tests.utils import create_scope, read_case

from ...data_generators.medical_data_service import utils as mds_utils
from ...data_generators.workbench_service import utils as wbs_utils


def test_app():
    patho_user_id = login_manager.patho_user()["user-id"]

    # read case from file
    case = read_case(get_test_cases, "JES_AS_MDS_MPS_CASES_v3", "TEST13v3", "v3")
    case_id = case["case_id"]
    slide_ids = case["slide_ids"]

    # add examination
    ead = app.ead
    headers = login_manager.wbs_client()
    app_id = get_app_id_by_ead(headers, ead)
    ex_id = mds_utils.add_or_get_examination_to_case(patho_user_id, case_id, app_id)
    scope_id, scope_header = create_scope(ex_id)

    # only create inputs
    inputs_by_key, inputs_type_id = gen_data_TA13_v3._gen_inputs_preprocessing(ead, slide_ids[0])
    job_mode = "PREPROCESSING"
    creator_type = "SERVICE"
    _, job_id, _ = mds_utils.create_job(ead, app_id, patho_user_id, ex_id, creator_type=creator_type, mode=job_mode)
    wbs_utils.add_inputs_to_job(job_id, inputs_by_key, scope_id, scope_header)
    wbs_utils.lock_to_job(job_id, inputs_type_id, case_id)
    mds_utils.set_js_job_statuses(job_id, ["READY"])

    final_jes_status = "TERMINATED"
    final_job_status = "COMPLETED"

    wait_jes_job_status(job_id=job_id, final_status=final_jes_status, timeout=600)
    wait_js_job_status(job_id=job_id, final_status=final_job_status, timeout=600)

    wait_js_job_input_validation_status(job_id=job_id, final_status="COMPLETED")
    wait_js_job_output_validation_status(job_id=job_id, final_status="COMPLETED", timeout=600)
