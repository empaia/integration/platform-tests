from sample_apps.sample_apps.tutorial._TA01_simple_app import v3 as app

from ....global_services.data_generators.marketplace_service.utils import get_app_id_by_ead
from ....global_services.data_generators.singletons import login_manager
from ...data_generators.case_data import get_test_cases
from ...data_generators.medical_data_service import utils as mds_utils
from ...data_generators.workbench_service import gen_data_TA01_v3
from ...data_generators.workbench_service import utils as wbs_utils
from ..shared import (
    wait_jes_job_status,
    wait_js_job_input_validation_status,
    wait_js_job_output_validation_status,
    wait_js_job_status,
)
from ..utils import create_scope, read_case


def test_app():
    patho_user_id = login_manager.patho_user()["user-id"]

    # read case from file
    case = read_case(get_test_cases, "JES_AS_MDS_MPS_CASES_v3", "TEST01v3", "v3")
    case_id = case["case_id"]
    slide_ids = case["slide_ids"]

    # add examination
    ead = app.ead
    headers = login_manager.wbs_client()
    app_id = get_app_id_by_ead(headers, ead)
    ex_id = mds_utils.add_or_get_examination_to_case(patho_user_id, case_id, app_id)
    scope_id, scope_header = create_scope(ex_id)

    # only create inputs
    inputs_by_key, _ = gen_data_TA01_v3._gen_inputs(ead, scope_id, scope_header, slide_ids[0])
    _, job_id, _ = mds_utils.create_job(ead, app_id, patho_user_id, ex_id)
    wbs_utils.add_inputs_to_job(job_id, inputs_by_key, scope_id, scope_header)
    wbs_utils.run_job(job_id, scope_id, scope_header)

    final_jes_status = "TERMINATED"
    final_job_status = "COMPLETED"

    wait_jes_job_status(job_id=job_id, final_status=final_jes_status)
    wait_js_job_status(job_id=job_id, final_status=final_job_status)
    gen_data_TA01_v3.check_containerized_app_output(job_id)

    wait_js_job_input_validation_status(job_id=job_id, final_status="COMPLETED")
    wait_js_job_output_validation_status(job_id=job_id, final_status="COMPLETED")
