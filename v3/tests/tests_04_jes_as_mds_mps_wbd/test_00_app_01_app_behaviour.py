from uuid import uuid4

import requests
from sample_apps.sample_apps.test.jes._JTA01_app_behaviour import v3 as app

from ....global_services.data_generators.marketplace_service.utils import get_app_id_by_ead
from ....global_services.data_generators.singletons import general_settings, login_manager
from ..shared import wait_jes_job_status


def test_app_completed():
    ead = app.ead
    running_time = "1"
    headers = login_manager.wbs_client()
    app_id = get_app_id_by_ead(headers, ead)
    job_id = str(uuid4())
    timeout = 30
    data = {
        "app_id": app_id,
        "job_id": job_id,
        "access_token": "string",
        "app_service_url": running_time,  # encoded app behaviour
        "timeout": timeout,
    }

    url = f"{general_settings.jes_host}v1/executions"
    r = requests.post(url, json=data, headers=headers, timeout=3)
    print(r.content)
    assert r.status_code == 200

    final_status = "TERMINATED"
    wait_jes_job_status(job_id=job_id, final_status=final_status)


def test_app_failed():
    ead = app.ead
    running_time = "1"
    headers = login_manager.wbs_client()
    app_id = get_app_id_by_ead(headers, ead)
    job_id = str(uuid4())
    timeout = 30
    data = {
        "app_id": app_id,
        "job_id": job_id,
        "access_token": "string",
        "app_service_url": f"{running_time} then fail",  # encoded app behaviour
        "timeout": timeout,
    }

    url = f"{general_settings.jes_host}v1/executions"
    r = requests.post(url, json=data, headers=headers, timeout=3)
    print(r.content)
    assert r.status_code == 200

    final_status = "TERMINATED"
    wait_jes_job_status(job_id=job_id, final_status=final_status)
