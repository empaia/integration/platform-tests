import requests
from sample_apps.sample_apps.tutorial._TA03_annotation_results import v3 as app

from ....global_services.data_generators.marketplace_service.utils import get_app_id_by_ead
from ....global_services.data_generators.singletons import general_settings, login_manager
from ...data_generators.case_data import get_test_cases
from ...data_generators.medical_data_service import utils as mds_utils
from ...data_generators.workbench_service import gen_data_TA03_v3
from ...data_generators.workbench_service import utils as wbs_utils
from ..shared import wait_jes_job_status, wait_js_job_status
from ..utils import create_scope, read_case


def test_app():
    patho_user_id = login_manager.patho_user()["user-id"]

    # read case from file
    case = read_case(get_test_cases, "JES_AS_MDS_MPS_CASES_v3", "TEST99v3", "v3")
    case_id = case["case_id"]
    slide_ids = case["slide_ids"]

    # add examination
    ead = app.ead
    headers = login_manager.wbs_client()
    app_id = get_app_id_by_ead(headers, ead)
    ex_id = mds_utils.add_or_get_examination_to_case(patho_user_id, case_id, app_id)
    scope_id, scope_header = create_scope(ex_id)

    # tutorial 03 - only create inputs
    inputs_by_key, _ = gen_data_TA03_v3._gen_inputs(ead, scope_id, scope_header, slide_ids[0])
    _, job_id, _ = mds_utils.create_job(ead, app_id, patho_user_id, ex_id)
    wbs_utils.add_inputs_to_job(job_id, inputs_by_key, scope_id, scope_header)
    wbs_utils.run_job(job_id, scope_id, scope_header)

    # check runtime: None
    check_runtime(job_id=job_id, is_none=True)

    final_jes_status = "TERMINATED"
    final_job_status = "COMPLETED"

    wait_jes_job_status(job_id=job_id, final_status=final_jes_status)
    wait_js_job_status(job_id=job_id, final_status=final_job_status)
    gen_data_TA03_v3.check_containerized_app_output(job_id)

    # check runtime: not None
    check_runtime(job_id=job_id, is_none=False)


def check_runtime(job_id, is_none: bool):
    url = f"{general_settings.mds_host}v3/jobs/{job_id}"
    headers = login_manager.wbs_client()
    r = requests.get(url, headers=headers, timeout=3)
    r.raise_for_status()
    job = r.json()
    print(job)
    if job["status"] != "RUNNING":
        return
    if is_none:
        assert job["runtime"] is None
    else:
        assert job["runtime"] is not None
