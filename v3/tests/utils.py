import json
from pathlib import Path, PurePath

import requests

from ...global_services.data_generators.singletons import general_settings, login_manager
from ..data_generators.case_data import get_test_cases
from ..data_generators.medical_data_service import utils as utils_mds


def read_case(get_case_data_func, case_group, case_tag, api):
    cases = get_case_data_func()
    case_path = PurePath(f"{Path.cwd()}/{cases[api][case_group][case_tag]['path']}")
    with open(case_path, encoding="UTF-8") as f:
        return json.load(f)


def read_case_from_file(case_group, case_tag, api):
    case = read_case(get_test_cases, case_group, case_tag, api)
    case_id = case["case_id"]
    slide_ids = case["slide_ids"]
    return case_id, slide_ids


def get_case_counts(headers, case_id, api_version="v3"):
    url = f"{general_settings.wbs_host}{api_version}/cases/{case_id}"
    r = requests.get(url, headers=headers)
    print(r.text)
    print("WBS - GET case, status_code: ", r.status_code)
    assert r.status_code == 200
    case = r.json()
    print(case)
    return case["slides_count"], len(case["examinations"])


def set_js_job_status(job_id, status):
    headers = login_manager.wbs_client()
    url = f"{general_settings.mds_host}v3/jobs/{job_id}/status"
    data = {"status": status}
    r = requests.put(url, json=data, headers=headers)
    print(f"J_Service - PUT job status [{status}], status_code: ", r.status_code)
    print(r.text)
    assert r.status_code == 200 or r.status_code == 400  # job not found


def get_annotation_count(headers, slide_id, query):
    if query is None:
        query = {}
    url = f"{general_settings.wbs_host}v3/slides/{slide_id}/annotations/query"
    r = requests.put(url, json=query, headers=headers, params={"limit": 0})
    print("WBS - PUT annotations/query, status_code: ", r.status_code)
    assert r.status_code == 200
    result_json = r.json()
    return result_json["item_count"]


def create_scope(ex_id):
    url = f"{general_settings.wbs_host}v3/examinations/{ex_id}/scope"
    headers = login_manager.patho_user()
    r = requests.put(url, headers=headers)
    print(r.text)
    assert r.status_code == 201 or r.status_code == 200
    data = r.json()
    scope_id = data["scope_id"]
    scope_access_token = data["access_token"]
    scope_header = {"Authorization": f"Bearer {scope_access_token}"}
    return scope_id, scope_header


def create_scope_mta(ex_id):
    url = f"{general_settings.mds_host}v3/scopes"
    headers = login_manager.wbs_client()
    mta_id = login_manager.mta_user()["user-id"]
    payload = {"examination_id": ex_id, "user_id": mta_id}
    r = requests.put(url, headers=headers, json=payload)
    print(r.text)
    assert r.status_code == 201 or r.status_code == 200
    data = r.json()
    scope_id = data["id"]
    url = f"{general_settings.mds_host}v3/scopes/{scope_id}/token"
    r = requests.get(url, headers=headers)
    assert r.status_code == 200
    raw_access_token = r.json()
    scope_access_token = raw_access_token["access_token"]
    scope_header = {"Authorization": f"Bearer {scope_access_token}"}
    return scope_id, scope_header


def mds_post_job_data(app_id, creator_id, creator_type):
    return {
        "app_id": app_id,
        "creator_id": creator_id,
        "creator_type": creator_type,
    }


def wbs_post_job_data(creator_id, creator_type):
    return {
        "creator_id": creator_id,
        "creator_type": creator_type,
    }


def add_or_get_examination_to_case(case_id, app_id):
    patho_user_id = login_manager.patho_user()["user-id"]
    ex_id = utils_mds.add_or_get_examination_to_case(patho_user_id, case_id, app_id)
    return ex_id


def query_scopes(scope_id, headers):
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/scopes"
    r = requests.get(url, headers=headers)
    print(r.text)
    assert r.status_code == 200
    return r.json()
