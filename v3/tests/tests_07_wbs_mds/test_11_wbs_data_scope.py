import uuid

import requests
from sample_apps.sample_apps.tutorial._TA05_classes import v3 as app

from ....global_services.data_generators.marketplace_service import utils as utils_mps
from ....global_services.data_generators.singletons import general_settings, login_manager, multi_user_settings
from ...data_generators.case_data import get_test_cases
from ...data_generators.medical_data_service import utils
from ..utils import create_scope, create_scope_mta, read_case


def test_data_scope():
    patho_user_id = login_manager.patho_user()["user-id"]

    # read cases from file
    case_1 = read_case(get_test_cases, "WBS_MDS_CASES_v3", "TEST11v3", "v3")
    # case_2 = read_case(get_test_cases, "WBS_MDS_CASES_v3", "TEST11Bv3", "v3")
    case_id_1 = case_1["case_id"]
    # case_id_2 = case_2["case_id"]

    headers = login_manager.wbs_client()
    app_id_1 = utils_mps.get_app_id_by_ead(headers, app.ead)
    app_id_2 = utils_mps.get_app_id_by_ead(headers, app.ead)

    ex_id_1 = utils.add_or_get_examination_to_case(patho_user_id, case_id_1, app_id_1)
    ex_id_2 = utils.add_or_get_examination_to_case(patho_user_id, case_id_1, app_id_2)

    scope_id_1, scope_header_1 = create_scope(ex_id_1)
    _, scope_header_2 = create_scope(ex_id_2)
    scope_id_3, _ = create_scope_mta(ex_id_1)

    # Post annotation, is not roi
    params = {}
    point = {
        "name": "annotation_1",
        "type": "point",
        "coordinates": [10000, 20000],
        "npp_created": 5000,
        "npp_viewing": [1, 50000],
        "creator_id": scope_id_1,
        "creator_type": "scope",
        "reference_id": "some-id",
        "reference_type": "wsi",
    }
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/annotations"
    r = requests.post(url, json=point, params=params, headers=scope_header_1)
    print("Create annotation in scope:", r.text)
    assert r.status_code == 201
    annotation_id = r.json()["id"]

    for data_type in ["primitives", "classes", "collections"]:
        # Post incorrect scope
        url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/{data_type}"
        post_data = {
            "creator_id": "some_id",  # not scope id
            "creator_type": "scope",
        }

        if data_type == "primitives":
            post_data["type"] = "integer"
            post_data["value"] = 100
            post_data["name"] = f"{data_type} Name"
            post_data["description"] = f"{data_type} Description"
        if data_type == "classes":
            post_data["type"] = "class"
            post_data["value"] = "dummy"
            post_data["reference_id"] = annotation_id
            post_data["reference_type"] = "annotation"
        if data_type == "collections":
            post_data["type"] = "collection"
            post_data["item_type"] = "point"

        r = requests.post(url, json=post_data, headers=scope_header_1)
        assert r.status_code == 412
        assert r.json()["detail"] == "Creator_id must be set to scope_id"

        # Post incorrect scope type
        url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/{data_type}"
        post_data["creator_id"] = str(scope_id_1)  # correct scope
        post_data["creator_type"] = "user"  # but wrong type
        r = requests.post(url, json=post_data, headers=scope_header_1)
        assert r.status_code == 412
        assert r.json()["detail"] == "Creator_type must be set to scope"

        if data_type == "classes":
            url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/{data_type}"
            post_data["creator_id"] = str(scope_id_1)  # correct scope
            post_data["creator_type"] = "scope"  # correct type type
            r = requests.post(url, json=post_data, headers=scope_header_1)
            assert r.status_code == 400
            assert r.json()["detail"] == "Invalid class name for EAD"
            # set valid class value
            post_data["value"] = "org.empaia.vendor_name.tutorial_app_05.v3.0.classes.tumor"

        # Query, valid scope id in query params set
        if data_type != "collections":
            url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/{data_type}/query"
            query = {"creators": [str(scope_id_1)]}
            r = requests.put(url, json=query, headers=scope_header_1)
            assert r.status_code == 200
            item_count_old = r.json()["item_count"]

        # Valid post
        url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/{data_type}"
        post_data["creator_id"] = str(scope_id_1)  # correct scope
        post_data["creator_type"] = "scope"  # correct type type
        r = requests.post(url, json=post_data, headers=scope_header_1)
        print(r.text)
        assert r.status_code == 201
        assert r.json()["creator_id"] == str(scope_id_1)
        data_type_id = r.json()["id"]

        # Query, no query params set
        url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/{data_type}/query"
        query = {}
        r = requests.put(url, json=query, headers=scope_header_1)
        assert r.status_code == 400
        if data_type == "collections":
            expected_error = "Either a valid creator_id or job list must be set as query parameter"
        else:
            expected_error = (
                f"Either a valid creator_id or job list or a list of {data_type} must be set as query parameter"
            )
        assert r.json()["detail"] == expected_error

        # Query, valid scope id in query params set
        if data_type != "collections":
            url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/{data_type}/query"
            query = {"creators": [str(scope_id_1)]}
            r = requests.put(url, json=query, headers=scope_header_1)
            assert r.status_code == 200
            assert r.json()["item_count"] == item_count_old + 1

        # Query, valid item id in query params set
        if data_type != "collections":
            url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/{data_type}/query"
            query = {data_type: [str(data_type_id)]}
            r = requests.put(url, json=query, headers=scope_header_1)
            assert r.status_code == 200
            assert r.json()["item_count"] == 1

        # Query, other scope id in query params set
        if data_type != "collections":
            url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/{data_type}/query"
            query = {"creators": ["some_id"]}
            r = requests.put(url, json=query, headers=scope_header_1)
            if multi_user_settings.disable_multi_user:
                assert r.status_code == 400
                if data_type == "collections":
                    expected_error = "Either a valid creator_id or job list must be set as query parameter"
                else:
                    expected_error = (
                        f"Either a valid creator_id or job list or a list of {data_type} must be set as query parameter"
                    )
                assert r.json()["detail"] == expected_error
            else:
                assert r.status_code == 412
                assert r.json()["detail"] == "Invalid creator list query parameter for current scope"

            query = {"creators": [str(scope_id_3)]}
            r = requests.put(url, json=query, headers=scope_header_1)
            if multi_user_settings.disable_multi_user:
                assert r.status_code == 400
                if data_type == "collections":
                    expected_error = "Either a valid creator_id or job list must be set as query parameter"
                else:
                    expected_error = (
                        f"Either a valid creator_id or job list or a list of {data_type} must be set as query parameter"
                    )
                assert r.json()["detail"] == expected_error
            else:
                assert r.status_code == 200

        # Query, invalid job id in query params set
        if data_type != "collections":
            url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/{data_type}/query"
            query = {"jobs": [str(uuid.uuid4())]}
            r = requests.put(url, json=query, headers=scope_header_1)
            assert r.status_code == 412
            assert r.json()["detail"] == "Invalid job list query parameter for current scope"

        # Query unique references
        url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/{data_type}/query/unique-references"
        query = {"creators": [str(scope_id_1)]}
        print(query)
        print(data_type)
        r = requests.put(url, json=query, headers=scope_header_1)
        assert r.status_code == 200

        if data_type == "primitives" or data_type == "collections":
            print(r.json())
            assert r.json()["contains_items_without_reference"] is True
        if data_type == "classes":
            assert annotation_id in r.json()["annotation"]

        # Get correct scope
        url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/{data_type}/{data_type_id}"
        r = requests.get(url, headers=scope_header_1)
        assert r.status_code == 200
        assert r.json()["creator_id"] == str(scope_id_1)

        # Delete incorrect scope
        url = f"{general_settings.wbs_host}v3/scopes/some_id/{data_type}/{data_type_id}"
        r = requests.delete(url, headers=scope_header_2)
        assert r.status_code == 412
        assert "Precondition Failed" in r.json()["detail"]

        # Delete correct scope
        url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/{data_type}/{data_type_id}"
        r = requests.delete(url, headers=scope_header_1)
        assert r.status_code == 200
        assert r.json()["id"] == str(data_type_id)

    # Get class namespaces, invalid scope
    url = f"{general_settings.wbs_host}v3/scopes/some_id/class-namespaces"
    r = requests.get(url, headers=scope_header_1)
    assert r.status_code == 412

    # Get class namespaces
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/class-namespaces"
    r = requests.get(url, headers=scope_header_1)
    assert r.status_code == 200
    assert len(r.json()["org.empaia.global.v1"]["classes"]) == 1
    assert len(r.json()["org.empaia.vendor_name.tutorial_app_05.v3.0"]["classes"]) == 2
