import io
import uuid

import requests
from PIL import Image
from sample_apps.sample_apps.tutorial._TA01_simple_app import v3 as app

from ....global_services.data_generators.marketplace_service.utils import get_app_id_by_ead
from ....global_services.data_generators.singletons import general_settings, login_manager
from ...data_generators.case_data import get_test_cases
from ...data_generators.medical_data_service import utils
from ...tests.utils import create_scope, read_case
from ..utils import create_scope

PARAMS = {"image_format": "jpg", "image_quality": 99}


def test_slides_scope():
    patho_user_id = login_manager.patho_user()["user-id"]

    # read cases from file
    case = read_case(get_test_cases, "WBS_MDS_CASES_v3", "TEST10Bv3", "v3")
    case_id = case["case_id"]
    slide_ids = case["slide_ids"]

    slide_id = slide_ids[0]

    # add examination
    app_id = get_app_id_by_ead(login_manager.wbs_client(), app.ead)
    ex_id = utils.add_or_get_examination_to_case(patho_user_id, case_id, app_id)

    # Create scope
    scope_id, scope_header = create_scope(ex_id)

    # Get all slides for scope
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/slides"
    r = requests.get(url, headers=scope_header)
    print(r.content)
    assert r.status_code == 200
    assert len(r.json()["items"]) == 1

    # Get slide info for scope
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/slides/{slide_id}/info"
    r = requests.get(url, headers=scope_header)
    assert r.status_code == 200
    assert r.json()["id"] == slide_ids[0]

    # Get slide info for invalid scope uuid
    url = f"{general_settings.wbs_host}v3/scopes/{str(uuid.uuid4())}/slides/{slide_id}/info"
    r = requests.get(url, headers=scope_header)
    assert r.status_code == 412
    assert r.json()["detail"] == "Precondition Failed"

    # Get slide info for invalid slide id
    invalid_slide_id = str(uuid.uuid4())
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/slides/{invalid_slide_id}/info"
    r = requests.get(url, headers=scope_header)
    assert r.status_code == 404
    assert r.json()["detail"] == "Requested slide ID does not exist for current scope"

    # Get slide label
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/slides/{slide_id}/label/max_size/100/100"
    r = requests.get(url, headers=scope_header, params=PARAMS)
    assert r.status_code == 200
    assert r.headers["content-type"] == "image/jpeg"
    Image.open(io.BytesIO(r.content))

    # Get slide macro
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/slides/{slide_id}/macro/max_size/100/100"
    r = requests.get(url, headers=scope_header, params=PARAMS)
    assert r.status_code == 200
    assert r.headers["content-type"] == "image/jpeg"
    Image.open(io.BytesIO(r.content))

    # Get slide thumbnail
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/slides/{slide_id}/thumbnail/max_size/100/100"
    r = requests.get(url, headers=scope_header, params=PARAMS)
    assert r.status_code == 200
    assert r.headers["content-type"] == "image/jpeg"
    Image.open(io.BytesIO(r.content))

    # Get slide tile
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/slides/{slide_id}/tile/level/0/tile/1/1"
    r = requests.get(url, headers=scope_header, params=PARAMS)
    assert r.status_code == 200
    assert r.headers["content-type"] == "image/jpeg"
    Image.open(io.BytesIO(r.content))

    # region
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/slides/{slide_id}/region/level/0/start/0/0/size/200/200"
    r = requests.get(url, headers=scope_header, params=PARAMS)
    print("WBS - GET slide region, status_code: ", r.status_code)
    assert r.status_code == 200
    assert r.headers["content-type"] == "image/jpeg"
    Image.open(io.BytesIO(r.content))

    # Get tile for invalid scope uuid
    url = f"{general_settings.wbs_host}v3/scopes/{str(uuid.uuid4())}/slides/{slide_id}/tile/level/0/tile/1/1"
    r = requests.get(url, headers=scope_header)
    assert r.status_code == 412
    assert r.json()["detail"] == "Precondition Failed"
