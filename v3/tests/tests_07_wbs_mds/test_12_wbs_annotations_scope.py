import requests
from sample_apps.sample_apps.tutorial._TA05_classes import v3 as app

from ....global_services.data_generators.marketplace_service import utils as utils_mps
from ....global_services.data_generators.singletons import general_settings, login_manager, multi_user_settings
from ...data_generators.case_data import get_test_cases
from ...data_generators.medical_data_service import utils
from ..utils import create_scope, create_scope_mta, read_case


def test_annotations_scope():
    patho_user_id = login_manager.patho_user()["user-id"]

    # read cases from file
    case_1 = read_case(get_test_cases, "WBS_MDS_CASES_v3", "TEST12v3", "v3")
    case_2 = read_case(get_test_cases, "WBS_MDS_CASES_v3", "TEST12Bv3", "v3")
    case_id_1 = case_1["case_id"]
    case_id_2 = case_2["case_id"]

    headers = login_manager.wbs_client()
    app_id_1 = utils_mps.get_app_id_by_ead(headers, app.ead)
    app_id_2 = utils_mps.get_app_id_by_ead(headers, app.ead)

    ex_id_1 = utils.add_or_get_examination_to_case(patho_user_id, case_id_1, app_id_1)
    ex_id_2 = utils.add_or_get_examination_to_case(patho_user_id, case_id_2, app_id_2)

    scope_id_1, scope_header_1 = create_scope(ex_id_1)
    _, scope_header_2 = create_scope(ex_id_2)
    scope_id_3, _ = create_scope_mta(ex_id_1)

    # Put annotation query, scope id in query params set
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/annotations/query"
    query = {"creators": [str(scope_id_1)]}
    r = requests.put(url, json=query, headers=scope_header_1)
    assert r.status_code == 200
    annotation_count_old = r.json()["item_count"]

    # Post annotation as ROI for scope
    point = {
        "name": "annotation_1",
        "type": "point",
        "coordinates": [10000, 20000],
        "npp_created": 5000,
        "npp_viewing": [1, 50000],
        "creator_id": scope_id_1,
        "creator_type": "scope",
        "reference_id": "some-id",
        "reference_type": "wsi",
    }
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/annotations"
    params = {}
    params["is_roi"] = True
    r = requests.post(url, json=point, params=params, headers=scope_header_1)
    assert r.status_code == 201
    assert r.json()["name"] == "annotation_1"
    assert r.json()["classes"][0]["value"] == "org.empaia.global.v1.classes.roi"
    annotation_id = r.json()["id"]

    # Post annotation, is not roi
    params = {}
    point["name"] = "annotation_2"
    r = requests.post(url, json=point, params=params, headers=scope_header_1)
    assert r.status_code == 201
    assert r.json()["name"] == "annotation_2"
    assert r.json()["classes"] == []

    # Get annotation
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/annotations/{annotation_id}"
    r = requests.get(url, headers=scope_header_1)
    assert r.status_code == 200
    assert r.json()["name"] == "annotation_1"

    # Post annotation as ROI for wrong scope
    point["creator_id"] = "some_id"
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/annotations"
    r = requests.post(url, json=point, params=params, headers=scope_header_1)
    assert r.status_code == 412

    # Put annotation query, no query params set
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/annotations/query"
    query = {}
    r = requests.put(url, json=query, headers=scope_header_1)
    assert r.status_code == 400
    assert (
        r.json()["detail"]
        == "Either a valid creator_id or job list or a list of annotations must be set as query parameter"
    )

    # Put annotation query, scope id in query params set
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/annotations/query"
    query = {"creators": [str(scope_id_1)]}
    r = requests.put(url, json=query, headers=scope_header_1)
    assert r.status_code == 200
    assert r.json()["item_count"] == annotation_count_old + 2
    assert len(r.json()["items"]) == annotation_count_old + 2

    # Put annotation query, scope id in query params set
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/annotations/query"
    query = {"creators": ["some_id"]}
    r = requests.put(url, json=query, headers=scope_header_1)
    if multi_user_settings.disable_multi_user:
        assert r.status_code == 400
        assert (
            r.json()["detail"]
            == "Either a valid creator_id or job list or a list of annotations must be set as query parameter"
        )
    else:
        assert r.status_code == 412
        assert r.json()["detail"] == "Invalid creator list query parameter for current scope"

    query = {"creators": [str(scope_id_3)]}
    r = requests.put(url, json=query, headers=scope_header_1)
    if multi_user_settings.disable_multi_user:
        assert r.status_code == 400
        assert (
            r.json()["detail"]
            == "Either a valid creator_id or job list or a list of annotations must be set as query parameter"
        )
    else:
        assert r.status_code == 200

    query = {"creators": ["some_id"]}
    r = requests.put(url, json=query, headers=scope_header_1)
    if multi_user_settings.disable_multi_user:
        assert r.status_code == 400
        assert (
            r.json()["detail"]
            == "Either a valid creator_id or job list or a list of annotations must be set as query parameter"
        )
    else:
        assert r.status_code == 412
        assert r.json()["detail"] == "Invalid creator list query parameter for current scope"

    for suffix in ["count", "viewer", "unique-class-values", "unique-references"]:
        # Query annotations
        url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/annotations/query/{suffix}"
        query = {"creators": [str(scope_id_1)]}
        if suffix == "viewer":
            query["npp_viewing"] = [5.67, 7.89]
        r = requests.put(url, json=query, headers=scope_header_1)
        assert r.status_code == 200

        # Query annotations, no query params set
        query = {}
        r = requests.put(url, json=query, headers=scope_header_1)
        assert r.status_code == 400
        assert (
            r.json()["detail"]
            == "Either a valid creator_id or job list or a list of annotations must be set as query parameter"
        )

        # Query annotations, no query params set, wrong header
        query = {}
        r = requests.put(url, json=query, headers=scope_header_2)
        assert r.status_code == 412
        assert r.json()["detail"] == "Precondition Failed"

    # Delete annotation, wrong scope
    url = f"{general_settings.wbs_host}v3/scopes/some_id/annotations/{annotation_id}"
    r = requests.delete(url, headers=scope_header_2)
    assert r.status_code == 412

    # Delete annotation
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/annotations/{annotation_id}"
    r = requests.delete(url, headers=scope_header_1)
    assert r.status_code == 200
    assert r.json()["id"] == annotation_id

    # Get annotation
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/annotations/{annotation_id}"
    r = requests.get(url, headers=scope_header_1)
    assert r.status_code == 404
