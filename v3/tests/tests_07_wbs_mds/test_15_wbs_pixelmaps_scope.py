import gzip

import requests
from sample_apps.sample_apps.tutorial._TA05_classes import v3 as app

from ....global_services.data_generators.marketplace_service import utils as utils_mps
from ....global_services.data_generators.singletons import general_settings, login_manager
from ...data_generators.case_data import get_test_cases
from ...data_generators.medical_data_service import utils
from ..utils import create_scope, read_case


def test_annotations_scope():
    patho_user_id = login_manager.patho_user()["user-id"]

    # read cases from file
    case = read_case(get_test_cases, "WBS_MDS_CASES_v3", "TEST15v3", "v3")
    case_id = case["case_id"]
    slide_ids = case["slide_ids"]
    slide_id = slide_ids[0]

    headers = login_manager.wbs_client()
    app_id = utils_mps.get_app_id_by_ead(headers, app.ead)

    ex_id = utils.add_or_get_examination_to_case(patho_user_id, case_id, app_id)

    scope_id, scope_header = create_scope(ex_id)

    scope_header["Accept-Encoding"] = None
    scope_header["Content-Encoding"] = None

    # Post pixelmap
    pixelmap = {
        "type": "nominal_pixelmap",
        "name": "Pixelmap",
        "reference_type": "wsi",
        "reference_id": slide_id,
        "creator_id": scope_id,
        "creator_type": "scope",
        "tilesize": 256,
        "neutral_value": 0,
        "element_type": "uint8",
        "channel_count": 1,
        "levels": [
            {"slide_level": 0, "position_min_x": 0, "position_max_x": 3, "position_min_y": 0, "position_max_y": 3},
        ],
        "element_class_mapping": [{"number_value": 0, "class_value": "some-class"}],
    }
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/pixelmaps"
    params = {}
    r = requests.post(url, json=pixelmap, params=params, headers=scope_header)
    assert r.status_code == 201
    assert r.json()["name"] == "Pixelmap"
    pixelmap_id = r.json()["id"]

    # Get pixelmap
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/pixelmaps/{pixelmap_id}"
    r = requests.get(url, headers=scope_header)
    assert r.status_code == 200
    assert r.json()["name"] == "Pixelmap"

    # put tile
    int_array = [0 for _ in range(256 * 256)]
    int_array[42] = 1
    byte_array = bytearray(int_array)
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/pixelmaps/{pixelmap_id}/level/{0}/position/{0}/{0}/data"
    r = requests.put(url, data=byte_array, headers=scope_header)
    assert r.status_code == 204
    content_encoding = r.headers.get("Content-Encoding")
    assert content_encoding is None

    # get tile
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/pixelmaps/{pixelmap_id}/level/{0}/position/{0}/{0}/data"
    r = requests.get(url, headers=scope_header)
    assert r.status_code == 200
    int_array = list(r.content)
    assert int_array[42] == 1
    assert int_array[43] == 0
    content_encoding = r.headers.get("Content-Encoding")
    assert content_encoding is None

    # put tile
    int_array = [0 for _ in range(256 * 256)]
    int_array[42] = 1
    byte_array = bytearray(int_array)
    byte_array = gzip.compress(byte_array, compresslevel=1)
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/pixelmaps/{pixelmap_id}/level/{0}/position/{0}/{0}/data"
    scope_header["Accept-Encoding"] = None
    scope_header["Content-Encoding"] = "gzip"
    r = requests.put(url, data=byte_array, headers=scope_header)
    assert r.status_code == 204
    content_encoding = r.headers.get("Content-Encoding")
    assert content_encoding is None

    # get tile
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/pixelmaps/{pixelmap_id}/level/{0}/position/{0}/{0}/data"
    scope_header["Accept-Encoding"] = "gzip"
    scope_header["Content-Encoding"] = None
    r = requests.get(url, headers=scope_header)
    assert r.status_code == 200
    int_array = list(r.content)
    assert int_array[42] == 1
    assert int_array[43] == 0
    content_encoding = r.headers.get("Content-Encoding")
    assert content_encoding is not None
    assert "gzip" in content_encoding

    scope_header["Accept-Encoding"] = None
    scope_header["Content-Encoding"] = None

    # Post pixelmap for bulk test
    pixelmap = {
        "type": "nominal_pixelmap",
        "name": "Pixelmap",
        "reference_type": "wsi",
        "reference_id": slide_id,
        "creator_id": scope_id,
        "creator_type": "scope",
        "tilesize": 256,
        "neutral_value": 0,
        "element_type": "uint8",
        "channel_count": 1,
        "levels": [
            {"slide_level": 0, "position_min_x": 0, "position_max_x": 3, "position_min_y": 0, "position_max_y": 3},
        ],
        "element_class_mapping": [{"number_value": 0, "class_value": "some-class"}],
    }
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/pixelmaps"
    params = {}

    r = requests.post(url, json=pixelmap, params=params, headers=scope_header)
    assert r.status_code == 201
    assert r.json()["name"] == "Pixelmap"
    content_encoding = r.headers.get("Content-Encoding")
    assert content_encoding is None
    pixelmap_id = r.json()["id"]

    # put tile bulk
    int_array = [0 for _ in range(256 * 256)]
    int_array[42] = 1
    byte_array = bytearray(int_array)
    url_base = f"{general_settings.wbs_host}v3/scopes/{scope_id}"
    url = url_base + f"/pixelmaps/{pixelmap_id}/level/{0}/position/start/{0}/{0}/end/{0}/{1}/data"
    r = requests.put(url, data=byte_array + byte_array, headers=scope_header)
    assert r.status_code == 204
    content_encoding = r.headers.get("Content-Encoding")
    assert content_encoding is None

    # get tile bulk
    url_base = f"{general_settings.wbs_host}v3/scopes/{scope_id}"
    url = url_base + f"/pixelmaps/{pixelmap_id}/level/{0}/position/start/{0}/{0}/end/{0}/{1}/data"
    r = requests.get(url, headers=scope_header)
    assert r.status_code == 200
    int_array = list(r.content)
    assert int_array[42] == 1
    assert int_array[42 + 256 * 256] == 1
    assert int_array[43] == 0
    content_encoding = r.headers.get("Content-Encoding")
    assert content_encoding is None

    # put tile bulk
    int_array = [0 for _ in range(256 * 256)]
    int_array[42] = 1
    byte_array = bytearray(int_array)
    byte_array = gzip.compress(byte_array, compresslevel=1)
    url_base = f"{general_settings.wbs_host}v3/scopes/{scope_id}"
    url = url_base + f"/pixelmaps/{pixelmap_id}/level/{0}/position/start/{0}/{0}/end/{0}/{1}/data"
    scope_header["Accept-Encoding"] = None
    scope_header["Content-Encoding"] = "gzip"
    r = requests.put(url, data=byte_array + byte_array, headers=scope_header)
    assert r.status_code == 204
    content_encoding = r.headers.get("Content-Encoding")
    assert content_encoding is None

    # get tile bulk
    url_base = f"{general_settings.wbs_host}v3/scopes/{scope_id}"
    url = url_base + f"/pixelmaps/{pixelmap_id}/level/{0}/position/start/{0}/{0}/end/{0}/{1}/data"
    scope_header["Accept-Encoding"] = "gzip"
    scope_header["Content-Encoding"] = None
    r = requests.get(url, headers=scope_header)
    assert r.status_code == 200
    int_array = list(r.content)
    assert int_array[42] == 1
    assert int_array[42 + 256 * 256] == 1
    assert int_array[43] == 0
    content_encoding = r.headers.get("Content-Encoding")
    assert content_encoding is not None
    assert "gzip" in content_encoding

    scope_header["Accept-Encoding"] = None
    scope_header["Content-Encoding"] = None

    # get pixelmap info
    url_base = f"{general_settings.wbs_host}v3/scopes/{scope_id}"
    url = url_base + f"/pixelmaps/{pixelmap_id}/info"
    r = requests.get(url, headers=scope_header)
    assert r.status_code == 200
    info = r.json()
    for key in ["extent", "num_levels", "pixel_size_nm", "levels"]:
        assert key in info
