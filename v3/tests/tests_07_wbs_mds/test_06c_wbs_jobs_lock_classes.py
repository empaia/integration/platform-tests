import requests
from sample_apps.sample_apps.test.internal._ITA12_job_input_class_constraint import v3 as ita12v3
from sample_apps.sample_apps.tutorial._TA05_classes import v3 as ta05v3

from ....global_services.data_generators.marketplace_service.utils import get_app_id_by_ead
from ....global_services.data_generators.singletons import general_settings, login_manager
from .. import utils


def test_run_job_lock_classes():
    # read cases from file
    case_id, slide_ids = utils.read_case_from_file("WBS_MDS_CASES_v3", "TEST06Cv3", "v3")

    app_id_1 = get_app_id_by_ead(login_manager.wbs_client(), ta05v3.ead)
    app_id_2 = get_app_id_by_ead(login_manager.wbs_client(), ita12v3.ead)

    ex_id_1 = utils.add_or_get_examination_to_case(case_id, app_id_1)
    ex_id_2 = utils.add_or_get_examination_to_case(case_id, app_id_2)

    scope_id_1, scope_header_1 = utils.create_scope(ex_id_1)
    scope_id_2, scope_header_2 = utils.create_scope(ex_id_2)

    # Post new job
    job_data_1 = utils.wbs_post_job_data(scope_id_1, "SCOPE")
    job_data_2 = utils.wbs_post_job_data(scope_id_1, "SCOPE")

    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/jobs"
    r = requests.post(url, json=job_data_1, headers=scope_header_1)
    assert r.status_code == 201
    job = r.json()
    job_id = job["id"]

    # Put wsi input
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/jobs/{job_id}/inputs/my_wsi"
    r = requests.put(url, json=dict(id=slide_ids[0]), headers=scope_header_1)
    assert r.status_code == 200

    # Post annotation as ROI for scope
    rectangle = {
        "name": "Rectangle ROI",
        "type": "rectangle",
        "upper_left": [0, 26151],
        "width": 1770,
        "height": 1638,
        "npp_created": 5000,
        "npp_viewing": [1, 50000],
        "creator_id": scope_id_1,
        "creator_type": "scope",
        "reference_id": slide_ids[0],
        "reference_type": "wsi",
    }
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/annotations"
    params = {}
    params["is_roi"] = True
    r = requests.post(url, json=rectangle, params=params, headers=scope_header_1)
    assert r.status_code == 201
    assert r.json()["name"] == "Rectangle ROI"
    assert r.json()["classes"][0]["value"] == "org.empaia.global.v1.classes.roi"
    annotation_id = r.json()["id"]

    # post collection
    collection = {
        "name": "Collection name",
        "description": "Collection description",
        "creator_id": str(scope_id_1),
        "creator_type": "scope",
        "reference_id": slide_ids[0],
        "reference_type": "wsi",
        "type": "collection",
        "item_type": "rectangle",
        "items": [{"id": annotation_id}],
    }
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/collections"
    r = requests.post(url, json=collection, headers=scope_header_1)
    result_json = r.json()
    assert r.status_code == 201
    assert result_json["reference_type"] == "wsi"
    assert result_json["item_type"] == "rectangle"
    collection_id = result_json["id"]

    # put annotation input
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/jobs/{job_id}/inputs/my_rectangles"
    r = requests.put(url, json=dict(id=collection_id), headers=scope_header_1)
    assert r.status_code == 200

    # Run job
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/jobs/{job_id}/run"
    r = requests.put(url, headers=scope_header_1)
    print(r.text)
    assert r.status_code == 200

    # Post new job
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/jobs"
    r = requests.post(url, json=job_data_1, headers=scope_header_1)
    assert r.status_code == 201
    job = r.json()
    job_id = job["id"]

    # Post wsi input
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/jobs/{job_id}/inputs/my_wsi"
    r = requests.put(url, json=dict(id=slide_ids[0]), headers=scope_header_1)
    assert r.status_code == 200

    # post annotation without class
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/annotations"
    rectangle["name"] = "Rectangle ROI 2"
    params = {}
    r = requests.post(url, json=rectangle, params=params, headers=scope_header_1)
    assert r.status_code == 201
    assert r.json()["name"] == "Rectangle ROI 2"
    assert r.json()["classes"] == []
    annotation_id = r.json()["id"]

    # post collection
    collection["items"] = [{"id": annotation_id}]
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/collections"
    r = requests.post(url, json=collection, headers=scope_header_1)
    result_json = r.json()
    assert r.status_code == 201
    assert result_json["reference_type"] == "wsi"
    assert result_json["item_type"] == "rectangle"
    collection_id = result_json["id"]

    # post annotation input
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/jobs/{job_id}/inputs/my_rectangles"
    r = requests.put(url, json=dict(id=collection_id), headers=scope_header_1)
    assert r.status_code == 200

    # Run job with unfullfilled class constraint in inputs
    # UPDATE 2023-01-09: type checking done by Worbench daemon offline
    # Running a job now here succeeds
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/jobs/{job_id}/run"
    r = requests.put(url, headers=scope_header_1)
    assert r.status_code == 200

    # CLASS VALIDATION

    # Post new job
    job_data_2["creator_id"] = scope_id_2
    job_data_2["creator_type"] = "SCOPE"
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/jobs"
    r = requests.post(url, json=job_data_2, headers=scope_header_2)
    assert r.status_code == 201
    job_2 = r.json()
    job_id_2 = job_2["id"]

    # Post wsi input
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/jobs/{job_id_2}/inputs/my_wsi"
    r = requests.put(url, json=dict(id=slide_ids[0]), headers=scope_header_2)
    assert r.status_code == 200

    # Post annotation as ROI for scope
    rectangle = {
        "name": "Rectangle ROI",
        "type": "rectangle",
        "upper_left": [0, 26151],
        "width": 1770,
        "height": 1638,
        "npp_created": 5000,
        "npp_viewing": [1, 50000],
        "creator_id": scope_id_2,
        "creator_type": "scope",
        "reference_id": slide_ids[0],
        "reference_type": "wsi",
    }
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/annotations"
    params = {}
    r = requests.post(url, json=rectangle, params=params, headers=scope_header_2)
    assert r.status_code == 201
    assert r.json()["name"] == "Rectangle ROI"
    annotation_id2 = r.json()["id"]

    # post class that is allowed in class constraint
    post_class = {
        "creator_id": str(scope_id_2),
        "creator_type": "scope",
        "type": "class",
        "value": "org.empaia.vendor_name.internal_test_app_12.v3.0.classes.tumor",
        "reference_id": annotation_id2,
        "reference_type": "annotation",
    }
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/classes"
    params = {}
    r = requests.post(url, json=post_class, params=params, headers=scope_header_2)
    assert r.status_code == 201

    # post collection
    collection = {
        "name": "Collection name",
        "description": "Collection description",
        "creator_id": str(scope_id_2),
        "creator_type": "scope",
        "reference_id": slide_ids[0],
        "reference_type": "wsi",
        "type": "collection",
        "item_type": "rectangle",
        "items": [{"id": annotation_id2}],
    }
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/collections"
    r = requests.post(url, json=collection, headers=scope_header_2)
    result_json = r.json()
    assert r.status_code == 201
    assert result_json["reference_type"] == "wsi"
    assert result_json["item_type"] == "rectangle"
    collection_id2 = result_json["id"]

    # post annotation input
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/jobs/{job_id_2}/inputs/my_rectangles"
    r = requests.put(url, json=dict(id=collection_id2), headers=scope_header_2)
    assert r.status_code == 200

    # post invalid class
    post_class["value"] = "org.empaia.vendor_name.internal_test_app_12.v3.0.classes.input_classes.roi_class3"
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/classes"
    params = {}
    r = requests.post(url, json=post_class, params=params, headers=scope_header_2)
    assert r.status_code == 400
    assert r.json()["detail"] == "Invalid class name for EAD"

    # post valid class
    post_class["value"] = "org.empaia.vendor_name.internal_test_app_12.v3.0.classes.input_classes.roi_class1"
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/classes"
    params = {}
    r = requests.post(url, json=post_class, params=params, headers=scope_header_2)
    assert r.status_code == 201

    # Run job again
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/jobs/{job_id_2}/run"
    r = requests.put(url, headers=scope_header_2)
    assert r.status_code == 200
