import io

import requests
from PIL import Image

from ....global_services.data_generators.singletons import general_settings, login_manager
from ...data_generators.case_data import get_test_cases
from ..utils import read_case


def test_slide_tiles_slide_info():
    # read case from file
    case = read_case(get_test_cases, "WBS_MDS_CASES_v3", "TEST10v3", "v3")
    slide_ids = case["slide_ids"]

    params = {"image_format": "jpg", "image_quality": 99, "z": 0}

    # tile
    tile_x = 100
    tile_y = 100
    level = 0

    url = f"{general_settings.wbs_host}v3/slides/{slide_ids[0]}/tile/level/{level}/tile/{tile_x}/{tile_y}"
    headers = login_manager.patho_user()
    r = requests.get(url, headers=headers, params=params)
    print("WBS - GET slide tile, status_code: ", r.status_code)
    assert r.status_code == 200
    assert r.headers["content-type"] == "image/jpeg"
    Image.open(io.BytesIO(r.content))

    # info
    url = f"{general_settings.wbs_host}v3/slides/{slide_ids[0]}/info"
    headers = login_manager.patho_user()
    r = requests.get(url, headers=headers, params=params)
    print("WBS - GET slide info, status_code: ", r.status_code)
    print(r.content)
    assert r.status_code == 200
    assert r.json()["id"] == slide_ids[0]
