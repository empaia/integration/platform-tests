import uuid

import requests
from sample_apps.sample_apps.tutorial._TA01_simple_app import v3 as ta01v3
from sample_apps.sample_apps.tutorial._TA07_configuration import v3 as ta07v3

from ....global_services.data_generators.marketplace_service import utils as utils_mps
from ....global_services.data_generators.singletons import general_settings, login_manager
from ...data_generators.case_data import get_test_cases
from ...data_generators.medical_data_service import utils
from ...data_generators.workbench_service import gen_data_TA01_v3
from ..utils import create_scope, read_case


def test_wbs_examinations_scope():
    patho_user_id = login_manager.patho_user()["user-id"]

    # read case from file
    case = read_case(get_test_cases, "WBS_MDS_CASES_v3", "TEST05Bv3", "v3")
    case_id = case["case_id"]
    slide_ids = case["slide_ids"]

    slide_id = slide_ids[0]

    headers = login_manager.wbs_client()
    app_id_1 = utils_mps.get_app_id_by_ead(headers, ta01v3.ead)
    app_id_2 = utils_mps.get_app_id_by_ead(headers, ta07v3.ead)

    ex_id_1 = utils.add_or_get_examination_to_case(patho_user_id, case_id, app_id_1)
    ex_id_2 = utils.add_or_get_examination_to_case(patho_user_id, case_id, app_id_2)
    scope_id_1, scope_header_1 = create_scope(ex_id_1)
    scope_id_2, scope_header_2 = create_scope(ex_id_2)

    app_id_1, job_id_invalid_1 = create_job_incomplete(scope_id_1, scope_header_1, ex_id_1, slide_id, ta01v3.ead)
    app_id_2, job_id_invalid_2 = create_job_incomplete(scope_id_2, scope_header_2, ex_id_2, slide_id, ta07v3.ead)

    try:
        # GET examination
        url = f"{general_settings.wbs_host}v3/examinations/{ex_id_1}"
        headers = login_manager.patho_user()
        r = requests.get(url, headers=headers)
        assert r.status_code == 200
        # PUT - New scope or get existing
        url = f"{general_settings.wbs_host}v3/examinations/{ex_id_1}/scope"
        headers = login_manager.patho_user()
        r = requests.put(url, headers=headers)
        assert r.status_code == 201 or r.status_code == 200
        data = r.json()
        scope_id_1 = data["scope_id"]
        scope_access_token_1 = data["access_token"]

        # PUT - New scope or get existing
        url = f"{general_settings.wbs_host}v3/examinations/{ex_id_2}/scope"
        headers = login_manager.patho_user()
        r = requests.put(url, headers=headers)
        assert r.status_code == 201 or r.status_code == 200
        data = r.json()
        _scope_id_2 = data["scope_id"]
        scope_access_token_2 = data["access_token"]

        # Get scope data
        url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}"
        r = requests.get(url, headers={"Authorization": f"Bearer {scope_access_token_1}"})
        assert r.status_code == 200
        data = r.json()
        assert data["id"] == scope_id_1
        assert data["user_id"] == patho_user_id
        assert data["examination_id"] == ex_id_1
        assert data["examination_state"] == "OPEN"
        assert data["case_id"] == case_id
        assert "ead" in data
        assert data["ead"] is not None
        assert "created_at" in data

        # No scope token - must be rejected
        url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}"
        r = requests.get(url)
        assert r.status_code == 403

        # Invalid scope token - must be rejected
        url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}"
        r = requests.get(url, headers={"Authorization": f"Bearer {str(uuid.uuid4())}"})
        assert r.status_code == 401

        # Valid scope token with wrong scope_id - must be rejected
        url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}"
        r = requests.get(url, headers={"Authorization": f"Bearer {scope_access_token_2}"})
        assert r.status_code == 412

    finally:
        # MDS
        set_job_failed(job_id_invalid_1)
        set_job_failed(job_id_invalid_2)


def create_job_incomplete(scope_id, scope_headers, ex_id, slide_id, ead):
    app_id = utils_mps.get_app_id_by_ead(login_manager.wbs_client(), ead)
    # incomplete job
    _, _ = gen_data_TA01_v3._gen_inputs(ead, scope_id, scope_headers, slide_id)
    _, job_id_invalid, _ = utils.create_job(ead, app_id, scope_id, ex_id)
    return app_id, job_id_invalid


def set_job_failed(job_id):
    url = f"{general_settings.mds_host}v3/jobs/{job_id}/status"
    payload = {"status": "ERROR"}
    headers = login_manager.wbs_client()
    r = requests.put(url, headers=headers, json=payload)
    print(r.content)
    print(job_id)
    assert r.status_code == 200
    assert r.json()["status"] == "ERROR"
