import requests
from sample_apps.sample_apps.tutorial._TA05_classes import v3 as app

from ....global_services.data_generators.marketplace_service import utils as utils_mps
from ....global_services.data_generators.singletons import general_settings, login_manager
from ...data_generators.case_data import get_test_cases
from ...data_generators.medical_data_service import utils, utils_examination
from ..utils import create_scope, read_case


def test_app_ui_storage():
    patho_user_id = login_manager.patho_user()["user-id"]

    # read cases from file
    case_1 = read_case(get_test_cases, "WBS_MDS_CASES_v3", "TEST14v3", "v3")
    case_2 = read_case(get_test_cases, "WBS_MDS_CASES_v3", "TEST14Bv3", "v3")
    case_id_1 = case_1["case_id"]
    case_id_2 = case_2["case_id"]

    headers = login_manager.wbs_client()
    app_id_1 = utils_mps.get_app_id_by_ead(headers, app.ead)
    app_id_2 = utils_mps.get_app_id_by_ead(headers, app.ead)

    ex_id_1 = utils.add_or_get_examination_to_case(patho_user_id, case_id_1, app_id_1)
    ex_id_2 = utils.add_or_get_examination_to_case(patho_user_id, case_id_2, app_id_2)

    urls_data_headers = get_urls_storagedata_scopeheaders(ex_id_1, ex_id_2)

    # put get:
    for el in urls_data_headers:
        url, data, header = el
        # put
        r = requests.put(url, headers=header, json=data)
        assert r.status_code == 201
        assert r.json() == data
        # get
        r = requests.get(
            url,
            headers=header,
        )
        assert r.status_code == 200
        assert r.json() == data
        # put
        data["content"]["key"] += "_(2)"
        r = requests.put(url, headers=header, json=data)
        assert r.status_code == 201
        assert r.json() == data
        # get
        r = requests.get(
            url,
            headers=header,
        )
        assert r.status_code == 200
        assert r.json() == data

    # new scope
    utils_examination.mds_close_examination(headers, ex_id_1)
    utils_examination.mds_close_examination(headers, ex_id_2)

    ex_id_1 = utils.add_or_get_examination_to_case(patho_user_id, case_id_1, app_id_1)
    ex_id_2 = utils.add_or_get_examination_to_case(patho_user_id, case_id_2, app_id_2)

    urls_data_headers = get_urls_storagedata_scopeheaders(ex_id_1, ex_id_2)

    # put get:
    for el in urls_data_headers:
        url, data, header = el
        # get
        r = requests.get(
            url,
            headers=header,
        )
        assert r.status_code == 200
        if url.endswith("/user"):
            assert r.json()["content"]["key"].endswith("_(2)")
        else:
            assert r.json() == {"content": {}}

    # wrong header
    r = requests.get(urls_data_headers[0][0], headers=urls_data_headers[1][2])
    assert r.status_code == 412
    r = requests.put(urls_data_headers[0][0], json=urls_data_headers[0][1], headers=urls_data_headers[1][2])
    assert r.status_code == 412


def test_app_ui_storage_datatypes():
    patho_user_id = login_manager.patho_user()["user-id"]

    case_1 = read_case(get_test_cases, "WBS_MDS_CASES_v3", "TEST14v3", "v3")
    case_id_1 = case_1["case_id"]

    headers = login_manager.wbs_client()
    app_id_1 = utils_mps.get_app_id_by_ead(headers, app.ead)

    ex_id_1 = utils.add_or_get_examination_to_case(patho_user_id, case_id_1, app_id_1)
    scope_id_1, scope_header_1 = create_scope(ex_id_1)

    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/app-ui-storage/user"
    data = {"content": {"str_key": "string", "int_key": 100, "numb_key": 100.382, "bool_key": False}}
    r = requests.put(url, headers=scope_header_1, json=data)
    assert r.status_code == 201
    print(r.json())
    content = r.json()["content"]
    assert isinstance(content["bool_key"], bool)
    assert isinstance(content["int_key"], int)
    assert isinstance(content["str_key"], str)
    assert isinstance(content["numb_key"], float)
    assert content == data["content"]

    r = requests.get(url, headers=scope_header_1)
    assert r.status_code == 200
    content = r.json()["content"]
    assert isinstance(content["bool_key"], bool)
    assert isinstance(content["int_key"], int)
    assert isinstance(content["str_key"], str)
    assert isinstance(content["numb_key"], float)
    assert content == data["content"]


def get_urls_storagedata_scopeheaders(ex_id1, ex_id2):
    scope_id_1, scope_header_1 = create_scope(ex_id1)
    scope_id_2, scope_header_2 = create_scope(ex_id2)

    # urls and data
    url_data_scope_1_scope = (
        f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/app-ui-storage/scope",
        {"content": {"key": "s1s"}},
        scope_header_1,
    )
    url_data_scope_2_scope = (
        f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/app-ui-storage/scope",
        {"content": {"key": "s2s"}},
        scope_header_2,
    )
    url_data_scope_1_user = (
        f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/app-ui-storage/user",
        {"content": {"key": "s1u"}},
        scope_header_1,
    )
    url_data_scope_2_user = (
        f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/app-ui-storage/user",
        {"content": {"key": "s2u"}},
        scope_header_2,
    )
    return [
        url_data_scope_1_scope,
        url_data_scope_2_scope,
        url_data_scope_1_user,
        url_data_scope_2_user,
    ]
