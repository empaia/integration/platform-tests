from uuid import uuid4

import requests
from sample_apps.sample_apps.tutorial._TA05_classes import v3 as app

from ....global_services.data_generators.marketplace_service.utils import get_app_id_by_ead
from ....global_services.data_generators.singletons import general_settings, login_manager
from ...data_generators.case_data import get_test_cases
from ...data_generators.medical_data_service import utils
from ..utils import create_scope, read_case, wbs_post_job_data


def test_wbs_data_creation_examination_closed():
    patho_user_id = login_manager.patho_user()["user-id"]

    # read case from file
    case = read_case(get_test_cases, "WBS_MDS_CASES_v3", "TEST05Bv3", "v3")
    case_id = case["case_id"]

    # add examination
    headers = login_manager.wbs_client()
    app_id = get_app_id_by_ead(login_manager.wbs_client(), app.ead)
    ex_id = utils.add_or_get_examination_to_case(patho_user_id, case_id, app_id)

    scope_id, scope_header = create_scope(ex_id)

    # PUT examination states to CLOSED
    url = f"{general_settings.wbs_host}v3/examinations/{ex_id}/close"
    headers = login_manager.patho_user()
    r = requests.put(url, headers=headers)
    print(r.content)
    assert r.status_code == 200

    # test endpoints that may not be addressed with closed examination
    error_msg = "Examination has state 'CLOSED': Create or delete actions are not allowed"

    point = {
        "name": "annotation_1",
        "type": "point",
        "coordinates": [10000, 20000],
        "npp_created": 5000,
        "npp_viewing": [1, 50000],
        "creator_id": scope_id,
        "creator_type": "scope",
        "reference_id": "some-id",
        "reference_type": "wsi",
    }
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/annotations"
    params = {}
    r = requests.post(url, json=point, params=params, headers=scope_header)
    assert r.status_code == 423
    assert r.json()["detail"] == error_msg

    for data_type in ["primitives", "classes", "collections"]:
        url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/{data_type}"
        post_data = {
            "creator_id": str(scope_id),
            "creator_type": "scope",
        }
        if data_type == "primitives":
            post_data["type"] = "integer"
            post_data["value"] = 100
            post_data["name"] = f"{data_type} Name"
            post_data["description"] = f"{data_type} Description"
        if data_type == "classes":
            post_data["type"] = "class"
            post_data["value"] = "org.empaia.global.v1.classes.roi"
            post_data["reference_id"] = str(uuid4())
            post_data["reference_type"] = "annotation"
        if data_type == "collections":
            post_data["type"] = "collection"
            post_data["item_type"] = "point"

        r = requests.post(url, json=post_data, params=params, headers=scope_header)
        assert r.status_code == 423
        assert r.json()["detail"] == error_msg

    job_data = wbs_post_job_data(scope_id, "SCOPE")
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/jobs"
    r = requests.post(url, json=job_data, headers=scope_header)
    print(r.json())
    assert r.status_code == 423
    # error message is coming from examination service
    assert r.json()["detail"]["detail"] == "Examination has state 'CLOSED': job can not be added!"

    # test delete annotations or jobs
    error_msg = "Examination has state 'CLOSED': Create or delete actions are not allowed"

    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/annotations/{str(uuid4())}"
    params = {}
    r = requests.delete(url, params=params, headers=scope_header)
    assert r.status_code == 423
    assert r.json()["detail"] == error_msg

    for data_type in ["primitives", "classes", "collections"]:
        url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/{data_type}/{str(uuid4())}"

        r = requests.delete(url, params=params, headers=scope_header)
        assert r.status_code == 423
        assert r.json()["detail"] == error_msg

    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/jobs/{str(uuid4())}"
    r = requests.delete(url, headers=scope_header)
    assert r.status_code == 423
    assert r.json()["detail"] == error_msg
