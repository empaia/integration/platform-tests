import requests
from sample_apps.sample_apps.tutorial._TA05_classes import v3 as app

from ....global_services.data_generators.marketplace_service import utils as utils_mps
from ....global_services.data_generators.singletons import general_settings, login_manager
from ...data_generators.case_data import get_test_cases
from ...data_generators.medical_data_service import utils
from ..utils import create_scope, read_case


def test_collections_scope():
    patho_user_id = login_manager.patho_user()["user-id"]

    # read cases from file
    case_1 = read_case(get_test_cases, "WBS_MDS_CASES_v3", "TEST13v3", "v3")
    case_2 = read_case(get_test_cases, "WBS_MDS_CASES_v3", "TEST13Bv3", "v3")
    case_id_1 = case_1["case_id"]
    case_id_2 = case_2["case_id"]

    headers = login_manager.wbs_client()
    app_id_1 = utils_mps.get_app_id_by_ead(headers, app.ead)
    app_id_2 = utils_mps.get_app_id_by_ead(headers, app.ead)

    ex_id_1 = utils.add_or_get_examination_to_case(patho_user_id, case_id_1, app_id_1)
    ex_id_2 = utils.add_or_get_examination_to_case(patho_user_id, case_id_2, app_id_2)

    scope_id_1, scope_header_1 = create_scope(ex_id_1)
    scope_id_2, _ = create_scope(ex_id_2)

    # Post annotation
    point = {
        "name": "annotation_1",
        "type": "point",
        "coordinates": [10000, 20000],
        "npp_created": 5000,
        "npp_viewing": [1, 50000],
        "creator_id": scope_id_1,
        "creator_type": "scope",
        "reference_id": "some-id",
        "reference_type": "wsi",
    }
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/annotations"
    params = {}
    r = requests.post(url, json=point, params=params, headers=scope_header_1)
    assert r.status_code == 201
    annotation_id = r.json()["id"]

    # Post class for annotation
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/classes"
    post_class = {
        "type": "class",
        "value": "org.empaia.vendor_name.tutorial_app_05.v3.0.classes.tumor",
        "creator_id": str(scope_id_1),
        "creator_type": "scope",
        "reference_id": annotation_id,
        "reference_type": "annotation",
    }
    r = requests.post(url, json=post_class, headers=scope_header_1)
    assert r.status_code == 201

    # Get annotation
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/annotations/{annotation_id}"
    params = {
        "with_classes": "true",
    }
    r = requests.get(url, params=params, headers=scope_header_1)
    assert r.status_code == 200
    assert r.json()["classes"][0]["value"] == "org.empaia.vendor_name.tutorial_app_05.v3.0.classes.tumor"

    # Post collection
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/collections"
    point["name"] = "Annotation in collection"
    items = [point]
    collection = {
        "name": "Collection name",
        "description": "Collection description",
        "creator_id": str(scope_id_1),
        "creator_type": "scope",
        "reference_id": "wsi_id",
        "reference_type": "wsi",
        "type": "collection",
        "item_type": "point",
        "items": items,
    }
    r = requests.post(url, json=collection, headers=scope_header_1)
    assert r.status_code == 201
    assert len(r.json()["items"]) == 1
    collection_id = r.json()["id"]

    # Post collection, invalid item scope
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/collections"
    collection["items"][0]["creator_id"] = str(scope_id_2)
    r = requests.post(url, json=collection, headers=scope_header_1)
    assert r.status_code == 412
    assert r.json()["detail"] == "Creator_id must be set to scope_id"

    # Post collection without items
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/collections"
    collection["items"] = []
    r = requests.post(url, json=collection, headers=scope_header_1)
    assert r.status_code == 201

    # Post collection, invalid scope
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/collections"
    collection["creator_id"] = str(scope_id_2)
    r = requests.post(url, json=collection, headers=scope_header_1)
    assert r.status_code == 412
    assert r.json()["detail"] == "Creator_id must be set to scope_id"

    # Put collection item query
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/collections/{collection_id}/items/query"
    empty_query = {}
    r = requests.put(url, json=empty_query, headers=scope_header_1)
    assert r.status_code == 200

    # Put collection item query, invalid scope
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/collections/{collection_id}/items/query"
    r = requests.put(url, json=empty_query, headers=scope_header_1)
    assert r.status_code == 412

    # Put collection item query unique references
    url = (
        f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/collections/{collection_id}/items/"
        "query/unique-references"
    )
    r = requests.put(url, json=empty_query, headers=scope_header_1)
    assert r.status_code == 200

    # Put collection item query unique references, invalid scope
    url = (
        f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/collections/{collection_id}/items/"
        "query/unique-references"
    )
    r = requests.put(url, json=empty_query, headers=scope_header_1)
    assert r.status_code == 412

    # post single new item

    # post item
    point["creator_id"] = scope_id_1
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/collections/{collection_id}/items"
    r = requests.post(url, json=point, headers=scope_header_1)
    print(r.text)
    assert r.status_code == 201
    item_id = r.json()["id"]

    # post item, invalid scope
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/collections/{collection_id}/items"
    r = requests.post(url, json=point, headers=scope_header_1)
    assert r.status_code == 412

    # post item, invalid creator
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/collections/{collection_id}/items"
    p = point.copy()
    p["creator_id"] = scope_id_2
    r = requests.post(url, json=p, headers=scope_header_1)
    assert r.status_code == 412
    assert r.json()["detail"] == "Creator_id must be set to scope_id"

    # post list of new items

    points = {"items": [point, point]}

    # post items
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/collections/{collection_id}/items"
    r = requests.post(url, json=points, headers=scope_header_1)
    assert r.status_code == 201
    assert len(r.json()["items"]) == len(points["items"])

    # post items, invalid scope
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/collections/{collection_id}/items"
    r = requests.post(url, json=points, headers=scope_header_1)
    assert r.status_code == 412

    # post items, invalid creator
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/collections/{collection_id}/items"
    p = point.copy()
    p["creator_id"] = scope_id_2
    points = {"items": [p, p]}
    r = requests.post(url, json=p, headers=scope_header_1)
    assert r.status_code == 412
    assert r.json()["detail"] == "Creator_id must be set to scope_id"

    # post item by id

    # Post annotations
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/annotations"
    params = {}
    r = requests.post(url, json=point, params=params, headers=scope_header_1)
    assert r.status_code == 201
    point_id1 = r.json()["id"]
    r = requests.post(url, json=point, params=params, headers=scope_header_1)
    assert r.status_code == 201
    point_id2 = r.json()["id"]
    r = requests.post(url, json=point, params=params, headers=scope_header_1)
    assert r.status_code == 201
    point_id3 = r.json()["id"]

    item = {"id": point_id1}

    # post item
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/collections/{collection_id}/items"
    r = requests.post(url, json=item, headers=scope_header_1)
    assert r.status_code == 201

    # post item, invalid scope
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/collections/{collection_id}/items"
    r = requests.post(url, json=item, headers=scope_header_1)
    assert r.status_code == 412

    # post items by id

    items = {"items": [{"id": point_id2}, {"id": point_id3}]}

    # post items
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/collections/{collection_id}/items"
    r = requests.post(url, json=items, headers=scope_header_1)
    assert r.status_code == 201

    # post items, invalid scope
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/collections/{collection_id}/items"
    r = requests.post(url, json=items, headers=scope_header_1)
    assert r.status_code == 412

    # delete item

    # delete item, invalid scope
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_2}/collections/{collection_id}/items/{item_id}"
    r = requests.delete(url, headers=scope_header_1)
    assert r.status_code == 412

    # delete item
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id_1}/collections/{collection_id}/items/{item_id}"
    r = requests.delete(url, headers=scope_header_1)
    assert r.status_code == 200
    assert r.json()["id"] == item_id
