from pathlib import Path
from uuid import uuid4

import requests
from tusclient import exceptions

from ....global_services.data_generators.singletons import compose_settings, general_settings, login_manager
from ...data_generators.case_data import get_test_cases, get_test_wsis
from ...data_generators.commons import MDS_HOST_DOCKER_INTERNAL, MOUNTED_WSIS_ROOT_DIR
from ...data_generators.medical_data_service import utils_case, utils_idm, utils_storage, utils_wsi
from ...data_generators.upload_service import utils as utils_us
from ..utils import read_case


def test_upload_register_get_wsi_info():
    # ##########################
    # ## CDS ###################
    # ##########################

    # Read case
    case_id = read_case(get_test_cases, "US_MDS_CASES_v3", "TEST01v3", "v3")["case_id"]
    wsi = get_test_wsis()[0]

    # POST Slide
    headers = login_manager.mta_user()
    r = utils_case.mds_post_slide(headers, case_id, wsi["tissue"], wsi["stain"], wsi["block"])
    print("CD_Service - POST slide, status_code: ", r.status_code)
    print(r.content)
    assert r.status_code == 200
    slide_id = r.json()["id"]

    # ##########################
    # ## US ####################
    # ##########################
    slide_container_source_path = wsi["path"]
    slide_rel_path = slide_container_source_path.replace(MOUNTED_WSIS_ROOT_DIR, "").lstrip("/")

    extension = slide_container_source_path.split(".")[-1]
    upload_f_name = f"{slide_id}.{extension}"

    slide_container_target_path = f"{MOUNTED_WSIS_ROOT_DIR}/{upload_f_name}".replace("//", "/")
    source_path = f"{general_settings.script_path_to_wsis}/{slide_rel_path}".replace("//", "/")
    target_path = f"{compose_settings.host_path_to_wsis}/{upload_f_name}".replace("//", "/")
    assert Path(target_path).exists() is False

    try:
        upload_url = utils_us.us_upload_file(f_path=source_path, f_name=upload_f_name, upload_id=uuid4())

        # ##########################
        # ## IDM ###################
        # ##########################

        # POST IDM Slide
        local_id = (
            slide_container_source_path.split("/")[-2]
            + "_"
            + slide_container_source_path.split("/")[-1]
            + "_"
            + slide_id.split("-")[0]
        )
        headers = login_manager.mta_user()
        r = utils_idm.idm_post_slide(headers, MDS_HOST_DOCKER_INTERNAL, slide_id, local_id)
        print("IDM_Service - POST slide, status_code: ", r.status_code)
        print(r.json())
        assert r.status_code == 200

        # ##########################
        # ## Register Slide ########
        # ##########################

        # Register a slide
        headers = login_manager.mta_user()
        r = utils_storage.mds_register_wsi(headers, slide_id, main_path=slide_container_target_path)
        print("CD_Service - POST WSI, status_code: ", r.status_code)
        assert r.status_code == 200

        # Validate slide storage
        headers = login_manager.mta_user()
        r = utils_storage.mds_get_wsi_storage(headers, slide_id)
        print("CD_Service - GET WSI, status_code: ", r.status_code)
        assert r.status_code == 200

        # ##########################
        # ## WSI ###################
        # ##########################

        # just test get the slide
        headers = login_manager.mta_user()
        r = utils_wsi.mds_get_wsi_info(headers, slide_id)
        print("WSI_Service - GET Info, status_code: ", r.status_code)
        print("WSI_Service - GET Info, content: ", r.content)
        assert r.status_code == 200
        assert r.json()["id"] == slide_id
        slide_meta = r.json()
        pixel_size = slide_meta["pixel_size_nm"]
        _npp_ranges = [pixel_size["x"] * level["downsample_factor"] for level in slide_meta["levels"]]

        # ##########################
        # ## delete file on host ###
        # ##########################
        r = requests.delete(upload_url, headers=headers)
        print("US - DELETE route: ", r.content, r.status_code)
        print(r.content)
        r.raise_for_status()

    except exceptions.TusCommunicationError as e:
        print(e.response_content)
        # ##########################
        # ## delete fallback     ###
        # ##########################
        Path(target_path).unlink()
