import requests
from sample_apps.sample_apps.test.internal._ITA02_wsi_collection import v2 as appv2
from sample_apps.sample_apps.test.internal._ITA02_wsi_collection import v3 as appv3

from ....global_services.data_generators.marketplace_service.utils import get_app_id_by_ead
from ....global_services.data_generators.singletons import general_settings, login_manager
from ...data_generators.case_data import get_test_cases
from ..utils import read_case


def test_wbs_apps():
    # read case from file
    case = read_case(get_test_cases, "WBS_v1_v2_v3", "TEST01v3", "v3")
    case_id = case["case_id"]

    # app_ids
    appv2_id = get_app_id_by_ead(login_manager.wbs_client(), appv2.ead)
    appv3_id = get_app_id_by_ead(login_manager.wbs_client(), appv3.ead)

    # close examinations for all WBS APIs
    wbsv2_close_all_examinations(case_id)
    wbsv3_close_all_examinations(case_id)

    # new examination for v1 (and v2 = same ex)
    ex_v12 = wbsv2_new_examination(case_id).json()

    # make sure app select in examination v2 only shows the one correct version of _ITA02_wsi_collection
    apps_v2 = wbsv2__get_available_apps(case_id).json()
    check_apps(available_apps=apps_v2, expected_app_id=appv2_id, not_expected_app_ids=[appv3_id])

    # make sure app slect in v3 (no examination context) only shows v3 app
    apps_v3 = wbsv3_get_available_apps().json()
    check_apps(available_apps=apps_v3, expected_app_id=appv3_id, not_expected_app_ids=[appv2_id])

    # add app in v1 v2 to same ex
    wbsv2_add_app_to_ex(ex_v12["id"], appv2_id)

    # new ex with app v3 in v3
    ex_v3 = wbsv3_new_examination(case_id, appv3_id).json()

    # make sure examination in each v2 v3 only shows the one correct app
    apps_v2 = wbsv2_get_examination_apps(ex_v12["id"]).json()
    check_apps(available_apps=apps_v2, expected_app_id=appv2_id, not_expected_app_ids=[appv3_id])
    # there is definitely just one app inside v3 examination, but check nevertheless
    apps_v3 = {"items": [ex_v3["app"]]}
    check_apps(available_apps=apps_v3, expected_app_id=appv3_id, not_expected_app_ids=[appv2_id])

    # # close all examinations
    # wbsv2_close_all_examinations(case_id)
    # wbsv3_close_all_examinations(case_id)


def wbsv2_get_examination_apps(ex_id):
    url = f"{general_settings.wbs_host}v2/examinations/{ex_id}/apps"
    r = requests.get(url=url, headers=login_manager.patho_user())
    assert r.status_code == 200
    return r


def wbsv3_new_examination(case_id, app_id):
    url = f"{general_settings.wbs_host}v3/examinations"
    payload = {"case_id": case_id, "app_id": app_id}
    r = requests.put(url=url, headers=login_manager.patho_user(), json=payload)
    assert r.status_code == 201 or r.status_code == 200
    return r


def wbsv2_add_app_to_ex(ex_id, app_id):
    url = f"{general_settings.wbs_host}v2/examinations/{ex_id}/apps/{app_id}"
    r = requests.put(url=url, headers=login_manager.patho_user())
    print(r.text)
    assert r.status_code == 201 or r.status_code == 200
    return r


def check_apps(available_apps, expected_app_id, not_expected_app_ids):
    app_ids = []
    for app in available_apps["items"]:
        app_id = None
        if app.get("id"):  # v1 v2
            app_id = app.get("id")
        elif app.get("app_id"):  # v3
            app_id = app.get("app_id")
        app_ids.append(app_id)
    assert expected_app_id in app_ids
    for a_id in not_expected_app_ids:
        assert a_id not in app_ids


def wbsv2__get_available_apps(case_id):
    url = f"{general_settings.wbs_host}v2/cases/{case_id}/apps"
    r = requests.get(url=url, headers=login_manager.patho_user())
    assert r.status_code == 200
    return r


def wbsv3_get_available_apps():
    url = f"{general_settings.wbs_host}v3/apps/query"
    r = requests.put(url=url, headers=login_manager.patho_user(), json={})
    assert r.status_code == 200
    return r


def wbsv2_new_examination(case_id):
    url = f"{general_settings.wbs_host}v2/cases/{case_id}/examinations"
    r = requests.post(url=url, headers=login_manager.patho_user())
    assert r.status_code == 201
    return r


def wbsv2_close_all_examinations(case_id):
    url = f"{general_settings.wbs_host}v2/cases/{case_id}/examinations"
    r = requests.get(url=url, headers=login_manager.patho_user())
    assert r.status_code == 200
    for ex in r.json()["items"]:
        if ex["state"] == "OPEN":
            url = f"{general_settings.wbs_host}v2/examinations/{ex['id']}/close"
            r = requests.put(url=url, headers=login_manager.patho_user())
            print(r.text)
            assert r.status_code == 200


def wbsv3_close_all_examinations(case_id):
    url = f"{general_settings.wbs_host}v3/examinations/query"
    r = requests.put(url=url, headers=login_manager.patho_user(), json={"cases": [case_id]})
    assert r.status_code == 200
    for ex in r.json()["items"]:
        if ex["state"] == "OPEN":
            url = f"{general_settings.wbs_host}v3/examinations/{ex['id']}/close"
            r = requests.put(url=url, headers=login_manager.patho_user())
            assert r.status_code == 200
