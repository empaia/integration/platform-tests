import requests

from ....global_services.data_generators.singletons import general_settings


def mds_put_examination(headers, case_id, creator_id, app_id, creator_type):
    url = f"{general_settings.mds_host}v3/examinations"
    data = {
        "case_id": case_id,
        "app_id": app_id,
        "creator_id": creator_id,
        "creator_type": creator_type,
    }
    r = requests.put(url, json=data, headers=headers)
    return r


def mds_put_scope(headers, examination_id, user_id):
    url = f"{general_settings.mds_host}v3/scopes"
    data = {
        "examination_id": examination_id,
        "user_id": user_id,
    }
    r = requests.put(url, json=data, headers=headers)
    return r


def mds_get_scope_token(headers, scope_id):
    url = f"{general_settings.mds_host}v3/scopes/{scope_id}/token"
    r = requests.get(url, headers=headers)
    return r


def mds_close_examination(headers, ex_id):
    url = f"{general_settings.mds_host}v3/examinations/{ex_id}/state"
    data = {"state": "CLOSED"}
    r = requests.put(url, json=data, headers=headers)
    return r


def mds_put_job_to_exmaination(headers, examination_id, job_id):
    url = f"{general_settings.mds_host}v3/examinations/{examination_id}/jobs/{job_id}/add"
    r = requests.put(url, headers=headers)
    return r
