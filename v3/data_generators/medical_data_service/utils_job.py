import requests

from ....global_services.data_generators.singletons import general_settings


def delete_job(headers, job_id):
    url = f"{general_settings.mds_host}v3/jobs/{job_id}"
    r = requests.delete(url, headers=headers)
    return r


def post_job(headers, job):
    url = f"{general_settings.mds_host}v3/jobs"
    r = requests.post(url, json=job, headers=headers)
    return r


def get_job(headers, job_id):
    url = f"{general_settings.mds_host}v3/jobs/{job_id}"
    r = requests.get(url, headers=headers)
    return r


def get_token(headers, job_id):
    url = f"{general_settings.mds_host}v3/jobs/{str(job_id)}/token"
    r = requests.get(url, headers=headers)
    return r


def put_job_status(headers, job_id, status):
    url = f"{general_settings.mds_host}v3/jobs/{job_id}/status"
    data = {"status": status}
    r = requests.put(url, json=data, headers=headers)
    return r


def post_output_id_to_output(headers, job_id, output_param, output_id):
    data = {"id": output_id}
    url = f"{general_settings.mds_host}v3/jobs/{job_id}/outputs/{output_param}"
    r = requests.put(url, json=data, headers=headers)
    return r


def post_input_id_to_input(headers, job_id, input_param, input_id):
    data = {"id": input_id}
    url = f"{general_settings.mds_host}v3/jobs/{job_id}/inputs/{input_param}"
    r = requests.put(url, json=data, headers=headers)
    return r
