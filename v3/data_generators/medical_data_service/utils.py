import os
from pprint import pprint
from random import randint

import requests

from ....global_services.data_generators.singletons import general_settings, login_manager
from ..commons import DEFAULT_CASE_DESCRIPTION, MDS_HOST_DOCKER_INTERNAL
from . import utils_case, utils_dads, utils_examination, utils_hs, utils_idm, utils_job, utils_storage, utils_wsi


def ead_type_to_api_path(ead_type: str):
    if ead_type in ["point", "arrow", "line", "rectangle", "polygon", "circle"]:
        return "annotations"
    elif ead_type in ["integer", "float", "bool", "string"]:
        return "primitives"
    elif ead_type == "collection":
        return "collections"
    elif ead_type == "class":
        return "classes"
    elif ead_type == "wsi":
        return "slides"
    else:
        raise TypeError(f"Invalid Type: {ead_type}")


def add_or_get_examination_to_case(
    user_id,
    case_id,
    app_id,
    creator_type="USER",
    return_status=False,
):
    headers = login_manager.wbs_client()
    r = utils_examination.mds_put_examination(headers, case_id, user_id, app_id, creator_type=creator_type)
    print("Ex_Service - POST examination, status_code: ", r.status_code)
    pprint(r.json())
    assert r.status_code == 201 or r.status_code == 200
    ex_id = r.json()["id"]
    if return_status:
        return ex_id, r.status_code
    return ex_id


def new_case_with_slides(
    user_id: str,
    wsis: dict,
    local_case_id: str,
    description: str = DEFAULT_CASE_DESCRIPTION,
    indication: str = None,
    procedure: str = None,
    local_slide_ids: list = None,
    case_id=None,
):
    ###########################
    # CDS #####################
    ###########################

    if not case_id:
        # POST case
        headers = login_manager.wbs_client()
        r = utils_case.mds_post_case(headers, user_id, description, indication, procedure)
        print("CD_Service - POST case, status_code: ", r.status_code)
        pprint(r.json())
        assert r.status_code == 200
        case_id = r.json()["id"]

        # GET Case
        headers = login_manager.wbs_client()
        r = utils_case.mds_get_case(headers, case_id)
        print("CD_Service - GET case, status_code: ", r.status_code)
        assert r.status_code == 200

        # POST IDM Case
        headers = login_manager.wbs_client()
        r = utils_idm.idm_post_case(headers, MDS_HOST_DOCKER_INTERNAL, case_id, local_case_id)
        print("IDM_Service - POST case, status_code: ", r.status_code)
        pprint(r.json())
        assert r.status_code == 200

    slide_ids = []
    npp_ranges = []

    for idx, wsi in enumerate(wsis):
        address = wsi["path"]
        print(f"Processing slide at: {wsi['path']}")
        # POST Slide
        headers = login_manager.wbs_client()
        r = utils_case.mds_post_slide(
            headers, case_id, wsi.get("tissue"), wsi.get("stain"), wsi.get("block"), main_path=address
        )
        print("CD_Service - POST slide, status_code: ", r.status_code)
        assert r.status_code == 200
        slide_id = r.json()["id"]

        slide_ids.append(slide_id)

        # POST IDM Slide
        if isinstance(local_slide_ids, list):
            local_id = local_slide_ids[idx]
        else:
            local_id = address.split("/")[-2] + "_" + address.split("/")[-1] + "_" + slide_id.split("-")[0]
        local_id = local_id[-49:]
        headers = login_manager.wbs_client()
        r = utils_idm.idm_post_slide(headers, MDS_HOST_DOCKER_INTERNAL, slide_id, local_id)
        print("IDM_Service - POST slide, status_code: ", r.status_code)
        pprint(r.json())
        assert r.status_code == 200

        ###########################
        # Slide Storage ###########
        ###########################

        # validate get slide storage
        headers = login_manager.wbs_client()
        r = utils_storage.mds_get_wsi_storage(headers, slide_id)
        print("Storage_Service - GET WSI, status_code: ", r.status_code)
        assert r.status_code == 200

        ###########################
        # WSI #####################
        ###########################

        # just test get the slide
        headers = login_manager.wbs_client()
        r = utils_wsi.mds_get_wsi_info(headers, slide_id)
        print("WSI_Service - GET Info, status_code: ", r.status_code)
        print("WSI_Service - GET Info, content: ", r.content)
        assert r.status_code == 200
        pprint(r.json())
        pprint(slide_id)
        assert r.json()["id"] == slide_id
        slide_meta = r.json()
        pixel_size = slide_meta["pixel_size_nm"]
        npp_ranges_ = [pixel_size["x"] * levels["downsample_factor"] for levels in slide_meta["levels"]]
        npp_ranges = npp_ranges_

    return case_id, slide_ids, npp_ranges


def create_questionnaire(questionnaire):
    ###########################
    # Harpy Service ###########
    ###########################

    if questionnaire:
        # POST Questionnaire
        r = utils_hs.mds_post_questionnaire(questionnaire)
        print("Harpy_Service - POST Questionnaire, status_code: ", r.status_code)
        pprint(r.json())
        assert r.status_code == 201
        logical_id = r.json()["id"]
        version_id = r.json()["meta"]["versionId"]

        # GET Questionnaire
        r = utils_hs.mds_get_questionnaire(logical_id, version_id)
        print("Harpy_Service - GET Questionnaire, status_code: ", r.status_code)
        pprint(r.json())
        assert r.status_code == 200

        return logical_id


def create_selector(logical_id, selector):
    ###########################
    # Harpy Service ###########
    ###########################

    if logical_id and selector:
        # POST Selector
        r = utils_hs.mds_post_selector(logical_id, selector)
        print("Harpy_Service - POST Selector, status_code: ", r.status_code)
        pprint(r.json())

        if r.status_code == 200:
            # GET Selector
            selector_id = r.json()["id"]
            r = utils_hs.mds_get_selector(selector_id)
            print("Harpy_Service - GET Selector, status_code: ", r.status_code)
            pprint(r.json())
            assert r.status_code == 200


def create_selector_taggings(selector, indication, procedure):
    ###########################
    # Harpy Service ###########
    ###########################

    if selector and indication and procedure:
        # POST selector-taggings
        r = utils_hs.mds_post_selector_taggings(selector, indication, procedure)
        print("Harpy_Service - POST Selector-Taggings, status_code: ", r.status_code)
        selector_tagging = r.json()

        if r.status_code == 201:
            assert selector_tagging["selector_value"] == selector
            assert selector_tagging["indication"] == indication
            assert selector_tagging["procedure"] == procedure


def lock_to_job(
    job_id,
    params,
):
    for p in params:
        p_id = p[1]
        p_api_path = ead_type_to_api_path(p[0])
        headers = login_manager.wbs_client()
        r = utils_dads.mds_lock_any(headers, p_api_path, p_id, job_id)
        print("locking...type/id/job_id: ", p_api_path, p_id, job_id)
        print("DAD_Service - Lock, status_code: ", r.status_code)
        pprint(r.json())
        assert r.status_code == 200


def create_job(ead, app_id, creator_id, ex_id, creator_type="USER", mode="STANDALONE"):
    in_job = {"app_id": app_id, "creator_id": creator_id, "creator_type": creator_type, "mode": mode}
    # POST job
    headers = login_manager.wbs_client()
    r = utils_job.post_job(headers, in_job)
    print("J_Service - POST job, status_code: ", r.status_code)
    print(r.text)
    assert r.status_code == 200
    job_id = r.json()["id"]

    # GET job token
    headers = login_manager.wbs_client()
    r = utils_job.get_token(headers, job_id)
    print("J_Service - GET token, status_code: ", r.status_code)
    assert r.status_code == 200
    token = r.json()["access_token"]

    headers = login_manager.wbs_client()
    r = utils_examination.mds_put_job_to_exmaination(headers, ex_id, job_id)
    print("Ex_Service - POST add job to examination, status_code: ", r.status_code)
    print(r.text)
    assert r.status_code == 201 or r.status_code == 200  # new created  # new already exists

    return in_job, job_id, token


def delete_job(job_id):
    headers = login_manager.wbs_client()
    r = utils_job.delete_job(headers, job_id)
    print("J_Service - DELETE job, status_code: ", r.status_code)
    assert r.status_code == 200
    data = r.json()
    assert job_id == data["id"]
    return job_id


def set_js_job_statuses(job_id, statuses, expected_status_code=200):
    for status in statuses:
        headers = login_manager.wbs_client()
        r = utils_job.put_job_status(headers, job_id, status)
        print(f"J_Service - PUT job status [{status}], status_code: ", r.status_code)
        assert r.status_code == expected_status_code


def add_outputs_to_job(
    job_id,
    outputs,
):
    for out_k in outputs:
        out_v = outputs[out_k]
        headers = login_manager.wbs_client()
        r = utils_job.post_output_id_to_output(headers, job_id, out_k, out_v["id"])
        print("J_Service - PUT job add output-ID, status_code: ", r.content, r.status_code)
        assert r.status_code == 200

    headers = login_manager.wbs_client()
    r = utils_job.get_job(headers, job_id)
    print("J_Service - GET job, status_code: ", r.status_code)
    assert r.status_code == 200
    out_job = r.json()

    return out_job


def run_job(job_id, scope_id, scope_headers):
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/jobs/{job_id}/run"
    r = requests.put(url, headers=scope_headers)
    return r


def get_job(job_id):
    headers = login_manager.wbs_client()
    r = utils_job.get_job(headers, job_id)
    print("J_Service - GET job, status_code: ", r.status_code)
    assert r.status_code == 200
    job = r.json()
    return job


def rn_coords(x_min, y_min, x_max, y_max):
    x = randint(x_min, x_max)
    y = randint(y_min, y_max)
    return (x, y)


def delete_temp_file(filename):
    if os.path.exists(filename):
        os.remove(filename)


def simulate_dmc_delete_slide(slide_id):
    headers = login_manager.wbs_client()
    print(f"Deleting slide with id: {slide_id}")
    r = utils_storage.mds_delete_slide_storage(headers, slide_id)
    print("Storage - DELETE slide, status_code: ", r.status_code)
    assert r.status_code == 200
    utils_idm.idm_delete_slide(headers, slide_id)
    print("IDMS - DELETE slide, status_code: ", r.status_code)
    assert r.status_code == 200
    utils_case.mds_delete_slide(headers, slide_id)
    print("CDS - DELETE slide, status_code: ", r.status_code)
    assert r.status_code == 200


def simulate_dmc_delete_case(case_id):
    headers = login_manager.wbs_client()
    r = utils_case.mds_get_case(headers, case_id, with_slides=True)
    for s in r.json()["slides"]:
        simulate_dmc_delete_slide(s["id"])
    r = utils_case.mds_delete_case(headers, case_id)
    print("CDS - DELETE case, status_code: ", r.status_code)
    assert r.status_code == 200
