import requests

from ....global_services.data_generators.singletons import general_settings


def mds_post_case(headers, creator_id, description, indication, procedure):
    url = f"{general_settings.mds_host}private/v3/cases"
    data = {
        "creator_id": creator_id,
        "creator_type": "USER",
        "description": description,
        "indication": indication,
        "procedure": procedure,
    }
    r = requests.post(url, json=data, headers=headers)
    return r


def mds_get_case(headers, case_id, with_slides=False):
    url = f"{general_settings.mds_host}v3/cases/{case_id}"
    params = {}
    if with_slides:
        params["with_slides"] = True
    r = requests.get(url, headers=headers, params=params)
    return r


def mds_get_cases(headers, skip, limit):
    url = f"{general_settings.mds_host}v3/cases"
    params = {
        "skip": skip,
        "limit": limit,
    }
    r = requests.get(url, params=params, headers=headers)
    return r


def mds_post_slide(headers, case_id, tissue, stain, block, main_path: str = None):
    # POST Slide
    url = f"{general_settings.mds_host}private/v3/slides"
    data = {
        "case_id": case_id,
        "tissue": tissue,
        "stain": stain,
        "block": block,
        "main_path": main_path,
    }
    r = requests.post(url, json=data, headers=headers)
    return r


def mds_get_slide(headers, slide_id):
    url = f"{general_settings.mds_host}v3/slides/{slide_id}"
    r = requests.get(url, headers=headers)
    return r


def mds_delete_case(headers, case_id):
    url = f"{general_settings.mds_host}v3/cases/{case_id}"
    payload = {"deleted": True}
    r = requests.put(url, headers=headers, json=payload)
    return r


def mds_delete_slide(headers, slide_id):
    url = f"{general_settings.mds_host}v3/slides/{slide_id}"
    payload = {"deleted": True}
    r = requests.put(url, headers=headers, json=payload)
    return r
