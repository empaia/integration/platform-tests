import requests

from ....global_services.data_generators.singletons import general_settings


def mds_post_questionnaire(questionnaire):
    url = f"{general_settings.mds_host}v3/fhir/questionnaires"
    r = requests.post(url, json=questionnaire)
    return r


def mds_get_questionnaire(logical_id, version_id):
    url = f"{general_settings.mds_host}v3/fhir/questionnaires/{logical_id}/history/{version_id}"
    r = requests.get(url)
    return r


def mds_post_selector(logical_id, selector):
    url = f"{general_settings.mds_host}v3/fhir/selectors"
    payload = {"type": "Questionnaire", "logical_id": logical_id, "selector_value": selector}
    r = requests.post(url, json=payload)
    return r


def mds_get_selector(selector_id):
    url = f"{general_settings.mds_host}v3/fhir/selectors/{selector_id}"
    r = requests.get(url)
    return r


def mds_post_selector_taggings(selector, indication, procedure):
    url = f"{general_settings.mds_host}v3/fhir/selector-taggings"
    payload = {"type": "Questionnaire", "selector_value": selector, "indication": indication, "procedure": procedure}
    r = requests.post(url, json=payload)
    return r
