import argparse
from pathlib import Path

from .case_generators import generate_cases, generate_questionnaire_data, get_case_data, get_wsi_data

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--cases-file", help="Path to Cases JSON Definition File.")
    parser.add_argument("--wsis-file", help="Path to WSIs JSON Definition File.")
    parser.add_argument("--questionnaires-file", help="Path to FHIR Questionnaires JSON Definition File.")
    args = parser.parse_args()

    cases_file = Path(args.cases_file).expanduser()
    wsis_file = Path(args.wsis_file).expanduser()
    questionnaires_file = Path(args.questionnaires_file).expanduser()
    cases = get_case_data(cases_file)
    wsis = get_wsi_data(wsis_file)
    generate_questionnaire_data(questionnaires_file)
    generate_cases(cases, wsis)
