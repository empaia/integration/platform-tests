import errno
import json
import math
import os
from pathlib import Path, PurePath

from ....global_services.data_generators.singletons import login_manager
from ..commons import DEFAULT_CASE_DESCRIPTION, MOUNTED_WSIS_ROOT_DIR
from . import utils


def get_case_data(file_name):
    cases_path = Path(file_name)
    with open(cases_path, encoding="UTF-8") as f:
        data = json.load(f)
        for api_version in data:
            api_data = data[api_version]
            for case_group in api_data:
                print(case_group)
                for case in api_data[case_group]:
                    case_data = api_data[case_group][case]
                    print(case_data)
                    case_data["path"] = f"data/{case_group}/{case_data['name'].replace(' ', '_')}.json"
        return data


def get_wsi_data(file_name):
    wsis_path = Path(file_name)
    with open(wsis_path, encoding="UTF-8") as f:
        wsis = json.load(f)
        for wsi in wsis:
            mounted_wsis_dir = MOUNTED_WSIS_ROOT_DIR.rstrip("/")
            wsi["path"] = f"{mounted_wsis_dir}/{wsi['path']}"
        return wsis


def _case_with_n_slides(
    case_alias,
    slide_storage_adress,
    n_slides=20,
    slide_list=None,
    description=DEFAULT_CASE_DESCRIPTION,
    indication=None,
    procedure=None,
    local_slide_id_prefix=None,
):
    headers = login_manager.mta_user()
    user_id = headers["user-id"]

    if slide_list is None and n_slides is None:
        raise ValueError("No WSI in Case. Either slides or n_slides must be provided!")

    slides = []
    if slide_list is not None:
        slides = [slide_storage_adress[index] for index in slide_list]
    else:
        for _i in range(math.ceil(n_slides / len(slide_storage_adress))):
            slides += slide_storage_adress
        slides = slides[0:n_slides]

    local_slide_ids = None
    if local_slide_id_prefix is not None:
        local_slide_ids = [f"{local_slide_id_prefix}{i}" for i in range(n_slides)]

    case_id, slide_ids, npp_ranges = utils.new_case_with_slides(
        user_id, slides, case_alias, description, indication, procedure, local_slide_ids
    )
    return case_id, slide_ids, npp_ranges


def _persist_case(case_file_name, case_id, slide_ids, npp_ranges):
    path = PurePath(f"{Path.cwd()}/{case_file_name}")
    case = {"case_id": case_id, "slide_ids": slide_ids, "npp_ranges": npp_ranges}
    if not os.path.exists(os.path.dirname(path)):
        try:
            os.makedirs(os.path.dirname(path))
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise
    with open(path, mode="w", encoding="UTF-8") as f:
        json.dump(case, f, indent=4)


def generate_cases(cases, wsis, only_mds=False):
    for api_version in cases:
        api_cases = cases[api_version]
        for case_group in api_cases:
            if only_mds:
                if case_group != "MDS_CASES":
                    continue
            group_cases = api_cases[case_group]
            for case in group_cases:
                case_data = api_cases[case_group][case]
                description = case_data["description"] if "description" in case_data else DEFAULT_CASE_DESCRIPTION
                local_slide_id_prefix = (
                    case_data["local_slide_id_prefix"] if "local_slide_id_prefix" in case_data else None
                )
                n_slides = case_data["n_slides"] if "n_slides" in case_data else None
                indication = case_data["indication"] if "indication" in case_data else None
                procedure = case_data["procedure"] if "procedure" in case_data else None
                slide_list = case_data["slides"] if "slides" in case_data else None
                case_id, slide_ids, npp_ranges = _case_with_n_slides(
                    case_data["name"],
                    wsis,
                    n_slides=n_slides,
                    slide_list=slide_list,
                    description=description,
                    indication=indication,
                    procedure=procedure,
                    local_slide_id_prefix=local_slide_id_prefix,
                )
                _persist_case(
                    case_data["path"],
                    case_id,
                    slide_ids,
                    npp_ranges,
                )
                if "deleted_case" in case_data:
                    if case_data["deleted_case"]:
                        utils.simulate_dmc_delete_case(case_id)
                        continue
                if "deleted_slides" in case_data:
                    for s_id in case_data["deleted_slides"]:
                        utils.simulate_dmc_delete_slide(slide_ids[s_id])


def generate_questionnaire_data(file_name):
    questionnaires_path = Path(file_name)
    with open(questionnaires_path, encoding="UTF-8") as f:
        questionnaire_sections = json.load(f)
        for questionnaire_section in questionnaire_sections:
            questionnaire_path = f"{os.getcwd()}/{questionnaire_section['path']}"
            with open(questionnaire_path, encoding="UTF-8") as resource:
                logical_id = utils.create_questionnaire(json.load(resource))
                selector = questionnaire_section["selector"]
                utils.create_selector(logical_id, selector)
                utils.create_selector_taggings(
                    selector, questionnaire_section["indication"], questionnaire_section["procedure"]
                )
