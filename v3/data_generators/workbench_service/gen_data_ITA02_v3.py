from copy import deepcopy

import requests

from ....global_services.data_generators.singletons import general_settings, login_manager
from ..workbench_service import utils


def _gen_inputs(
    ead,
    scope_id,
    scope_headers,
    slide_ids,
):
    inputs_by_key = {}
    inputs_type_id = []
    ead = deepcopy(ead)

    # my_wsi
    inputs_by_key["my_wsi"] = ead["io"]["my_wsi"]
    inputs_by_key["my_wsi"]["id"] = slide_ids[0]
    inputs_type_id.append(("wsi", slide_ids[0]))

    # my_wsis_1
    items_simple_col = []
    items_simple_col.append({"id": slide_ids[1]})
    items_simple_col.append({"id": slide_ids[2]})

    data = utils.generate_collection_json(None, None, scope_id, "my_wsis_1", "scope", "wsi", items_simple_col)
    r = utils.wbs_post_collection(scope_headers, scope_id, data)
    print("WBS - POST simple Collection, status_code: ", r.status_code)
    assert r.status_code == 201
    col_result = r.json()
    inputs_by_key["my_wsis_1"] = col_result
    inputs_type_id.append(("collection", col_result["id"]))

    # my_wsis_2
    items_col_col = []
    items_simple_col = []
    items_simple_col.append({"id": slide_ids[3]})
    items_simple_col.append({"id": slide_ids[4]})
    data = utils.generate_collection_json(None, None, scope_id, "my_wsis_2_1", "scope", "wsi", items_simple_col)
    items_col_col.append(data)
    items_simple_col = []
    items_simple_col.append({"id": slide_ids[5]})
    items_simple_col.append({"id": slide_ids[6]})
    data = utils.generate_collection_json(None, None, scope_id, "my_wsis_2_2", "scope", "wsi", items_simple_col)
    items_col_col.append(data)

    data = utils.generate_collection_json(None, None, scope_id, "my_wsis_2", "scope", "collection", items_col_col)
    r = utils.wbs_post_collection(scope_headers, scope_id, data)
    print("WBS - POST Collection of Collection, status_code: ", r.status_code)
    assert r.status_code == 201
    col_col_result = r.json()
    inputs_by_key["my_wsis_2"] = col_col_result
    inputs_type_id.append(("collection", col_col_result["id"]))

    return inputs_by_key, inputs_type_id


def check_containerized_app_output(job_id):
    """
    Checks DADS output :
    - (1) this [_gen_inputs] are used as inputs
    - (2) the containtized app was executed using (1)
    """
    url = f"{general_settings.mds_host}v3/primitives/query"
    query = {"jobs": [job_id]}
    headers = login_manager.wbs_client()
    r = requests.put(url, json=query, headers=headers)
    print("MDS - PUT primitives query: ", r.status_code)
    assert r.status_code == 200
    assert r.json()["item_count"] == 1
    assert r.json()["items"][0]["value"] == 3
