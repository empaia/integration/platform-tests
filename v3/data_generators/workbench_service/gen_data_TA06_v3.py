from copy import deepcopy

import requests
from sample_apps.sample_apps.tutorial._TA06_large_collections import v3 as app

from ....global_services.data_generators.marketplace_service import utils as utils_mps
from ....global_services.data_generators.singletons import general_settings, login_manager
from ..medical_data_service import utils as mds_utils
from ..workbench_service import utils

# Constants
WSI_X_MAX = 100000
WSI_Y_MAX = 100000
RECTS = [  # upper left of my_rectangles
    [100, 1000],
    [2000, 5000],
    [7000, 9000],
]
RECTSW = 1600
RECTSH = 900


def _gen_inputs(ead, scope_id, scope_headers, slide_id, npp_ranges):
    inputs_by_key = {}
    inputs_type_id = []
    ead = deepcopy(ead)

    # my_wsi
    inputs_by_key["my_wsi"] = ead["io"]["my_wsi"]
    inputs_by_key["my_wsi"]["id"] = slide_id
    inputs_type_id.append(("wsi", slide_id))

    # my_rectangles
    items = []
    items.append(
        utils.generate_rectangle_json(
            slide_id,
            scope_id,
            "my_rectangle",
            "scope",
            [RECTS[0][0], RECTS[0][1]],
            RECTSW,
            RECTSH,
            npp_ranges[-1],
            [npp_ranges[0], npp_ranges[-1]],
        )
    )
    items.append(
        utils.generate_rectangle_json(
            slide_id,
            scope_id,
            "my_rectangle",
            "scope",
            [RECTS[1][0], RECTS[1][1]],
            RECTSW,
            RECTSH,
            npp_ranges[-1],
            [npp_ranges[0], npp_ranges[-1]],
        )
    )
    items.append(
        utils.generate_rectangle_json(
            slide_id,
            scope_id,
            "my_rectangle",
            "scope",
            [RECTS[2][0], RECTS[2][1]],
            RECTSW,
            RECTSH,
            npp_ranges[-1],
            [npp_ranges[0], npp_ranges[-1]],
        )
    )
    data = utils.generate_collection_json(None, None, scope_id, "my_rectangles", "scope", "rectangle", items)
    r = utils.wbs_post_collection(scope_headers, scope_id, data)
    print("DAD_Service - POST Collection, status_code: ", r.status_code)
    assert r.status_code == 201
    inputs_by_key["my_rectangles"] = r.json()
    inputs_type_id.append(("collection", r.json()["id"]))

    return inputs_by_key, inputs_type_id


def generate(
    scope_id,
    scope_headers,
    slide_id,
    ex_id,
    case_id,
    npp_ranges,
    set_job_ready=False,
):
    ead = app.ead
    headers = login_manager.wbs_client()
    app_id = utils_mps.get_app_id_by_ead(headers, ead)

    inputs_by_key, inputs_type_id = _gen_inputs(ead, scope_id, scope_headers, slide_id, npp_ranges)
    _, job_id, _ = mds_utils.create_job(ead, app_id, scope_id, ex_id)
    utils.add_inputs_to_job(job_id, inputs_by_key, scope_id, scope_headers)
    utils.lock_to_job(job_id, inputs_type_id, case_id)
    job_status = "ERROR"
    if set_job_ready:
        job_status = "READY"
    mds_utils.set_js_job_statuses(job_id, [job_status])
    job = mds_utils.get_job(job_id)
    return job, job_id


def check_containerized_app_output(job_id):
    """
    Checks DADS output :
    - (1) this [_gen_inputs] are used as inputs
    - (2) the containtized app was executed using (1)
    """
    url = f"{general_settings.mds_host}v3/annotations/query"
    query = {"jobs": [job_id], "creators": [job_id]}
    params = {"with_classes": True, "limit": 0}
    headers = login_manager.wbs_client()
    r = requests.put(url, json=query, headers=headers, params=params)
    print("MDS - PUT annotations query: ", r.status_code)
    assert r.status_code == 200
    assert r.json()["item_count"] == 12345 * 3
