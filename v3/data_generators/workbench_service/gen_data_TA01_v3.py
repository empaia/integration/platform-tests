from copy import deepcopy

import requests
from sample_apps.sample_apps.tutorial._TA01_simple_app import v3 as app

from ....global_services.data_generators.marketplace_service import utils as utils_mps
from ....global_services.data_generators.singletons import general_settings, login_manager
from ..medical_data_service import utils as mds_utils
from ..workbench_service import utils

# Constants
WSI_X_MAX = 100000
WSI_Y_MAX = 100000
RECTS = [  # upper left of my_rectangles
    [100, 1000],
    [2000, 5000],
    [7000, 9000],
]
RECTSW = 1600
RECTSH = 900

BASE_RES = 250
RECTS_MPP_C = BASE_RES * 2**4
RECTS_MPP_V = [BASE_RES * 2**2, BASE_RES * 2**8]


def _gen_inputs(
    ead,
    scope_id,
    scope_headers,
    slide_id,
):
    inputs_by_key = {}
    inputs_type_id = []
    ead = deepcopy(ead)

    # my_wsi
    inputs_by_key["my_wsi"] = ead["io"]["my_wsi"]
    inputs_by_key["my_wsi"]["id"] = slide_id
    inputs_type_id.append(("wsi", slide_id))

    # my_rectangle
    data = utils.generate_rectangle_json(
        slide_id, scope_id, "my_rectangle", "scope", [3000, 3000], RECTSW, RECTSH, RECTS_MPP_C, RECTS_MPP_V
    )
    r = utils.wbs_post_annotation(scope_headers, scope_id, data)
    print("WBS - POST Recangle, status_code: ", r.status_code)
    assert r.status_code == 201
    inputs_by_key["my_rectangle"] = r.json()
    inputs_type_id.append(("rectangle", r.json()["id"]))

    return inputs_by_key, inputs_type_id


def generate(
    scope_id,
    scope_headers,
    slide_id,
    ex_id,
    case_id,
    set_job_ready=False,
):
    ead = app.ead
    headers = login_manager.wbs_client()
    app_id = utils_mps.get_app_id_by_ead(headers, ead)

    inputs_by_key, inputs_type_id = _gen_inputs(ead, scope_id, scope_headers, slide_id)
    _, job_id, _ = mds_utils.create_job(ead, app_id, scope_id, ex_id)
    utils.add_inputs_to_job(job_id, inputs_by_key, scope_id, scope_headers)
    utils.lock_to_job(job_id, inputs_type_id, case_id)
    job_status = "ERROR"
    if set_job_ready:
        job_status = "READY"
    mds_utils.set_js_job_statuses(job_id, [job_status])
    job = mds_utils.get_job(job_id)
    return job, job_id


def check_containerized_app_output(job_id):
    """
    Checks DADS output :
    - (1) this [_gen_inputs] are used as inputs
    - (2) the containtized app was executed using (1)
    """
    url = f"{general_settings.mds_host}v3/primitives/query"
    query = {"jobs": [job_id]}
    headers = login_manager.wbs_client()
    r = requests.put(url, json=query, headers=headers)
    print("MDS - PUT primitives query: ", r.status_code)
    assert r.status_code == 200
    assert r.json()["item_count"] == 1
    assert r.json()["items"][0]["value"] == 42
