from pprint import pprint

import requests

from ....global_services.data_generators.singletons import case_separation, general_settings, login_manager
from ..medical_data_service import utils as ut
from ..medical_data_service import utils_job


def generate_point_json(
    reference_id,
    creator_id,
    name="my_point",
    creator_type="scope",
    coordinates=[1000, 2000],
    npp_created=250,
    npp_viewing=[250, 250 * 2**3],
):
    data = {
        "name": name,
        "type": "point",
        "coordinates": coordinates,
        "reference_id": reference_id,
        "reference_type": "wsi",
        "creator_id": creator_id,
        "creator_type": creator_type,
        "npp_created": npp_created,
        "npp_viewing": npp_viewing,
    }
    return data


def generate_rectangle_json(
    reference_id,
    creator_id,
    name="my_rectangle",
    creator_type="scope",
    upper_left=[1000, 2000],
    width=300,
    height=500,
    npp_created=250,
    npp_viewing=[250, 250 * 2**3],
):
    data = {
        "name": name,
        "type": "rectangle",
        "upper_left": upper_left,
        "width": width,
        "height": height,
        "reference_id": reference_id,
        "reference_type": "wsi",
        "creator_id": creator_id,
        "creator_type": creator_type,
        "npp_created": npp_created,
        "npp_viewing": npp_viewing,
    }
    return data


def generate_collection_json(
    reference_id, reference_type, creator_id, name, creator_type="scope", item_type="rectangle", items=[]
):
    data = {
        "name": name,
        "creator_id": creator_id,
        "creator_type": creator_type,
        "item_type": item_type,
        "type": "collection",
    }
    if items is not None and len(items) > 0:
        data["items"] = items
    if reference_id is not None:
        data["reference_id"] = reference_id
        data["reference_type"] = reference_type
    return data


def generate_class_json(
    reference_id, creator_id, creator_type="scope", value="org.empaia.my_vendor.my_app.v3.classes.tumor"
):
    data = {
        "value": value,
        "reference_id": reference_id,
        "reference_type": "annotation",
        "creator_id": creator_id,
        "creator_type": creator_type,
        "type": "class",
    }
    return data


def wbs_post_annotation(headers, scope_id, data):
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/annotations"
    r = requests.post(url, json=data, headers=headers)
    return r


def wbs_post_class(headers, scope_id, data):
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/classes"
    r = requests.post(url, json=data, headers=headers)
    return r


def wbs_post_collection(headers, scope_id, data):
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/collections"
    r = requests.post(url, json=data, headers=headers)
    return r


def add_inputs_to_job(
    job_id,
    inputs,
    scope_id,
    scope_headers,
):
    for in_k in inputs:
        in_v = inputs[in_k]
        r = post_input_id_to_input(scope_headers, job_id, in_k, in_v["id"], scope_id)
        print("J_Service - PUT job add input-ID, status_code: ", r.status_code)
        pprint(r.json())
        assert r.status_code == 200

    headers = login_manager.wbs_client()
    r = utils_job.get_job(headers, job_id)
    print("J_Service - GET job, status_code: ", r.status_code)
    assert r.status_code == 200
    job = r.json()
    return job


def lock_to_job(
    job_id,
    params,
    case_id,
):
    for p in params:
        p_id = p[1]
        p_api_path = ut.ead_type_to_api_path(p[0])
        headers = login_manager.wbs_client()
        r = mds_lock_any_with_case(headers, p_api_path, p_id, case_id, job_id)
        print("locking...type/id/job_id: ", p_api_path, p_id, job_id)
        print("DAD_Service - Lock, status_code: ", r.status_code)
        pprint(r.json())
        assert r.status_code == 200


def run_job(job_id, scope_id, scope_headers):
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/jobs/{job_id}/run"
    r = requests.put(url, headers=scope_headers)
    return r


def post_input_id_to_input(headers, job_id, input_param, input_id, scope_id):
    data = {"id": input_id}
    url = f"{general_settings.wbs_host}v3/scopes/{scope_id}/jobs/{job_id}/inputs/{input_param}"
    r = requests.put(url, json=data, headers=headers)
    return r


def mds_lock_any_with_case(headers, type_to_lock, id_to_lock, case_id, job_id):
    if case_separation.enable_annot_case_data_partitioning:
        headers["case-id"] = case_id
    url = f"{general_settings.mds_host}v3/jobs/{job_id}/lock/{type_to_lock}/{id_to_lock}"
    r = requests.put(url, json={}, headers=headers)
    return r
