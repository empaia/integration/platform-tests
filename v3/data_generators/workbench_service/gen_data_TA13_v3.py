from copy import deepcopy


def _gen_inputs_preprocessing(
    ead,
    slide_id,
):
    inputs_by_key = {}
    inputs_type_id = []
    ead = deepcopy(ead)

    # my_wsi
    inputs_by_key["input_wsi"] = ead["io"]["input_wsi"]
    inputs_by_key["input_wsi"]["id"] = slide_id
    inputs_type_id.append(("wsi", slide_id))

    return inputs_by_key, inputs_type_id
