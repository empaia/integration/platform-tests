from copy import deepcopy

import requests

from ....global_services.data_generators.singletons import general_settings, login_manager
from ..workbench_service import utils

# Constants
WSI_X_MAX = 100000
WSI_Y_MAX = 100000
RECTS = [  # upper left of my_rectangles
    [100, 1000],
    [2000, 5000],
    [7000, 9000],
]
RECTSW = 1600
RECTSH = 900

BASE_RES = 250
RECTS_MPP_C = BASE_RES * 2**4
RECTS_MPP_V = [BASE_RES * 2**2, BASE_RES * 2**8]


def _gen_inputs(
    ead,
    scope_id,
    scope_headers,
    slide_id,
):
    inputs_by_key = {}
    inputs_type_id = []
    ead = deepcopy(ead)

    # my_wsi
    inputs_by_key["my_wsi"] = ead["io"]["my_wsi"]
    inputs_by_key["my_wsi"]["id"] = slide_id
    inputs_type_id.append(("wsi", slide_id))

    # my_rectangles
    items = []
    items.append(
        utils.generate_rectangle_json(
            slide_id,
            scope_id,
            "my_rectangle",
            "scope",
            [RECTS[0][0], RECTS[0][1]],
            RECTSW,
            RECTSH,
            RECTS_MPP_C,
            RECTS_MPP_V,
        )
    )
    items.append(
        utils.generate_rectangle_json(
            slide_id,
            scope_id,
            "my_rectangle",
            "scope",
            [RECTS[1][0], RECTS[1][1]],
            RECTSW,
            RECTSH,
            RECTS_MPP_C,
            RECTS_MPP_V,
        )
    )
    items.append(
        utils.generate_rectangle_json(
            slide_id,
            scope_id,
            "my_rectangle",
            "scope",
            [RECTS[2][0], RECTS[2][1]],
            RECTSW,
            RECTSH,
            RECTS_MPP_C,
            RECTS_MPP_V,
        )
    )
    data = utils.generate_collection_json(None, None, scope_id, "my_rectangles", "scope", "rectangle", items)
    r = utils.wbs_post_collection(scope_headers, scope_id, data)
    print("DAD_Service - POST Collection, status_code: ", r.status_code)
    assert r.status_code == 201
    inputs_by_key["my_rectangles"] = r.json()
    inputs_type_id.append(("collection", r.json()["id"]))

    return inputs_by_key, inputs_type_id


def check_containerized_app_output(job_id):
    """
    Checks DADS output :
    - (1) this [_gen_inputs] are used as inputs
    - (2) the containtized app was executed using (1)
    """
    url = f"{general_settings.mds_host}v3/annotations/query"
    query = {"jobs": [job_id], "creators": [job_id]}
    headers = login_manager.wbs_client()
    r = requests.put(url, json=query, headers=headers)
    print("MDS - PUT annotations query: ", r.status_code)
    assert r.status_code == 200
    assert r.json()["item_count"] == 10 * 3
    annot_ids = []
    for item in r.json()["items"]:
        assert item["type"] == "point"
        annot_ids.append(item["id"])
    url = f"{general_settings.mds_host}v3/primitives/query"
    query = {"jobs": [job_id], "creators": [job_id], "references": annot_ids}
    headers = login_manager.wbs_client()
    r = requests.put(url, json=query, headers=headers)
    print("MDS - PUT primitives query: ", r.status_code)
    assert r.json()["item_count"] == 10 * 3
    for item in r.json()["items"]:
        assert item["type"] == "float"
        assert item["reference_id"] in annot_ids
