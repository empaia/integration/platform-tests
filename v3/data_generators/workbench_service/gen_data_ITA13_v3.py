from copy import deepcopy

import requests

from platform_tests.global_services.data_generators.singletons import general_settings, login_manager


def _gen_inputs_standalone(
    ead,
    slide_id,
):
    inputs_by_key = {}
    inputs_type_id = []
    ead = deepcopy(ead)

    # my_wsi
    inputs_by_key["my_wsi"] = ead["io"]["my_wsi"]
    inputs_by_key["my_wsi"]["id"] = slide_id
    inputs_type_id.append(("wsi", slide_id))

    return inputs_by_key, inputs_type_id


def check_containerized_app_output(job_id, job_mode):
    """
    Checks DADS output :
    - (1) this [_gen_inputs] are used as inputs
    - (2) the containtized app was executed using (1)
    """
    url = f"{general_settings.mds_host}v3/annotations/query"
    query = {"jobs": [job_id], "creators": [job_id]}
    params = {"with_classes": True, "limit": 0}
    headers = login_manager.wbs_client()
    r = requests.put(url, json=query, headers=headers, params=params)
    print("MDS - PUT annotations query: ", r.status_code)
    assert r.status_code == 200
    if job_mode == "STANDALONE":
        assert r.json()["item_count"] == 1
    elif job_mode == "PREPROCESSING":
        assert r.json()["item_count"] == 1
