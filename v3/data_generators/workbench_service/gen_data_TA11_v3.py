from copy import deepcopy

import requests

from platform_tests.global_services.data_generators.singletons import general_settings, login_manager

from ..workbench_service import utils


def _gen_inputs_standalone(
    ead,
    scope_id,
    scope_headers,
    slide_id,
    npp_ranges,
):
    inputs_by_key = {}
    inputs_type_id = []
    ead = deepcopy(ead)

    # my_wsi
    inputs_by_key["my_wsi"] = ead["io"]["my_wsi"]
    inputs_by_key["my_wsi"]["id"] = slide_id
    inputs_type_id.append(("wsi", slide_id))

    # my_rectangle
    data = utils.generate_rectangle_json(
        slide_id,
        scope_id,
        "my_rectangle",
        "scope",
        upper_left=[1000, 2000],
        width=2000,
        height=2000,
        npp_created=npp_ranges[-1],
        npp_viewing=[npp_ranges[0], npp_ranges[-1]],
    )
    r = utils.wbs_post_annotation(scope_headers, scope_id, data)
    print("WBS - POST Recangle, status_code: ", r.status_code)
    assert r.status_code == 201
    inputs_by_key["my_rectangle"] = r.json()
    inputs_type_id.append(("rectangle", r.json()["id"]))

    # roi class
    reference_id = inputs_by_key["my_rectangle"]["id"]
    roi_class = utils.generate_class_json(reference_id, scope_id, "scope", "org.empaia.global.v1.classes.roi")
    r = utils.wbs_post_class(scope_headers, scope_id, roi_class)
    print("WBS - POST Class, status_code: ", r.status_code)
    print(r.content)
    assert r.status_code == 201
    inputs_type_id.append(("class", r.json()["id"]))

    return inputs_by_key, inputs_type_id


def _gen_inputs_preprocessing(
    ead,
    user_id,
    slide_id,
):
    inputs_by_key = {}
    inputs_type_id = []
    ead = deepcopy(ead)

    # my_wsi
    inputs_by_key["my_wsi"] = ead["io"]["my_wsi"]
    inputs_by_key["my_wsi"]["id"] = slide_id
    inputs_type_id.append(("wsi", slide_id))

    return inputs_by_key, inputs_type_id


def check_containerized_app_output(job_id, job_mode):
    """
    Checks DADS output :
    - (1) this [_gen_inputs] are used as inputs
    - (2) the containtized app was executed using (1)
    """
    url = f"{general_settings.mds_host}v3/annotations/query"
    query = {"jobs": [job_id], "creators": [job_id]}
    params = {"with_classes": True, "limit": 0}
    headers = login_manager.wbs_client()
    r = requests.put(url, json=query, headers=headers, params=params)
    print("MDS - PUT annotations query: ", r.status_code)
    assert r.status_code == 200
    if job_mode == "STANDALONE":
        assert r.json()["item_count"] > 1
    elif job_mode == "PREPROCESSING":
        assert r.json()["item_count"] > 0
