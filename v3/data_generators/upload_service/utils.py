from tusclient import client

from ....global_services.data_generators.singletons import general_settings, login_manager


def us_upload_file(f_path, f_name, upload_id):
    url = f"{general_settings.us_host}v1/files"
    headers = login_manager.mta_user()
    my_client = client.TusClient(url, headers=headers)
    uploader = my_client.uploader(
        file_path=f_path,
        chunk_size=5 * 1024**2,
        metadata={"file_name": f_name, "id": str(upload_id)},
    )
    uploader.upload()
    return uploader.url
