from pathlib import Path

from .medical_data_service.case_generators import get_case_data, get_wsi_data


def get_test_wsis():
    file_name = Path.joinpath(Path(__file__).parents[1], "resources", "test_wsis.json")
    return get_wsi_data(file_name)


def get_test_cases():
    file_name = Path.joinpath(Path(__file__).parents[1], "resources", "test_cases.json")
    return get_case_data(file_name)
